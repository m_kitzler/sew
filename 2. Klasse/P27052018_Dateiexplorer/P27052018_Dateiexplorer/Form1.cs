﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P27052018_Dateiexplorer
{
    public partial class Form1 : Form
    {
        private string currDir = "";
        private string lastDir = "";
        public Form1()
        {
            InitializeComponent();
            listViewFilesNFolders.SmallImageList = icons;
            //refresh(@"C:\");
            foreach (DriveInfo d in DriveInfo.GetDrives())
            {
                if (d.IsReady)
                {
                    TreeNode hlp = new TreeNode(d.VolumeLabel + " (" + d.Name + ")");
                    hlp.Tag = d.Name;
                    treeViewDirs.Nodes.Find("KnotenThisPC", false)[0].Nodes.Add(hlp);
                }
            }
        }

        private void refresh (string path)
        {
            AddAllNodes(path);
            listViewFilesNFolders.Items.Clear();
            List<string> folders = new List<string>(Directory.GetDirectories(path));    //0: Name, 1: Änderungsdatum, 2: Typ, 3: Größe, 4: Pfad
            folders.Sort();
            if (folders.Count > 0)
            {
                foreach (string folder in folders)
                {
                    string[] folderPath = folder.Split('\\');
                    string[] info = { folderPath[folderPath.Length - 1], new DirectoryInfo(folder).LastWriteTime.ToString("g"), "Dateiordner", "", folder };
                    listViewFilesNFolders.Items.Add(new ListViewItem(info));
                }
            }
            List<string> files = new List<string>(Directory.GetFiles(path));
            files.Sort();
            if (files.Count > 0)
            {
                foreach (string file in files)
                {
                    string[] filePath = file.Split('\\');
                    string[] fileName = filePath[filePath.Length - 1].Split('.');
                    string[] info = { filePath[filePath.Length - 1], new FileInfo(file).LastWriteTime.ToString("g"), fileName[fileName.Length - 1], GetFileSize(new FileInfo(file)), file };
                    listViewFilesNFolders.Items.Add(new ListViewItem(info));
                }
            }
        }

        /// <summary>
        /// Fügt den Ordner und alle Ordner in dem Sich der Order befindet der TreeView hinzu.
        /// </summary>
        /// <param name="path"></param>
        private void AddAllNodes(string path)
        {
            if (path.Split('\\')[1] == "")
            {
                AddNodes(path, "root");
            }
            else
            {
                string[] hlp = path.Split('\\');
                hlp[0] += "\\";
                for (int i = 1; i < hlp.Length; i++)
                {
                    hlp[i] = hlp[i - 1] + "\\" + hlp[i] + "\\";
                }
                AddNodes(hlp[0], "root");
                for (int i = 1; i < hlp.Length; i++)
                {
                    AddNodes(hlp[i], hlp[i - 1]);
                }
            }
        }

        /// <summary>
        /// Fügt den angegebenen Order der TreeView hinzu.
        /// </summary>
        /// <param name="path"></param>
        private void AddNodes(string path, string parentPath)
        {
            if (parentPath == "root")
            {
                if (treeViewDirs.Nodes.Find("Node" + path, false).Length == 0)
                {
                    TreeNode add = new TreeNode(path.Split('\\')[path.Split('\\').Length - 1]);
                    add.Name = "Node" + path;
                    add.Tag = path;
                    List<TreeNode> hlp = new List<TreeNode>();
                    foreach (string dp in Directory.GetDirectories(path))
                    {
                        DirectoryInfo d = new DirectoryInfo(dp);
                        TreeNode tn = new TreeNode(d.Name);
                        tn.Name = "Node" + d.FullName;
                        tn.Tag = d.FullName;
                        hlp.Add(tn);
                    }
                    add.Nodes.AddRange(hlp.ToArray());
                    treeViewDirs.Nodes.Add(add);
                }
                else
                {
                    treeViewDirs.Nodes.Find("Node" + path, false)[0].Expand();
                }
            }
            else
            {
                if (treeViewDirs.Nodes.Find("Node" + path, true).Length == 0)
                {
                    TreeNode add = new TreeNode(path.Split('\\')[path.Split('\\').Length - 1]);
                    add.Name = "Node" + path;
                    add.Tag = path;
                    List<TreeNode> hlp = new List<TreeNode>();
                    foreach (string dp in Directory.GetDirectories(path))
                    {
                        DirectoryInfo d = new DirectoryInfo(dp);
                        TreeNode tn = new TreeNode(d.Name);
                        tn.Name = "Node" + d.FullName;
                        tn.Tag = d.FullName;
                        hlp.Add(tn);
                    }
                    add.Nodes.AddRange(hlp.ToArray());
                    treeViewDirs.Nodes.Find("Node" + parentPath, true)[0].Nodes.Add(add);
                }
                else
                {
                    treeViewDirs.Nodes.Find("Node" + path, true)[0].Expand();
                }
            }
        }

        private string GetFileSize(FileInfo file)
        {
            string hlp = "B";
            double hlp1 = file.Length;
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "KB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "MB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "GB";
            }
            return Math.Round(hlp1, 2).ToString() + " " + hlp;
        }

        private void ListViewFilesNFolders_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem.ListViewSubItemCollection hlp = listViewFilesNFolders.SelectedItems[0].SubItems;
            if (Directory.Exists(hlp[hlp.Count - 1].Text))
            {
                lastDir = currDir;
                currDir = hlp[hlp.Count - 1].Text;
                refresh(hlp[hlp.Count - 1].Text);
            }
            else
            {
                Process.Start(hlp[hlp.Count - 1].Text);
            }
        }

        #region MenuStrip
        private void LadenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogBrowseFiles.ShowDialog() == DialogResult.OK)
            {
                refresh(folderBrowserDialogBrowseFiles.SelectedPath);
            }
        }

        private void DetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem[] viewMenuItems = new ToolStripMenuItem[] { detailToolStripMenuItem, largeIconToolStripMenuItem};
            if (!detailToolStripMenuItem.Checked)
            {
                foreach (ToolStripMenuItem t in viewMenuItems)
                {
                    t.Checked = false;
                }
                detailToolStripMenuItem.Checked = true;
            }
            listViewFilesNFolders.View = View.Details;
        }

        private void LargeIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem[] viewMenuItems = new ToolStripMenuItem[] { detailToolStripMenuItem, largeIconToolStripMenuItem };
            if (!largeIconToolStripMenuItem.Checked)
            {
                foreach (ToolStripMenuItem t in viewMenuItems)
                {
                    t.Checked = false;
                }
                largeIconToolStripMenuItem.Checked = true;
            }
            listViewFilesNFolders.View = View.LargeIcon;
        }
        #endregion
    }
}
