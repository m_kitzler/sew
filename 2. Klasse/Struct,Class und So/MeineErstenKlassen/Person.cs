﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeineErstenKlassen
{
    class Person
    {
        string Test; //Automatisch private
        private string vorname;
        string nachname;
        public Person(string vorname,string nn)
        {
            this.vorname = vorname;
            nachname = nn;
        }
        public string getVorname()
        {
            return vorname;
        }
        public string Vorname
        {
            get
            {
                return vorname;
            }
        }
        public override string ToString()
        {
            return vorname+ " " + nachname;
        }
    }
}