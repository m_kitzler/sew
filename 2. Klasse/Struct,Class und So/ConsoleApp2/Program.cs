﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Person
    {
        public string Vorname;
        public string Nachname;
        public override string ToString()
        {
            return Vorname + " " + Nachname;
        }
    }
    class Mensch
    {
        public string Vorname;
        public string Nachname;
        public override string ToString()
        {
            return Vorname + " " + Nachname;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int x = 42;
            string y = "Zwettl";
            int[] z = { 1, 2, 3, 4, 5 };
            Person p = new Person();
            Mensch m = new Mensch();
            p.Vorname = "Herwig";
            p.Nachname = "Macho";
            m.Vorname = "Herwig";
            m.Nachname = "Macho";
            tuwas(x, y, z, p, m);
            Console.WriteLine("{0} - {1} - {2} - {3} - {4}", x, y, z[3], p, m);
        }
        static void tuwas(int a, string b, int[] c, Person d, Mensch e)
        {
            Console.WriteLine("{0} - {1} - {2} - {3} - {4}", a, b, c[3], d, e);

            a = 24;
            b = "Miau";
            c[3] = 33;
            d.Vorname = "Martin";
            d.Nachname = "Kitzler";
            e.Vorname = "Marcel";
            e.Nachname = "Davis";

            Console.WriteLine("{0} - {1} - {2} - {3} - {4}", a, b, c[3], d, e);
        }
    }
}
