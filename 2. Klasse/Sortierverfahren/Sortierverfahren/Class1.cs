﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortierverfahren
{
    public class SortLib
    {
        static public int[] Bubblesort(int[] toSort)
        {
            for (int i = toSort.Length; i > 1; i--)
            {
                for (int j = 1; j < i; j++)
                {
                    if (toSort[j-1] > toSort[j])
                    {
                        int hlp = toSort[j-1];
                        toSort[j-1] = toSort[j];
                        toSort[j] = hlp;
                    }
                }
            }
            return toSort;
        }
        static public int[] Insertionsort(int[] toSort)
        {
            for (int i = 1; i < toSort.Length; i++)
            {
                int hlp = toSort[i];
                int j = i;
                while(j > 0 && toSort[j - 1] > hlp)
                {
                    toSort[j] = toSort[j - 1];
                    j--;
                }
                toSort[j] = hlp;
            }
            return toSort;
        }
        static public int[] Mergesort(int[] toSort)
        {
            if (!(toSort.Length <= 1))
            {
                double hlp = toSort.Length / 2;
                int[] array1 = toSort.Take((int)Math.Ceiling(hlp)).ToArray();
                int[] array2 = toSort.Skip((int)Math.Floor(hlp)).ToArray();
                List<int> list1 = Mergesort(array1).ToList();
                List<int> list2 = Mergesort(array2).ToList();
                for (int i = 0; i < toSort.Length; i++)
                {
                    if (list1.Count == 0)
                    {
                        toSort[i] = list2[0];
                    }
                    else if (list2.Count == 0)
                    {
                        toSort[i] = list1[0];
                    }
                    else if (list1[0] <= list2[0])
                    {
                        toSort[i] = list1[0];
                        list1.RemoveAt(0);
                    }
                    else
                    {
                        toSort[i] = list2[0];
                        list2.RemoveAt(0);
                    }
                }
            }
            return toSort;
        }
    }
}
