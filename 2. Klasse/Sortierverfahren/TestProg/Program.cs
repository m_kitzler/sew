﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sortierverfahren;

namespace TestProg
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sortTarget = { 5, 2, 6, 3, 6, 1, 4};
            //sortTarget = SortLib.Bubblesort(sortTarget);
            //sortTarget = SortLib.Insertionsort(sortTarget);
            //sortTarget = SortLib.Mergesort(sortTarget);
            foreach(int x in sortTarget)
            {
                Console.WriteLine(x);
            }
        }
    }
}
