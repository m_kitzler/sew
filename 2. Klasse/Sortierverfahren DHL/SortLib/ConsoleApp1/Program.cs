﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortLib;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            int[] sortTarget = new int[10000];
            for (int i = 0; i < sortTarget.Length; i++)
            {
                sortTarget[i] = r.Next(1, 1000000);
            }
            double[] result = Sort.Bubblesort(ref sortTarget);
            //double[] result = Sort.Insertionsort(ref sortTarget);
            //sortTarget = Sort.Mergesort(sortTarget);
            //sortTarget = Sort.Quicksort(sortTarget);
            //sortTarget = Sort.Selectionsort(sortTarget);
            Console.WriteLine("Compares: {0}\nSwaps: {1}\nTime: {2}ms",result[0], result[1], result[2]);
        }
    }
}
