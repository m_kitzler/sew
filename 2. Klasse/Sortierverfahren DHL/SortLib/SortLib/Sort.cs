﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortLib
{
    public class Sort
    {
        static public double[] Bubblesort(ref int[] toSort)
        {
            DateTime t = DateTime.Now;
            double compares = 0;
            double swaps = 0;
            for (int i = toSort.Length; i > 1; i--)
            {
                for (int j = 1; j < i; j++)
                {
                    compares++;
                    if (toSort[j - 1] > toSort[j])
                    {
                        swaps++;
                        int hlp = toSort[j - 1];
                        toSort[j - 1] = toSort[j];
                        toSort[j] = hlp;
                    }
                }
            }
            return new double[] { compares, swaps, DateTime.Now.Subtract(t).TotalMilliseconds };
        }
        static public double[] Insertionsort(ref int[] toSort)
        {
            DateTime t = DateTime.Now;
            double compares = 0;
            double swaps = 0;
            for (int i = 1; i < toSort.Length; i++)
            {
                int hlp = toSort[i];
                int j = i;
                compares++;
                while (j > 0 && toSort[j - 1] > hlp)
                {
                    swaps++;
                    toSort[j] = toSort[j - 1];
                    j--;
                }
                toSort[j] = hlp;
            }
            return new double[] { compares, swaps, DateTime.Now.Subtract(t).TotalMilliseconds };
        }
        static public int[] Mergesort(int[] toSort)
        {
            if (!(toSort.Length <= 1))
            {
                double hlp = toSort.Length / 2;
                int[] array1 = toSort.Take((int)Math.Ceiling(hlp)).ToArray();
                int[] array2 = toSort.Skip((int)Math.Floor(hlp)).ToArray();
                List<int> list1 = Mergesort(array1).ToList();
                List<int> list2 = Mergesort(array2).ToList();
                for (int i = 0; i < toSort.Length; i++)
                {
                    if (list1.Count == 0)
                    {
                        toSort[i] = list2[0];
                    }
                    else if (list2.Count == 0)
                    {
                        toSort[i] = list1[0];
                    }
                    else if (list1[0] <= list2[0])
                    {
                        toSort[i] = list1[0];
                        list1.RemoveAt(0);
                    }
                    else
                    {
                        toSort[i] = list2[0];
                        list2.RemoveAt(0);
                    }
                }
            }
            return toSort;
        }
        static public int[] Quicksort(int[] toSort)
        {
            if (toSort.Length > 1) //prüft, ob Array mindestens 2 Elemente hat
            {
                int i = 0;
                int j = toSort.Length - 2;
                int pivot = toSort[toSort.Length - 1];
                do
                {
                    for (; i < toSort.Length - 1; i++) //suche Element zum tauschen von Links...
                    {
                        if (toSort[i] > pivot)
                            break;
                    }
                    for (; j > 0; j--) //und hier von Rechts.
                    {
                        if (toSort[j] < pivot)
                            break;
                    }
                    if (i < j) //tauschen
                    {
                        int hlp = toSort[i];
                        toSort[i] = toSort[j];
                        toSort[j] = hlp;
                    }
                }
                while (i < j); //prüfe, ob die beiden aneinander vorbei sind
                if (toSort[i] > pivot) //tausche Pivot-Element in Mitte, falls möglich
                {
                    toSort[toSort.Length - 1] = toSort[i];
                    toSort[i] = pivot;
                }
                int[] array1 = Quicksort(toSort.Take(i).ToArray()); //führe Quicksort für die zwei entstandenen Hälften aus
                int[] array2 = Quicksort(toSort.Skip(i).ToArray());
                Array.Copy(array1, toSort, array1.Length); //kombiniere Arrays
                Array.Copy(array2, 0, toSort, array1.Length, array2.Length);
            }
            return toSort;
        }
        static public int[] Selectionsort(int[] toSort)
        {
            for (int i = 0; i < toSort.Length - 1; i++)
            {
                int lowest = i;
                for (int j = i; j < toSort.Length; j++)
                {
                    if (toSort[j] < toSort[lowest])
                        lowest = j;
                }
                if (toSort[i] > toSort[lowest])
                {
                    int hlp = toSort[i];
                    toSort[i] = toSort[lowest];
                    toSort[lowest] = hlp;
                }
            }
            return toSort;
        }
    }
}