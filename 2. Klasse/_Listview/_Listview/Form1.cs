﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _Listview
{
    public partial class Form1 : Form
    {
        ListView choosenListView;
        ImageList liL = new ImageList();
        ImageList liS = new ImageList();
        public Form1()
        {
            InitializeComponent();
            choosenListView = listViewDetails;
            liL.ImageSize = new Size(32, 32);
            liL.ColorDepth = ColorDepth.Depth32Bit;
            liS.ColorDepth = ColorDepth.Depth32Bit;
            choosenListView.LargeImageList = liL;
            choosenListView.SmallImageList = liS;
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            Process[] processes = Process.GetProcesses();
            foreach(Process p in processes)
            {
                bool alreadyexists = false;
                foreach(ListViewItem item in choosenListView.Items)
                {
                    if (p.Id.ToString() == item.SubItems[2].Text)
                    {
                        alreadyexists = true;
                    }
                }
                if (!alreadyexists)
                {
                    string[] info = { p.ProcessName + ".exe", "wird ausgeführt", p.Id.ToString(), (Math.Round((double)p.NonpagedSystemMemorySize64 / 1024, 2)).ToString() + " MB" };
                    ListViewItem item = new ListViewItem(info);
                    item.ImageKey = p.Id.ToString();
                    item.Group = choosenListView.Groups[0];
                    choosenListView.Items.Add(item);
                    try
                    {
                        liL.Images.Add(p.Id.ToString(), Icon.ExtractAssociatedIcon(p.MainModule.FileName));
                        liS.Images.Add(p.Id.ToString(), Icon.ExtractAssociatedIcon(p.MainModule.FileName));
                    }
                    catch { }
                }
            }
            foreach (ListViewItem item in choosenListView.Items)
            {
                bool exists = false;
                foreach(Process p in processes)
                {
                    if (p.Id.ToString() == item.SubItems[2].Text)
                        exists = true;
                }
                if (exists)
                    item.SubItems[1].Text = "wird ausgeführt";
                else
                    item.SubItems[1].Text = "beendet";
                if (Convert.ToInt32(item.SubItems[2].Text) > 1000)
                    item.Group = choosenListView.Groups[1];
            }
            choosenListView.Sort();
        }

        private void buttonEndProcess_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in choosenListView.SelectedItems)
            {
                Int32.TryParse(item.SubItems[2].Text, out int hlp);
                try
                {
                    Process.GetProcessById(hlp).Kill();
                }
                catch (Exception ex)
                {
                    if (fehlermeldungenAnzeigenToolStripMenuItem.Checked)
                    {
                        MessageBox.Show(ex.Message, "Fehler");
                    }
                }
            }
        }

        private void listViewDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (choosenListView.SelectedItems.Count == 0)
                buttonEndProcess.Enabled = false;
            else
                buttonEndProcess.Enabled = true;
        }

        private void jetztAktualisierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer_Tick(sender, e);
        }

        private void extremToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer.Interval = 1;
            uncheck();
            extremToolStripMenuItem.Checked = true;
        }

        private void hochToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer.Interval = 500;
            uncheck();
            hochToolStripMenuItem.Checked = true;
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer.Interval = 1000;
            uncheck();
            normalToolStripMenuItem.Checked = true;
        }

        private void niedrigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer.Interval = 2000;
            uncheck();
            niedrigToolStripMenuItem.Checked = true;
        }

        private void vielleichtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int hlp = (new Random()).Next(1, 5000);
            refreshTimer.Interval = hlp;
            randomToolStripMenuItem.Text = "Random (" + hlp + "ms)";
            uncheck();
            randomToolStripMenuItem.Checked = true;
        }

        private void angehaltenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshTimer.Stop();
            uncheck();
            angehaltenToolStripMenuItem.Checked = true;
        }
        private void angehaltenToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!angehaltenToolStripMenuItem.Checked)
                refreshTimer.Start();
        }
        private void uncheck()
        {
            foreach (ToolStripMenuItem item in aktualisierungsrateToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
        }

        private void multiselectToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            choosenListView.MultiSelect = multiselectToolStripMenuItem.Checked;
        }

        private void tabControlPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(tabControlPrograms.SelectedIndex)
            {
                case 0:
                    choosenListView = listViewDetails;
                    break;
                case 1:
                    choosenListView = listViewList;
                    break;
                case 2:
                    choosenListView = listViewTile;
                    break;
                case 3:
                    choosenListView = listViewSmall;
                    break;
                case 4:
                    choosenListView = listViewLarge;
                    break;
            }
            choosenListView.LargeImageList = liL;
            choosenListView.SmallImageList = liS;
            multiselectToolStripMenuItem_CheckedChanged(sender, e);
            gruppenToolStripMenuItem_CheckedChanged(sender,e);
        }

        private void gruppenToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            choosenListView.ShowGroups = gruppenToolStripMenuItem.Checked;
        }

        private void columnReorderToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            choosenListView.AllowColumnReorder = columnReorderToolStripMenuItem.Checked;
        }
    }
}
