﻿namespace _Listview
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Programme", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("PID größer 1000", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Programme", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("PID größer 1000", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("Programme", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("PID größer 1000", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.ListViewGroup listViewGroup7 = new System.Windows.Forms.ListViewGroup("Programme", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup8 = new System.Windows.Forms.ListViewGroup("PID größer 1000", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup9 = new System.Windows.Forms.ListViewGroup("Programme", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup10 = new System.Windows.Forms.ListViewGroup("PID größer 1000", System.Windows.Forms.HorizontalAlignment.Left);
            this.tabControlPrograms = new System.Windows.Forms.TabControl();
            this.tabPageDetails = new System.Windows.Forms.TabPage();
            this.listViewDetails = new System.Windows.Forms.ListView();
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageList = new System.Windows.Forms.TabPage();
            this.listViewList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageTile = new System.Windows.Forms.TabPage();
            this.listViewTile = new System.Windows.Forms.ListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButtonOptions = new System.Windows.Forms.ToolStripDropDownButton();
            this.multiselectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fehlermeldungenAnzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonView = new System.Windows.Forms.ToolStripDropDownButton();
            this.jetztAktualisierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aktualisierungsrateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extremToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hochToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.niedrigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.angehaltenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonEndProcess = new System.Windows.Forms.Button();
            this.refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.tabPageSmall = new System.Windows.Forms.TabPage();
            this.tabPageLarge = new System.Windows.Forms.TabPage();
            this.listViewSmall = new System.Windows.Forms.ListView();
            this.listViewLarge = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gruppenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.columnReorderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControlPrograms.SuspendLayout();
            this.tabPageDetails.SuspendLayout();
            this.tabPageList.SuspendLayout();
            this.tabPageTile.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPageSmall.SuspendLayout();
            this.tabPageLarge.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlPrograms
            // 
            this.tabControlPrograms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlPrograms.Controls.Add(this.tabPageDetails);
            this.tabControlPrograms.Controls.Add(this.tabPageList);
            this.tabControlPrograms.Controls.Add(this.tabPageTile);
            this.tabControlPrograms.Controls.Add(this.tabPageSmall);
            this.tabControlPrograms.Controls.Add(this.tabPageLarge);
            this.tabControlPrograms.Location = new System.Drawing.Point(0, 28);
            this.tabControlPrograms.Name = "tabControlPrograms";
            this.tabControlPrograms.SelectedIndex = 0;
            this.tabControlPrograms.Size = new System.Drawing.Size(684, 392);
            this.tabControlPrograms.TabIndex = 0;
            this.tabControlPrograms.SelectedIndexChanged += new System.EventHandler(this.tabControlPrograms_SelectedIndexChanged);
            // 
            // tabPageDetails
            // 
            this.tabPageDetails.Controls.Add(this.listViewDetails);
            this.tabPageDetails.Location = new System.Drawing.Point(4, 22);
            this.tabPageDetails.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageDetails.Name = "tabPageDetails";
            this.tabPageDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDetails.Size = new System.Drawing.Size(676, 366);
            this.tabPageDetails.TabIndex = 0;
            this.tabPageDetails.Text = "Details";
            this.tabPageDetails.UseVisualStyleBackColor = true;
            // 
            // listViewDetails
            // 
            this.listViewDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderName,
            this.columnHeaderStatus,
            this.columnHeaderPID,
            this.columnHeaderRam});
            this.listViewDetails.FullRowSelect = true;
            listViewGroup1.Header = "Programme";
            listViewGroup1.Name = "listViewGroupDefault";
            listViewGroup2.Header = "PID größer 1000";
            listViewGroup2.Name = "listViewGroupPID";
            this.listViewDetails.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.listViewDetails.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewDetails.Location = new System.Drawing.Point(-4, 0);
            this.listViewDetails.MultiSelect = false;
            this.listViewDetails.Name = "listViewDetails";
            this.listViewDetails.Size = new System.Drawing.Size(684, 370);
            this.listViewDetails.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewDetails.TabIndex = 0;
            this.listViewDetails.UseCompatibleStateImageBehavior = false;
            this.listViewDetails.View = System.Windows.Forms.View.Details;
            this.listViewDetails.SelectedIndexChanged += new System.EventHandler(this.listViewDetails_SelectedIndexChanged);
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Name";
            this.columnHeaderName.Width = 200;
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            this.columnHeaderStatus.Width = 84;
            // 
            // columnHeaderPID
            // 
            this.columnHeaderPID.Text = "PID";
            // 
            // columnHeaderRam
            // 
            this.columnHeaderRam.Text = "RAM";
            // 
            // tabPageList
            // 
            this.tabPageList.Controls.Add(this.listViewList);
            this.tabPageList.Location = new System.Drawing.Point(4, 22);
            this.tabPageList.Name = "tabPageList";
            this.tabPageList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageList.Size = new System.Drawing.Size(676, 366);
            this.tabPageList.TabIndex = 1;
            this.tabPageList.Text = "Liste";
            this.tabPageList.UseVisualStyleBackColor = true;
            // 
            // listViewList
            // 
            this.listViewList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listViewList.FullRowSelect = true;
            listViewGroup3.Header = "Programme";
            listViewGroup3.Name = "listViewGroupDefault";
            listViewGroup4.Header = "PID größer 1000";
            listViewGroup4.Name = "listViewGroupPID";
            this.listViewList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.listViewList.Location = new System.Drawing.Point(-4, -2);
            this.listViewList.MultiSelect = false;
            this.listViewList.Name = "listViewList";
            this.listViewList.Size = new System.Drawing.Size(684, 370);
            this.listViewList.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewList.TabIndex = 1;
            this.listViewList.UseCompatibleStateImageBehavior = false;
            this.listViewList.View = System.Windows.Forms.View.List;
            this.listViewList.SelectedIndexChanged += new System.EventHandler(this.listViewDetails_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "PID";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 84;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "RAM";
            // 
            // tabPageTile
            // 
            this.tabPageTile.Controls.Add(this.listViewTile);
            this.tabPageTile.Location = new System.Drawing.Point(4, 22);
            this.tabPageTile.Name = "tabPageTile";
            this.tabPageTile.Size = new System.Drawing.Size(676, 366);
            this.tabPageTile.TabIndex = 2;
            this.tabPageTile.Text = "Tile";
            this.tabPageTile.UseVisualStyleBackColor = true;
            // 
            // listViewTile
            // 
            this.listViewTile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewTile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader8});
            this.listViewTile.FullRowSelect = true;
            listViewGroup5.Header = "Programme";
            listViewGroup5.Name = "listViewGroupDefault";
            listViewGroup6.Header = "PID größer 1000";
            listViewGroup6.Name = "listViewGroupPID";
            this.listViewTile.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup5,
            listViewGroup6});
            this.listViewTile.Location = new System.Drawing.Point(-4, -2);
            this.listViewTile.MultiSelect = false;
            this.listViewTile.Name = "listViewTile";
            this.listViewTile.Size = new System.Drawing.Size(684, 370);
            this.listViewTile.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewTile.TabIndex = 2;
            this.listViewTile.UseCompatibleStateImageBehavior = false;
            this.listViewTile.View = System.Windows.Forms.View.Tile;
            this.listViewTile.SelectedIndexChanged += new System.EventHandler(this.listViewDetails_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonFile,
            this.toolStripButtonOptions,
            this.toolStripButtonView});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(684, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonFile
            // 
            this.toolStripButtonFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFile.Image")));
            this.toolStripButtonFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFile.Name = "toolStripButtonFile";
            this.toolStripButtonFile.ShowDropDownArrow = false;
            this.toolStripButtonFile.Size = new System.Drawing.Size(38, 22);
            this.toolStripButtonFile.Text = "Datei";
            // 
            // toolStripButtonOptions
            // 
            this.toolStripButtonOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multiselectToolStripMenuItem,
            this.fehlermeldungenAnzeigenToolStripMenuItem,
            this.gruppenToolStripMenuItem,
            this.columnReorderToolStripMenuItem});
            this.toolStripButtonOptions.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOptions.Image")));
            this.toolStripButtonOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOptions.Name = "toolStripButtonOptions";
            this.toolStripButtonOptions.ShowDropDownArrow = false;
            this.toolStripButtonOptions.Size = new System.Drawing.Size(61, 22);
            this.toolStripButtonOptions.Text = "Optionen";
            // 
            // multiselectToolStripMenuItem
            // 
            this.multiselectToolStripMenuItem.CheckOnClick = true;
            this.multiselectToolStripMenuItem.Name = "multiselectToolStripMenuItem";
            this.multiselectToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.multiselectToolStripMenuItem.Text = "Multiselect";
            this.multiselectToolStripMenuItem.CheckedChanged += new System.EventHandler(this.multiselectToolStripMenuItem_CheckedChanged);
            // 
            // fehlermeldungenAnzeigenToolStripMenuItem
            // 
            this.fehlermeldungenAnzeigenToolStripMenuItem.Checked = true;
            this.fehlermeldungenAnzeigenToolStripMenuItem.CheckOnClick = true;
            this.fehlermeldungenAnzeigenToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fehlermeldungenAnzeigenToolStripMenuItem.Name = "fehlermeldungenAnzeigenToolStripMenuItem";
            this.fehlermeldungenAnzeigenToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.fehlermeldungenAnzeigenToolStripMenuItem.Text = "Fehlermeldungen anzeigen";
            // 
            // toolStripButtonView
            // 
            this.toolStripButtonView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jetztAktualisierenToolStripMenuItem,
            this.aktualisierungsrateToolStripMenuItem});
            this.toolStripButtonView.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonView.Image")));
            this.toolStripButtonView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonView.Name = "toolStripButtonView";
            this.toolStripButtonView.ShowDropDownArrow = false;
            this.toolStripButtonView.Size = new System.Drawing.Size(51, 22);
            this.toolStripButtonView.Text = "Ansicht";
            // 
            // jetztAktualisierenToolStripMenuItem
            // 
            this.jetztAktualisierenToolStripMenuItem.Name = "jetztAktualisierenToolStripMenuItem";
            this.jetztAktualisierenToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.jetztAktualisierenToolStripMenuItem.Text = "Jetzt Aktualisieren";
            this.jetztAktualisierenToolStripMenuItem.Click += new System.EventHandler(this.jetztAktualisierenToolStripMenuItem_Click);
            // 
            // aktualisierungsrateToolStripMenuItem
            // 
            this.aktualisierungsrateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extremToolStripMenuItem,
            this.hochToolStripMenuItem,
            this.normalToolStripMenuItem,
            this.niedrigToolStripMenuItem,
            this.randomToolStripMenuItem,
            this.angehaltenToolStripMenuItem});
            this.aktualisierungsrateToolStripMenuItem.Name = "aktualisierungsrateToolStripMenuItem";
            this.aktualisierungsrateToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.aktualisierungsrateToolStripMenuItem.Text = "Aktualisierungsrate";
            // 
            // extremToolStripMenuItem
            // 
            this.extremToolStripMenuItem.Name = "extremToolStripMenuItem";
            this.extremToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.extremToolStripMenuItem.Text = "Extrem";
            this.extremToolStripMenuItem.Click += new System.EventHandler(this.extremToolStripMenuItem_Click);
            // 
            // hochToolStripMenuItem
            // 
            this.hochToolStripMenuItem.Name = "hochToolStripMenuItem";
            this.hochToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.hochToolStripMenuItem.Text = "Hoch";
            this.hochToolStripMenuItem.Click += new System.EventHandler(this.hochToolStripMenuItem_Click);
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Checked = true;
            this.normalToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // niedrigToolStripMenuItem
            // 
            this.niedrigToolStripMenuItem.Name = "niedrigToolStripMenuItem";
            this.niedrigToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.niedrigToolStripMenuItem.Text = "Niedrig";
            this.niedrigToolStripMenuItem.Click += new System.EventHandler(this.niedrigToolStripMenuItem_Click);
            // 
            // randomToolStripMenuItem
            // 
            this.randomToolStripMenuItem.Name = "randomToolStripMenuItem";
            this.randomToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.randomToolStripMenuItem.Text = "Random";
            this.randomToolStripMenuItem.Click += new System.EventHandler(this.vielleichtToolStripMenuItem_Click);
            // 
            // angehaltenToolStripMenuItem
            // 
            this.angehaltenToolStripMenuItem.Name = "angehaltenToolStripMenuItem";
            this.angehaltenToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.angehaltenToolStripMenuItem.Text = "Angehalten";
            this.angehaltenToolStripMenuItem.CheckedChanged += new System.EventHandler(this.angehaltenToolStripMenuItem_CheckedChanged);
            this.angehaltenToolStripMenuItem.Click += new System.EventHandler(this.angehaltenToolStripMenuItem_Click);
            // 
            // buttonEndProcess
            // 
            this.buttonEndProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEndProcess.Enabled = false;
            this.buttonEndProcess.Location = new System.Drawing.Point(597, 426);
            this.buttonEndProcess.Name = "buttonEndProcess";
            this.buttonEndProcess.Size = new System.Drawing.Size(75, 23);
            this.buttonEndProcess.TabIndex = 2;
            this.buttonEndProcess.Text = "KILL";
            this.buttonEndProcess.UseVisualStyleBackColor = true;
            this.buttonEndProcess.Click += new System.EventHandler(this.buttonEndProcess_Click);
            // 
            // refreshTimer
            // 
            this.refreshTimer.Enabled = true;
            this.refreshTimer.Interval = 1000;
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // tabPageSmall
            // 
            this.tabPageSmall.Controls.Add(this.listViewSmall);
            this.tabPageSmall.Location = new System.Drawing.Point(4, 22);
            this.tabPageSmall.Name = "tabPageSmall";
            this.tabPageSmall.Size = new System.Drawing.Size(676, 366);
            this.tabPageSmall.TabIndex = 3;
            this.tabPageSmall.Text = "Small Icons";
            this.tabPageSmall.UseVisualStyleBackColor = true;
            // 
            // tabPageLarge
            // 
            this.tabPageLarge.Controls.Add(this.listViewLarge);
            this.tabPageLarge.Location = new System.Drawing.Point(4, 22);
            this.tabPageLarge.Name = "tabPageLarge";
            this.tabPageLarge.Size = new System.Drawing.Size(676, 366);
            this.tabPageLarge.TabIndex = 4;
            this.tabPageLarge.Text = "Large Icons";
            this.tabPageLarge.UseVisualStyleBackColor = true;
            // 
            // listViewSmall
            // 
            this.listViewSmall.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewSmall.FullRowSelect = true;
            listViewGroup7.Header = "Programme";
            listViewGroup7.Name = "listViewGroupDefault";
            listViewGroup8.Header = "PID größer 1000";
            listViewGroup8.Name = "listViewGroupPID";
            this.listViewSmall.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup7,
            listViewGroup8});
            this.listViewSmall.Location = new System.Drawing.Point(-4, -2);
            this.listViewSmall.MultiSelect = false;
            this.listViewSmall.Name = "listViewSmall";
            this.listViewSmall.Size = new System.Drawing.Size(684, 370);
            this.listViewSmall.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewSmall.TabIndex = 3;
            this.listViewSmall.UseCompatibleStateImageBehavior = false;
            this.listViewSmall.View = System.Windows.Forms.View.SmallIcon;
            this.listViewSmall.SelectedIndexChanged += new System.EventHandler(this.listViewDetails_SelectedIndexChanged);
            // 
            // listViewLarge
            // 
            this.listViewLarge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewLarge.FullRowSelect = true;
            listViewGroup9.Header = "Programme";
            listViewGroup9.Name = "listViewGroupDefault";
            listViewGroup10.Header = "PID größer 1000";
            listViewGroup10.Name = "listViewGroupPID";
            this.listViewLarge.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup9,
            listViewGroup10});
            this.listViewLarge.Location = new System.Drawing.Point(-4, -2);
            this.listViewLarge.MultiSelect = false;
            this.listViewLarge.Name = "listViewLarge";
            this.listViewLarge.Size = new System.Drawing.Size(684, 370);
            this.listViewLarge.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewLarge.TabIndex = 3;
            this.listViewLarge.UseCompatibleStateImageBehavior = false;
            this.listViewLarge.SelectedIndexChanged += new System.EventHandler(this.listViewDetails_SelectedIndexChanged);
            // 
            // gruppenToolStripMenuItem
            // 
            this.gruppenToolStripMenuItem.Checked = true;
            this.gruppenToolStripMenuItem.CheckOnClick = true;
            this.gruppenToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gruppenToolStripMenuItem.Name = "gruppenToolStripMenuItem";
            this.gruppenToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.gruppenToolStripMenuItem.Text = "Gruppen";
            this.gruppenToolStripMenuItem.CheckedChanged += new System.EventHandler(this.gruppenToolStripMenuItem_CheckedChanged);
            // 
            // columnReorderToolStripMenuItem
            // 
            this.columnReorderToolStripMenuItem.CheckOnClick = true;
            this.columnReorderToolStripMenuItem.Name = "columnReorderToolStripMenuItem";
            this.columnReorderToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.columnReorderToolStripMenuItem.Text = "ColumnReorder";
            this.columnReorderToolStripMenuItem.CheckedChanged += new System.EventHandler(this.columnReorderToolStripMenuItem_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.buttonEndProcess);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControlPrograms);
            this.Name = "Form1";
            this.Text = "Taskmanager 2.0 (besser)";
            this.tabControlPrograms.ResumeLayout(false);
            this.tabPageDetails.ResumeLayout(false);
            this.tabPageList.ResumeLayout(false);
            this.tabPageTile.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPageSmall.ResumeLayout(false);
            this.tabPageLarge.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlPrograms;
        private System.Windows.Forms.TabPage tabPageDetails;
        private System.Windows.Forms.TabPage tabPageList;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ListView listViewDetails;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderPID;
        private System.Windows.Forms.Button buttonEndProcess;
        private System.Windows.Forms.Timer refreshTimer;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButtonView;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButtonFile;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButtonOptions;
        private System.Windows.Forms.ToolStripMenuItem jetztAktualisierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aktualisierungsrateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extremToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hochToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem niedrigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem angehaltenToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeaderRam;
        private System.Windows.Forms.ToolStripMenuItem multiselectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fehlermeldungenAnzeigenToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeaderStatus;
        private System.Windows.Forms.ListView listViewList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TabPage tabPageTile;
        private System.Windows.Forms.ListView listViewTile;
        private System.Windows.Forms.TabPage tabPageSmall;
        private System.Windows.Forms.ListView listViewSmall;
        private System.Windows.Forms.TabPage tabPageLarge;
        private System.Windows.Forms.ListView listViewLarge;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ToolStripMenuItem gruppenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem columnReorderToolStripMenuItem;
    }
}

