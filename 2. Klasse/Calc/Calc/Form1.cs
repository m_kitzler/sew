﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc
{
    public partial class Form1 : Form
    {
        static List<string> operations = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }
        private void numbutton_Click(object sender, EventArgs e)
        {
            if (labelNumber.ForeColor == Color.DeepSkyBlue)
            {
                labelNumber.ForeColor = SystemColors.ControlText;
                labelNumber.Text = "";
            }
            labelNumber.Text += (sender as Button).Text;
        }
        private void buttonEquals_Click(object sender, EventArgs e)
        {
            if (operations.Count > 0)
            {
                bool ignorelineoperations = true;
                if (labelNumber.Text.Length > 0)
                {
                    operations.Add(labelNumber.Text);
                }
                else if (!Char.IsDigit(operations[operations.Count - 1][0]))
                {
                    operations.RemoveAt(operations.Count - 1);
                }
                for (int i = 0; i < operations.Count; i++)
                {
                    if (!Char.IsDigit(operations[i][0]))
                    {
                        switch (operations[i][0])
                        {
                            case '*':
                                {
                                    double var1, var2;
                                    Double.TryParse(operations[i - 1], out var1); Double.TryParse(operations[i + 1], out var2);
                                    operations[i - 1] = (var1 * var2).ToString();
                                    operations.RemoveRange(i, 2); i--;
                                    break;
                                }
                            case '/':
                                {
                                    double var1, var2;
                                    Double.TryParse(operations[i - 1], out var1); Double.TryParse(operations[i + 1], out var2);
                                    operations[i - 1] = (var1 / var2).ToString();
                                    operations.RemoveRange(i, 2); i--;
                                    break;
                                }
                            case '+':
                                {
                                    if (!ignorelineoperations)
                                    {
                                        double var1, var2;
                                        Double.TryParse(operations[i - 1], out var1); Double.TryParse(operations[i + 1], out var2);
                                        operations[i - 1] = (var1 + var2).ToString();
                                        operations.RemoveRange(i, 2); i--;
                                    }
                                    break;
                                }
                            case '-':
                                {
                                    if (!ignorelineoperations)
                                    {
                                        double var1, var2;
                                        Double.TryParse(operations[i - 1], out var1); Double.TryParse(operations[i + 1], out var2);
                                        operations[i - 1] = (var1 - var2).ToString();
                                        operations.RemoveRange(i, 2); i--;
                                    }
                                    break;
                                }
                        }
                    }
                    if (ignorelineoperations && i == operations.Count - 1)
                    {
                        ignorelineoperations = false;
                        i = 0;
                    }
                }
                labelNumber.Text = operations[0];
                labelNumber.ForeColor = Color.DeepSkyBlue;
                textBoxChronik.Clear();
                operations.Clear();
            }
            else
            {
                labelNumber.ForeColor = Color.DeepSkyBlue;
            }
        }

        private void Operationbutton_Click(object sender, EventArgs e)
        {
            if (labelNumber.ForeColor == Color.DeepSkyBlue)
                labelNumber.ForeColor = SystemColors.ControlText;
            if (labelNumber.Text.Length > 0)
            {
                textBoxChronik.Text += labelNumber.Text + (sender as Button).Text;
                operations.Add(labelNumber.Text);
                operations.Add((sender as Button).Text);
                labelNumber.Text = "";
            }
        }

        private void buttonOneBack_Click(object sender, EventArgs e)
        {
            string helppls = labelNumber.Text;
            if (helppls != "")
            {
                helppls = helppls.Remove(helppls.Length - 1);
                labelNumber.Text = helppls;
            }
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            labelNumber.Text = "";
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            operations.Clear(); textBoxChronik.Clear(); labelNumber.Text = "";
        }

        private void focus_Enter(object sender, EventArgs e)
        {
            focusBox.Focus();
        }

        private void focusBox_KeyDown(object sender, KeyEventArgs e)
        {
#region quick and dirty
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        buttonEquals.PerformClick();
                        break;
                    }
                case Keys.Back:
                    {
                        buttonOneBack.PerformClick();
                        break;
                    }
                case Keys.Subtract:
                    {
                        buttonSub.PerformClick();
                        break;
                    }
                case Keys.Add:
                    {
                        buttonAdd.PerformClick();
                        break;
                    }
                case Keys.Divide:
                    {
                        buttonDivide.PerformClick();
                        break;
                    }
                case Keys.Multiply:
                    {
                        buttonMultiply.PerformClick();
                        break;
                    }
                case Keys.NumPad0:
                    {
                        button0.PerformClick();
                        break;
                    }
                case Keys.NumPad1:
                    {
                        button1.PerformClick();
                        break;
                    }
                case Keys.NumPad2:
                    {
                        button2.PerformClick();
                        break;
                    }
                case Keys.NumPad3:
                    {
                        button3.PerformClick();
                        break;
                    }
                case Keys.NumPad4:
                    {
                        button4.PerformClick();
                        break;
                    }
                case Keys.NumPad5:
                    {
                        button5.PerformClick();
                        break;
                    }
                case Keys.NumPad6:
                    {
                        button6.PerformClick();
                        break;
                    }
                case Keys.NumPad7:
                    {
                        button7.PerformClick();
                        break;
                    }
                case Keys.NumPad8:
                    {
                        button8.PerformClick();
                        break;
                    }
                case Keys.NumPad9:
                    {
                        button9.PerformClick();
                        break;
                    }
                case Keys.Decimal:
                    {
                        buttonComma.PerformClick();
                        break;
                    }
            }
#endregion
        }
    }
}
