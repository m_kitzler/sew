﻿namespace Calc
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.buttonDivide = new System.Windows.Forms.Button();
            this.buttonMultiply = new System.Windows.Forms.Button();
            this.buttonSub = new System.Windows.Forms.Button();
            this.buttonEquals = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.labelNumber = new System.Windows.Forms.Label();
            this.textBoxChronik = new System.Windows.Forms.TextBox();
            this.buttonComma = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonCE = new System.Windows.Forms.Button();
            this.buttonOneBack = new System.Windows.Forms.Button();
            this.focusBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 51);
            this.button1.TabIndex = 0;
            this.button1.TabStop = false;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.numbutton_Click);
            this.button1.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(69, 188);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(51, 51);
            this.button2.TabIndex = 1;
            this.button2.TabStop = false;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.numbutton_Click);
            this.button2.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(126, 188);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(51, 51);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.numbutton_Click);
            this.button3.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(12, 131);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(51, 51);
            this.button4.TabIndex = 3;
            this.button4.TabStop = false;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.numbutton_Click);
            this.button4.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(69, 131);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(51, 51);
            this.button5.TabIndex = 4;
            this.button5.TabStop = false;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.numbutton_Click);
            this.button5.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(126, 131);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(51, 51);
            this.button6.TabIndex = 5;
            this.button6.TabStop = false;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.numbutton_Click);
            this.button6.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(12, 74);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(51, 51);
            this.button7.TabIndex = 6;
            this.button7.TabStop = false;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.numbutton_Click);
            this.button7.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(69, 74);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(51, 51);
            this.button8.TabIndex = 7;
            this.button8.TabStop = false;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.numbutton_Click);
            this.button8.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(126, 74);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(51, 51);
            this.button9.TabIndex = 8;
            this.button9.TabStop = false;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.numbutton_Click);
            this.button9.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // button0
            // 
            this.button0.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button0.Location = new System.Drawing.Point(69, 245);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(51, 51);
            this.button0.TabIndex = 9;
            this.button0.TabStop = false;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.numbutton_Click);
            this.button0.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonDivide
            // 
            this.buttonDivide.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDivide.Location = new System.Drawing.Point(240, 74);
            this.buttonDivide.Name = "buttonDivide";
            this.buttonDivide.Size = new System.Drawing.Size(51, 51);
            this.buttonDivide.TabIndex = 10;
            this.buttonDivide.TabStop = false;
            this.buttonDivide.Text = "/";
            this.buttonDivide.UseVisualStyleBackColor = true;
            this.buttonDivide.Click += new System.EventHandler(this.Operationbutton_Click);
            this.buttonDivide.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonMultiply
            // 
            this.buttonMultiply.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMultiply.Location = new System.Drawing.Point(240, 131);
            this.buttonMultiply.Name = "buttonMultiply";
            this.buttonMultiply.Size = new System.Drawing.Size(51, 51);
            this.buttonMultiply.TabIndex = 11;
            this.buttonMultiply.TabStop = false;
            this.buttonMultiply.Text = "*";
            this.buttonMultiply.UseVisualStyleBackColor = true;
            this.buttonMultiply.Click += new System.EventHandler(this.Operationbutton_Click);
            this.buttonMultiply.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonSub
            // 
            this.buttonSub.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSub.Location = new System.Drawing.Point(240, 188);
            this.buttonSub.Name = "buttonSub";
            this.buttonSub.Size = new System.Drawing.Size(51, 51);
            this.buttonSub.TabIndex = 12;
            this.buttonSub.TabStop = false;
            this.buttonSub.Text = "-";
            this.buttonSub.UseVisualStyleBackColor = true;
            this.buttonSub.Click += new System.EventHandler(this.Operationbutton_Click);
            this.buttonSub.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonEquals
            // 
            this.buttonEquals.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEquals.Location = new System.Drawing.Point(126, 245);
            this.buttonEquals.Name = "buttonEquals";
            this.buttonEquals.Size = new System.Drawing.Size(108, 51);
            this.buttonEquals.TabIndex = 13;
            this.buttonEquals.TabStop = false;
            this.buttonEquals.Text = "=";
            this.buttonEquals.UseVisualStyleBackColor = true;
            this.buttonEquals.Click += new System.EventHandler(this.buttonEquals_Click);
            this.buttonEquals.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.Location = new System.Drawing.Point(240, 245);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(51, 51);
            this.buttonAdd.TabIndex = 14;
            this.buttonAdd.TabStop = false;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.Operationbutton_Click);
            this.buttonAdd.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // labelNumber
            // 
            this.labelNumber.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber.Location = new System.Drawing.Point(12, 35);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(278, 36);
            this.labelNumber.TabIndex = 16;
            // 
            // textBoxChronik
            // 
            this.textBoxChronik.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxChronik.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxChronik.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxChronik.Enabled = false;
            this.textBoxChronik.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxChronik.Location = new System.Drawing.Point(12, 12);
            this.textBoxChronik.Name = "textBoxChronik";
            this.textBoxChronik.Size = new System.Drawing.Size(278, 20);
            this.textBoxChronik.TabIndex = 17;
            this.textBoxChronik.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonComma
            // 
            this.buttonComma.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonComma.Location = new System.Drawing.Point(12, 245);
            this.buttonComma.Name = "buttonComma";
            this.buttonComma.Size = new System.Drawing.Size(51, 51);
            this.buttonComma.TabIndex = 18;
            this.buttonComma.TabStop = false;
            this.buttonComma.Text = ",";
            this.buttonComma.UseVisualStyleBackColor = true;
            this.buttonComma.Click += new System.EventHandler(this.numbutton_Click);
            this.buttonComma.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonC
            // 
            this.buttonC.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC.Location = new System.Drawing.Point(183, 188);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(51, 51);
            this.buttonC.TabIndex = 19;
            this.buttonC.TabStop = false;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            this.buttonC.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonCE
            // 
            this.buttonCE.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCE.Location = new System.Drawing.Point(183, 131);
            this.buttonCE.Name = "buttonCE";
            this.buttonCE.Size = new System.Drawing.Size(51, 51);
            this.buttonCE.TabIndex = 20;
            this.buttonCE.TabStop = false;
            this.buttonCE.Text = "CE";
            this.buttonCE.UseVisualStyleBackColor = true;
            this.buttonCE.Click += new System.EventHandler(this.buttonCE_Click);
            this.buttonCE.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // buttonOneBack
            // 
            this.buttonOneBack.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOneBack.Location = new System.Drawing.Point(183, 74);
            this.buttonOneBack.Name = "buttonOneBack";
            this.buttonOneBack.Size = new System.Drawing.Size(51, 51);
            this.buttonOneBack.TabIndex = 21;
            this.buttonOneBack.TabStop = false;
            this.buttonOneBack.Text = "←";
            this.buttonOneBack.UseVisualStyleBackColor = true;
            this.buttonOneBack.Click += new System.EventHandler(this.buttonOneBack_Click);
            this.buttonOneBack.Enter += new System.EventHandler(this.focus_Enter);
            // 
            // focusBox
            // 
            this.focusBox.Location = new System.Drawing.Point(219, 7);
            this.focusBox.Name = "focusBox";
            this.focusBox.Size = new System.Drawing.Size(0, 20);
            this.focusBox.TabIndex = 22;
            this.focusBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.focusBox_KeyDown);
            // 
            // Form1
            // 
            this.AcceptButton = this.buttonEquals;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 308);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.focusBox);
            this.Controls.Add(this.buttonOneBack);
            this.Controls.Add(this.buttonCE);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonComma);
            this.Controls.Add(this.textBoxChronik);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonEquals);
            this.Controls.Add(this.buttonSub);
            this.Controls.Add(this.buttonMultiply);
            this.Controls.Add(this.buttonDivide);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(318, 347);
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.Text = "Minisoft Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttonDivide;
        private System.Windows.Forms.Button buttonMultiply;
        private System.Windows.Forms.Button buttonSub;
        private System.Windows.Forms.Button buttonEquals;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.TextBox textBoxChronik;
        private System.Windows.Forms.Button buttonComma;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCE;
        private System.Windows.Forms.Button buttonOneBack;
        private System.Windows.Forms.TextBox focusBox;
    }
}

