﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;

namespace UnitTestProjectSVNr
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            SVNr a = new SVNr(123, new DateTime(1980, 1, 1));
            Assert.AreEqual("1237010180", a.Ausgabe());
        }
    }
}
