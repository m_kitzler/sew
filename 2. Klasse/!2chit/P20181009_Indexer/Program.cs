﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer_P09102018
{
    class Program
    {
        static void Main(string[] args)
        {
            MyArray<int> MA = new MyArray<int>(new int[]{0,10,3});
            Console.WriteLine("{0}, {1}, {2}",MA[0], MA[1], MA[2]);
            MA[2] = 50;
            Console.WriteLine("{0}, {1}, {2}", MA[0], MA[1], MA[2]);
        }
    }
}
