﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P20190402_HTL_Anmeldung
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            using (StreamReader sr = new StreamReader("Personen.login"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    listViewLogins.Items.Add(new ListViewItem(new string[] { line.Split(';')[0], line.Split(';')[1], line.Split(';')[2], line.Split(';')[3], line.Split(';')[4] }));
                }
            }
        }
    }
}
