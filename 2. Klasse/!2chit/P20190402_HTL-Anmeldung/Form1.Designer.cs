﻿namespace P20190402_HTL_Anmeldung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFN = new System.Windows.Forms.TextBox();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.textBoxLN = new System.Windows.Forms.TextBox();
            this.numericUpDownNumber = new System.Windows.Forms.NumericUpDown();
            this.radioButtonEWitk = new System.Windows.Forms.RadioButton();
            this.radioButtonEWitz = new System.Windows.Forms.RadioButton();
            this.radioButtonEWh = new System.Windows.Forms.RadioButton();
            this.radioButtonEWt = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.labelEW = new System.Windows.Forms.Label();
            this.labelZW = new System.Windows.Forms.Label();
            this.radioButtonZWt = new System.Windows.Forms.RadioButton();
            this.radioButtonZWh = new System.Windows.Forms.RadioButton();
            this.radioButtonZWitz = new System.Windows.Forms.RadioButton();
            this.radioButtonZWitk = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.textBoxAC = new System.Windows.Forms.TextBox();
            this.buttonReadLogins = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumber)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxFN
            // 
            this.textBoxFN.Location = new System.Drawing.Point(119, 12);
            this.textBoxFN.Name = "textBoxFN";
            this.textBoxFN.Size = new System.Drawing.Size(100, 20);
            this.textBoxFN.TabIndex = 0;
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(12, 15);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(52, 13);
            this.labelFirstName.TabIndex = 1;
            this.labelFirstName.Text = "Vorname:";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(12, 42);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(62, 13);
            this.labelLastName.TabIndex = 2;
            this.labelLastName.Text = "Nachname:";
            // 
            // textBoxLN
            // 
            this.textBoxLN.Location = new System.Drawing.Point(119, 39);
            this.textBoxLN.Name = "textBoxLN";
            this.textBoxLN.Size = new System.Drawing.Size(100, 20);
            this.textBoxLN.TabIndex = 3;
            // 
            // numericUpDownNumber
            // 
            this.numericUpDownNumber.Location = new System.Drawing.Point(137, 65);
            this.numericUpDownNumber.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.numericUpDownNumber.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.numericUpDownNumber.Name = "numericUpDownNumber";
            this.numericUpDownNumber.ReadOnly = true;
            this.numericUpDownNumber.Size = new System.Drawing.Size(82, 20);
            this.numericUpDownNumber.TabIndex = 4;
            this.numericUpDownNumber.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.numericUpDownNumber.Value = new decimal(new int[] {
            -1948823481,
            1,
            0,
            0});
            // 
            // radioButtonEWitk
            // 
            this.radioButtonEWitk.AutoSize = true;
            this.radioButtonEWitk.Location = new System.Drawing.Point(3, 3);
            this.radioButtonEWitk.Name = "radioButtonEWitk";
            this.radioButtonEWitk.Size = new System.Drawing.Size(67, 17);
            this.radioButtonEWitk.TabIndex = 5;
            this.radioButtonEWitk.TabStop = true;
            this.radioButtonEWitk.Text = "IT Krems";
            this.radioButtonEWitk.UseVisualStyleBackColor = true;
            this.radioButtonEWitk.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonEWitk.Click += new System.EventHandler(this.radioButtonEW_Click);
            // 
            // radioButtonEWitz
            // 
            this.radioButtonEWitz.AutoSize = true;
            this.radioButtonEWitz.Location = new System.Drawing.Point(3, 26);
            this.radioButtonEWitz.Name = "radioButtonEWitz";
            this.radioButtonEWitz.Size = new System.Drawing.Size(67, 17);
            this.radioButtonEWitz.TabIndex = 6;
            this.radioButtonEWitz.TabStop = true;
            this.radioButtonEWitz.Text = "IT Zwettl";
            this.radioButtonEWitz.UseVisualStyleBackColor = true;
            this.radioButtonEWitz.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonEWitz.Click += new System.EventHandler(this.radioButtonEW_Click);
            // 
            // radioButtonEWh
            // 
            this.radioButtonEWh.AutoSize = true;
            this.radioButtonEWh.Location = new System.Drawing.Point(3, 49);
            this.radioButtonEWh.Name = "radioButtonEWh";
            this.radioButtonEWh.Size = new System.Drawing.Size(69, 17);
            this.radioButtonEWh.TabIndex = 7;
            this.radioButtonEWh.TabStop = true;
            this.radioButtonEWh.Text = "Hochbau";
            this.radioButtonEWh.UseVisualStyleBackColor = true;
            this.radioButtonEWh.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonEWh.Click += new System.EventHandler(this.radioButtonEW_Click);
            // 
            // radioButtonEWt
            // 
            this.radioButtonEWt.AutoSize = true;
            this.radioButtonEWt.Location = new System.Drawing.Point(3, 72);
            this.radioButtonEWt.Name = "radioButtonEWt";
            this.radioButtonEWt.Size = new System.Drawing.Size(61, 17);
            this.radioButtonEWt.TabIndex = 8;
            this.radioButtonEWt.TabStop = true;
            this.radioButtonEWt.Text = "Tiefbau";
            this.radioButtonEWt.UseVisualStyleBackColor = true;
            this.radioButtonEWt.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonEWt.Click += new System.EventHandler(this.radioButtonEW_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Telefonnummer:";
            // 
            // labelEW
            // 
            this.labelEW.AutoSize = true;
            this.labelEW.Location = new System.Drawing.Point(9, 92);
            this.labelEW.Name = "labelEW";
            this.labelEW.Size = new System.Drawing.Size(50, 13);
            this.labelEW.TabIndex = 14;
            this.labelEW.Text = "Erstwahl:";
            // 
            // labelZW
            // 
            this.labelZW.AutoSize = true;
            this.labelZW.Location = new System.Drawing.Point(145, 92);
            this.labelZW.Name = "labelZW";
            this.labelZW.Size = new System.Drawing.Size(58, 13);
            this.labelZW.TabIndex = 15;
            this.labelZW.Text = "Zweitwahl:";
            // 
            // radioButtonZWt
            // 
            this.radioButtonZWt.AutoSize = true;
            this.radioButtonZWt.Location = new System.Drawing.Point(3, 72);
            this.radioButtonZWt.Name = "radioButtonZWt";
            this.radioButtonZWt.Size = new System.Drawing.Size(61, 17);
            this.radioButtonZWt.TabIndex = 8;
            this.radioButtonZWt.TabStop = true;
            this.radioButtonZWt.Text = "Tiefbau";
            this.radioButtonZWt.UseVisualStyleBackColor = true;
            this.radioButtonZWt.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonZWt.Click += new System.EventHandler(this.radioButtonZW_Click);
            // 
            // radioButtonZWh
            // 
            this.radioButtonZWh.AutoSize = true;
            this.radioButtonZWh.Location = new System.Drawing.Point(3, 49);
            this.radioButtonZWh.Name = "radioButtonZWh";
            this.radioButtonZWh.Size = new System.Drawing.Size(69, 17);
            this.radioButtonZWh.TabIndex = 7;
            this.radioButtonZWh.TabStop = true;
            this.radioButtonZWh.Text = "Hochbau";
            this.radioButtonZWh.UseVisualStyleBackColor = true;
            this.radioButtonZWh.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonZWh.Click += new System.EventHandler(this.radioButtonZW_Click);
            // 
            // radioButtonZWitz
            // 
            this.radioButtonZWitz.AutoSize = true;
            this.radioButtonZWitz.Location = new System.Drawing.Point(3, 26);
            this.radioButtonZWitz.Name = "radioButtonZWitz";
            this.radioButtonZWitz.Size = new System.Drawing.Size(67, 17);
            this.radioButtonZWitz.TabIndex = 6;
            this.radioButtonZWitz.TabStop = true;
            this.radioButtonZWitz.Text = "IT Zwettl";
            this.radioButtonZWitz.UseVisualStyleBackColor = true;
            this.radioButtonZWitz.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonZWitz.Click += new System.EventHandler(this.radioButtonZW_Click);
            // 
            // radioButtonZWitk
            // 
            this.radioButtonZWitk.AutoSize = true;
            this.radioButtonZWitk.Location = new System.Drawing.Point(3, 3);
            this.radioButtonZWitk.Name = "radioButtonZWitk";
            this.radioButtonZWitk.Size = new System.Drawing.Size(67, 17);
            this.radioButtonZWitk.TabIndex = 5;
            this.radioButtonZWitk.TabStop = true;
            this.radioButtonZWitk.Text = "IT Krems";
            this.radioButtonZWitk.UseVisualStyleBackColor = true;
            this.radioButtonZWitk.CheckedChanged += new System.EventHandler(this.radioButton_Disable);
            this.radioButtonZWitk.Click += new System.EventHandler(this.radioButtonZW_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonEWitk);
            this.panel1.Controls.Add(this.radioButtonEWitz);
            this.panel1.Controls.Add(this.radioButtonEWh);
            this.panel1.Controls.Add(this.radioButtonEWt);
            this.panel1.Location = new System.Drawing.Point(12, 108);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 93);
            this.panel1.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButtonZWitk);
            this.panel2.Controls.Add(this.radioButtonZWitz);
            this.panel2.Controls.Add(this.radioButtonZWh);
            this.panel2.Controls.Add(this.radioButtonZWt);
            this.panel2.Location = new System.Drawing.Point(148, 108);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(71, 93);
            this.panel2.TabIndex = 17;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(144, 207);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 18;
            this.buttonSubmit.Text = "Abschicken";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // textBoxAC
            // 
            this.textBoxAC.Location = new System.Drawing.Point(107, 65);
            this.textBoxAC.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.textBoxAC.Name = "textBoxAC";
            this.textBoxAC.Size = new System.Drawing.Size(30, 20);
            this.textBoxAC.TabIndex = 19;
            this.textBoxAC.Text = "+43";
            // 
            // buttonReadLogins
            // 
            this.buttonReadLogins.Location = new System.Drawing.Point(12, 207);
            this.buttonReadLogins.Name = "buttonReadLogins";
            this.buttonReadLogins.Size = new System.Drawing.Size(83, 23);
            this.buttonReadLogins.TabIndex = 20;
            this.buttonReadLogins.Text = "Anmeldungen";
            this.buttonReadLogins.UseVisualStyleBackColor = true;
            this.buttonReadLogins.Click += new System.EventHandler(this.buttonReadLogins_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.buttonSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 237);
            this.Controls.Add(this.buttonReadLogins);
            this.Controls.Add(this.textBoxAC);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelZW);
            this.Controls.Add(this.labelEW);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownNumber);
            this.Controls.Add(this.textBoxLN);
            this.Controls.Add(this.labelLastName);
            this.Controls.Add(this.labelFirstName);
            this.Controls.Add(this.textBoxFN);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTL Anmeldung";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumber)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFN;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.TextBox textBoxLN;
        private System.Windows.Forms.NumericUpDown numericUpDownNumber;
        private System.Windows.Forms.RadioButton radioButtonEWitk;
        private System.Windows.Forms.RadioButton radioButtonEWitz;
        private System.Windows.Forms.RadioButton radioButtonEWh;
        private System.Windows.Forms.RadioButton radioButtonEWt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelEW;
        private System.Windows.Forms.Label labelZW;
        private System.Windows.Forms.RadioButton radioButtonZWt;
        private System.Windows.Forms.RadioButton radioButtonZWh;
        private System.Windows.Forms.RadioButton radioButtonZWitz;
        private System.Windows.Forms.RadioButton radioButtonZWitk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.TextBox textBoxAC;
        private System.Windows.Forms.Button buttonReadLogins;
    }
}

