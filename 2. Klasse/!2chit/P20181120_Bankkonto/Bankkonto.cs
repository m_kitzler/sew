﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20112018_Bankkonto
{
    abstract class Bankkonto
    {
        protected int kontoNr;
        protected double betrag;
        public Bankkonto() { }
        public Bankkonto(int knr)
        {
            kontoNr = knr;
        }
        public void Einzahlen(double wieviel)
        {
            betrag += wieviel;
        }
        public void Auszahlen(double wieviel)
        {
            betrag -= wieviel;
        }
        public double Stand()
        {
            return betrag;
        }
        public abstract void Verzinsen(); //Jede Klasse muss diese Methode selber implementieren
    }
}
