﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20112018_Bankkonto
{
    class Girokonto:Bankkonto
    {
        public double Überziehung { get;
            set {
                if (value > 0)
                {
                    Überziehung = 0 - value;
                }
                else
                {
                    Überziehung = value;
                }
            };
        }
        public Girokonto(int KNr) : base(KNr) { }
        public override void Auszahlen(double wieviel)
        {
            if (!(abgehoben + wieviel > maxBetrag) && betrag - wieviel > 0)
            {
                betrag -= wieviel;
            }
            else
            {
                throw new Exception("Aha illegal");
            }
        }
    }
}
