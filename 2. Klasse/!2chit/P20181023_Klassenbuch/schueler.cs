﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassenbuch_P23102018
{
    class Schueler
    {
        string name;
        DateTime age;
        public Schueler (string Name, DateTime Geburtsdatum)
        {
            name = Name; age = Geburtsdatum;
        }
        public int GetAge ()
        {
            return DateTime.Today.Year - age.Year;
        }
        public string GetName()
        {
            return name;
        }
    }
}
