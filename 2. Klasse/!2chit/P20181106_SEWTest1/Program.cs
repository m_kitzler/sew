﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEWTest1_P06112018
{
    class Program
    {
        static void Main(string[] args)
        {
            /*1.)
            Eine Klasse verweist auf Variablen, Strukturen kopieren sich die Variable.*/
            //2.)
            Person p = new Person("Vorname","Nachname");
            //3.)
            //Siehe Klasse Person
            //4.)
            Circle c = new Circle();
            //7.)

            //9.)
            HTL.Lsr = "Hallo Welt";
            HTL htl = new HTL();
            Console.WriteLine(htl);
            //10.)
            //Siehe Klasse Auto
            //11.)
            Auto a = new Auto { Marke = "Porsche" };
            //12.)
            p.Vn = "Max";
            p.Nn = "Mustermann";
            Console.WriteLine(p.FullName);
            //13.)
            Test(0, 1, 2, 3);
        }
        static void Test(params int[] numbers)
        {
            foreach (int i in numbers)
            {
                Console.WriteLine(i);
            }
        }
    }
}
