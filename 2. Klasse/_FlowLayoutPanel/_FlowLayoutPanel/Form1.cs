﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _FlowLayoutPanel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Add(new Label { Text = textBox1.Text });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            flowLayoutPanel1.Controls.Add(new Button { Text = textBox2.Text, Size = new System.Drawing.Size(r.Next(10,100), r.Next(10, 100)) });
        }

        private void button3_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Add(new TextBox { Text = textBox3.Text });
        }
    }
}
