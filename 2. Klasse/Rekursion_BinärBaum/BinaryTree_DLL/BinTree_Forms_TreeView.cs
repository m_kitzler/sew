﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rekursion_BinärBaum
{
    public class BinTree_Forms_TreeView : BinTree
    {
        public BinTree_Forms_TreeView(Element rootElement) : base(rootElement) { }
        /// <summary>
        /// Fügt den Binärbaum in Form von Labels in die angegebene Control ein.
        /// </summary>
        /// <param name="visTarget"></param>
        /// <returns></returns>
        public TreeView Visualize(TreeView visTarget)
        {
            TreeNode t = Visualize(new TreeNode(root.ToString()), root, "");
            visTarget.Nodes.Add(t);
            return visTarget;
        }
        private TreeNode Visualize(TreeNode visTarget, Element currElement, string pos)
        {
            TreeNode t = new TreeNode(pos + currElement.value.ToString());
            if (currElement.Left != null)
            {
                t.Nodes.Add(Visualize(t, currElement.Left, "↓ "));
            }
            if (currElement.Right != null)
            {
                t.Nodes.Add(Visualize(t, currElement.Right, "↑ "));
            }
            visTarget.Nodes.Add(t);
            return visTarget;
        }
    }
}
