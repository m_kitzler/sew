﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion_BinärBaum
{
    public class BinTree
    {
        protected Element root;
        public BinTree(Element rootElement)
        {
            root = rootElement;
            root.Location = "0";
        }
        /// <summary>
        /// Fügt ein Element dem Baum hinzu.
        /// </summary>
        /// <param name="toAdd"></param>
        public void Add(Element toAdd)
        {
            root = AddRec(toAdd, root, root.Location);
        }
        private Element AddRec(Element toAdd, Element target, string path)
        {
            if (toAdd.value > target.value)
            {
                path += "1";
                if (target.Right == null)
                {
                    toAdd.Location = path;
                    target.Right = toAdd;
                }
                else
                {
                    target.Right = AddRec(toAdd, target.Right, path);
                }
            }
            else
            {
                path += "0";
                if (target.Left == null)
                {
                    toAdd.Location = path;
                    target.Left = toAdd;
                }
                else
                {
                    target.Left = AddRec(toAdd, target.Left, path);
                }
            }
            return target;
        }

        public int GetDepth()
        {
            return GetDepth(root);
        }
        private int GetDepth(Element currElement)
        {
            int depth = 0;
            if (currElement.Left != null)
                depth = GetDepth(currElement.Left);
            if (currElement.Right != null)
            {
                int temp = GetDepth(currElement.Right);
                if (temp > depth)
                    depth = temp;
            }
            return depth + 1;
        }
    }
}
