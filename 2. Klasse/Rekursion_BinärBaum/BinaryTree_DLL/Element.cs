﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion_BinärBaum
{
    public class Element
    {
        public Element Left { get; set; }
        public Element Right { get; set; }
        public string Location { get; set; }
        public int value { get; }
        public Element(int value)
        {
            this.value = value;
        }
    }
}
