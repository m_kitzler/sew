﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rekursion_BinärBaum
{
    public class BinTree_Forms : BinTree
    {
        public BinTree_Forms(Element rootElement) : base(rootElement) { }
        /// <summary>
        /// Fügt den Binärbaum in Form von Labels in die angegebene Control ein.
        /// </summary>
        /// <param name="visTarget"></param>
        /// <returns></returns>
        public Panel Visualize(Panel visTarget)
        {
            return Visualize(visTarget, root);
        }
        private Panel Visualize(Panel visTarget, Element currElement)
        {
            if (currElement.Left != null)
            {
                visTarget = Visualize(visTarget, currElement.Left);
            }
            Label l = new Label();
            l.Name = "test" + currElement.value;
            l.Text = currElement.value.ToString();
            l.Font = new System.Drawing.Font("Microsoft Sans Serif", 10);
            l.Location = CalcLocation(currElement);
            l.AutoSize = true;
            l.Parent = visTarget;
            l.BorderStyle = BorderStyle.Fixed3D;
            visTarget.Controls.Add(l);
            if (currElement.Right != null)
            {
                visTarget = Visualize(visTarget, currElement.Right);
            }
            return visTarget;
        }
        /// <summary>
        /// Berechnet die Position eines Elements.
        /// </summary>
        /// <param name="el"></param>
        /// <returns></returns>
        public System.Drawing.Point CalcLocation(Element el)
        {
            int row = el.Location.Length;
            int rowPosition = Convert.ToInt32(el.Location, 2) + 1;
            int margin = 1000 / ((int)Math.Pow(2, row));
            return new System.Drawing.Point((margin + ((rowPosition - 1) * (2 * margin))) - 12, (30 * (row - 1)));
        }
    }
}
