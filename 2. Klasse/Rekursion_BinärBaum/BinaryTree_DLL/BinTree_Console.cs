﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion_BinärBaum
{
    public class BinTree_Console : BinTree
    {
        public BinTree_Console(Element rootElement) : base(rootElement) { }
        /// <summary>
        /// Gibt den Binärbaum in die Konsole aus.
        /// </summary>
        public void Visualize()
        {
            Console.SetBufferSize(1000, 30);
            int depth = GetDepth();
            Visualize(root, depth);
            Console.SetCursorPosition(0, depth);
        }
        private void Visualize(Element currElement, int depth)
        {
            if (currElement.Left != null)
            {
                Visualize(currElement.Left, depth);
            }
            if (currElement.Right != null)
            {
                Visualize(currElement.Right, depth);
            }

            int pos = Convert.ToInt32(currElement.Location, 2);
            int level = currElement.Location.Length;

            const int oneSpace = 2;
            int space = (int)((Math.Pow(2, depth - level) - 1) * oneSpace);

            Console.SetCursorPosition(space + (oneSpace + space) * 2 * pos, level - 1);
            Console.Write(currElement.value.ToString("D2"));

            int curPos = space + (oneSpace + space) * pos;
        }
    }
}
