﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rekursion_BinärBaum;

namespace FormsTree
{
    public partial class Form1 : Form
    {
        BinTree_Forms bt = new BinTree_Forms(new Element(55));
        public Form1()
        {
            InitializeComponent();
            bt.Add(new Element(43));
            bt.Add(new Element(69));    //LOL
            bt.Add(new Element(34));
            bt.Add(new Element(77));
            bt.Add(new Element(12));
            bt.Add(new Element(45));
            bt.Add(new Element(68));
            bt.Add(new Element(37));
            bt.Add(new Element(55));
            panel1 = bt.Visualize(panel1);
        }
    }
}
