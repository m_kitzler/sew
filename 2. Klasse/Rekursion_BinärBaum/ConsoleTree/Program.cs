﻿using System;
using Rekursion_BinärBaum;

namespace ConsoleTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinTree_Console bt = new BinTree_Console(new Element(55));
            bt.Add(new Element(43));
            bt.Add(new Element(69));
            bt.Add(new Element(34));
            bt.Add(new Element(77));
            bt.Add(new Element(12));
            bt.Add(new Element(45));
            bt.Add(new Element(68));
            bt.Add(new Element(37));
            bt.Add(new Element(55));
            bt.Visualize();

            while (true)
            {
                int newInt;
                if (Int32.TryParse(Console.ReadLine(), out newInt))
                {
                    bt.Add(new Element(newInt));
                    Console.Clear();
                    bt.Visualize();
                }
                else
                {
                    Console.WriteLine("Ungültige Zahl");
                }
            }
        }
    }
}
