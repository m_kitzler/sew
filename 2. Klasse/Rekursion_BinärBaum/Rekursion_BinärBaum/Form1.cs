﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rekursion_BinärBaum
{
    public partial class Form1 : Form
    {
        int numberOfRows = 0;
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Berechnet die Position die ein Element in Form eines Labels haben sollte.
        /// </summary>
        /// <param name="row">Reihe</param>
        /// <param name="rowPosition">Position in Reihe</param>
        /// <returns>Vertikale Position und Margin des Elements</returns>
        public int calcLocX(int row, int rowPosition)
        {
            int margin = (32 * ((numberOfRows - row) + 1)) - 12;
            return margin + (24 + (margin * 2)) * (rowPosition - 1);
        }
        public int calcLocY(int row)
        {
            return 30 * (row - 1);
        }
    }
}
