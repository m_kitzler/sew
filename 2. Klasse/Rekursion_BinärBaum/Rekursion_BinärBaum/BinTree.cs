﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion_BinärBaum
{
    class BinTree
    {
        private Element root;
        public BinTree(Element rootElement)
        {
            root = rootElement;
        }
        public void Add(Element toAdd)
        {
            root = AddRec(toAdd, root);
        }
        private Element AddRec(Element toAdd, Element target)
        {
            if (toAdd.value > target.value)
            {
                if (target.Right == null)
                {
                    target.Right = toAdd;
                }
                else
                {
                    target.Right = AddRec(toAdd, target.Right);
                }
            }
            else
            {
                if (target.Left == null)
                {
                    target.Left = toAdd;
                }
                else
                {
                    target.Left = AddRec(toAdd, target.Left);
                }
            }
            return target;
        }
    }
}
