﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion_BinärBaum
{
    class Element
    {
        public Element Left { get; set; }
        public Element Right { get; set; }
        public int value { get; }
        public Element(int value)
        {
            if (value < 100 && value >= 0)
            {
                this.value = value;
            }
            else
            {
                throw new Exception("Wert von Element muss im positiven, zweistelligen Bereich sein");
            }
        }
    }
}
