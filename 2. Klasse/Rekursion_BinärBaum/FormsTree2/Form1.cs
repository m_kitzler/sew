﻿using Rekursion_BinärBaum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsTree2
{
    public partial class Form1 : Form
    {
        BinTree_Forms_TreeView bt = null;
        Form2 f2 = new Form2();
        //↑↓
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (bt == null)
            {
                bt = new BinTree_Forms_TreeView(new Element((int)numericUpDownElement.Value));
            }
            else
            {
                bt.Add(new Element((int)numericUpDownElement.Value));
            }
            treeViewPreview.Nodes.Add(numericUpDownElement.Value.ToString());
            treeViewPreview.Sort();
        }

        private void ButtonVis_Click(object sender, EventArgs e)
        {
            TreeView hlp = (TreeView)f2.Controls.Find("treeViewBinTree", false)[0];
            f2.Controls.Find("treeViewBinTree", false)[0] = bt.Visualize(hlp);
            f2.ShowDialog();
        }

        private void NumericUpDownElement_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ButtonAdd_Click(sender, e);
            }
        }
    }
}
