﻿namespace FormsTree2
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonVis = new System.Windows.Forms.Button();
            this.treeViewPreview = new System.Windows.Forms.TreeView();
            this.numericUpDownElement = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownElement)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(212, 7);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 2;
            this.buttonAdd.Text = "Hinzufügen";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // buttonVis
            // 
            this.buttonVis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVis.Location = new System.Drawing.Point(12, 214);
            this.buttonVis.Name = "buttonVis";
            this.buttonVis.Size = new System.Drawing.Size(275, 42);
            this.buttonVis.TabIndex = 3;
            this.buttonVis.Text = "Visualisieren";
            this.buttonVis.UseVisualStyleBackColor = true;
            this.buttonVis.Click += new System.EventHandler(this.ButtonVis_Click);
            // 
            // treeViewPreview
            // 
            this.treeViewPreview.Location = new System.Drawing.Point(12, 38);
            this.treeViewPreview.Name = "treeViewPreview";
            this.treeViewPreview.Size = new System.Drawing.Size(275, 170);
            this.treeViewPreview.TabIndex = 4;
            // 
            // numericUpDownElement
            // 
            this.numericUpDownElement.Location = new System.Drawing.Point(12, 9);
            this.numericUpDownElement.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownElement.Name = "numericUpDownElement";
            this.numericUpDownElement.Size = new System.Drawing.Size(194, 20);
            this.numericUpDownElement.TabIndex = 5;
            this.numericUpDownElement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownElement_KeyDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 268);
            this.Controls.Add(this.numericUpDownElement);
            this.Controls.Add(this.treeViewPreview);
            this.Controls.Add(this.buttonVis);
            this.Controls.Add(this.buttonAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownElement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonVis;
        private System.Windows.Forms.TreeView treeViewPreview;
        private System.Windows.Forms.NumericUpDown numericUpDownElement;
    }
}

