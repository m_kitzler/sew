﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    sealed class MarkeEigenbau : Raumschiff
    {
        public MarkeEigenbau(int Lebenspunkte, int Panzerung, int Schaden):base(Lebenspunkte)
        {
            if((Panzerung + Schaden) <= 50 && (Panzerung + Schaden) >= 0)
            {
                hull = Panzerung;
                dmg = Schaden;
                cost = dmg + hull + (Lebenspunkte / 10);
            }
        }
    }
}
