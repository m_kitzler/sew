﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    sealed class Allrounder : Raumschiff
    {
        public Allrounder(int Lebenspunkte) : base(Lebenspunkte)
        {
            hull = 20;
            dmg = 20;
            cost = (dmg + hull + (Lebenspunkte / 10)) - 10;
        }
    }
}
