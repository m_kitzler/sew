﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    sealed class Kampfschiff : Raumschiff
    {
        public Kampfschiff(int Lebenspunkte):base(Lebenspunkte)
        {
            hull = 10;
            dmg = 40;
            cost = (dmg + hull + (Lebenspunkte / 10)) - 10;
        }
    }
}
