﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StarWar
{
    class StarWar
    {
        Flotte f1, f2;
        public Flotte Flotte1
        {
            private get { return f1; }
            set {
                if (value.Cost <= MaxCost)
                {
                    Console.WriteLine("Flotte \"{0}\" ist Kampfbereit, Kosten:{1}", value.GetName, value.Cost);
                    f1 = value;
                }
                else
                {
                    throw new Exception("Flotte \"" + value.GetName + "\" ist zu teuer. Sie muss weniger als " + MaxCost + " kosten. Aktuelle Kosten: " + value.Cost);
                }
            }
        }
        public Flotte Flotte2
        {
            private get { return f2; }
            set
            {
                if (value.Cost <= MaxCost)
                {
                    Console.WriteLine("Flotte \"{0}\" ist Kampfbereit, Kosten:{1}", value.GetName, value.Cost);
                    f2 = value;
                }
                else
                {
                    throw new Exception("Flotte \"" + value.GetName + "\" ist zu teuer. Sie muss weniger als " + MaxCost + " kosten. Aktuelle Kosten: " + value.Cost);
                }
            }
        }
        bool log = false;
        int MaxCost = 255;
        public void ShowLog(bool b)
        {
            log = b;
        }
        public Flotte PInput()
        {
            int cost = 0, auswNow = 0, hlp = 0;
            string name = "Rebellen";
            string[] schiffe = {"Allrounder","Kampfschiff","Versorgungsschiff","Marke Eigenbau"};
            Raumschiff[] flotte = new Raumschiff[5];
            int[,] ausw = new int[,] { { 0, 50, 1, 1 }, { 0, 50, 1, 1 }, { 0, 50, 1, 1 }, { 0, 50, 1, 1 }, { 0, 50, 1, 1 } };
            while (true)
            {
                for (int i = 0; i < 5; i++)
                {
                    cost += ((ausw[i, 1] / 10) + (ausw[i,0] == 3 ? (ausw[i, 2] + ausw[i, 3]) : 40));
                }
                #region Print
                Console.Clear();
                Console.Write("Flotte erstellen, A/D: -/+10, Kosten:");
                if (cost <= 255)
                    Console.WriteLine(cost + "\n");
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(cost + "\n");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                cost = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (auswNow == hlp)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write("1. Schiff:");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write("1. Schiff:");
                    }
                    Console.WriteLine(" " + schiffe[ausw[i, 0]]);
                    hlp++;
                    if (auswNow == hlp)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write("HP:");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write("HP:");
                    }
                    Console.WriteLine(" " + ausw[i, 1]);
                    hlp++;
                    if (auswNow == hlp)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write("Schaden:");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (ausw[i,0] != 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("Schaden:");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write("Schaden:");
                    }
                    switch (ausw[i, 0])
                    {
                        case 0:
                            Console.WriteLine(" 25");
                            break;
                        case 1:
                            Console.WriteLine(" 40");
                            break;
                        case 2:
                            Console.WriteLine(" 10");
                            break;
                        case 3:
                            Console.WriteLine(" " + ausw[i, 2]);
                            break;
                    }
                    hlp++;
                    if (auswNow == hlp)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write("Panzerung:");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (ausw[i, 0] != 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("Panzerung:");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write("Panzerung:");
                    }
                    switch (ausw[i, 0])
                    {
                        case 0:
                            Console.WriteLine(" 25");
                            break;
                        case 1:
                            Console.WriteLine(" 10");
                            break;
                        case 2:
                            Console.WriteLine(" 40");
                            break;
                        case 3:
                            Console.WriteLine(" " + ausw[i, 3]);
                            break;
                    }
                    hlp++;
                }
                if (auswNow == hlp)
                {
                    Console.CursorVisible = true;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write("Name:");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.CursorVisible = false;
                    Console.Write("Name:");
                }
                Console.Write(" " + name);
                hlp = 0;
                #endregion
                #region Input
                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (auswNow < 20)
                            auswNow++;
                        else
                            auswNow = 0;
                        break;
                    case ConsoleKey.UpArrow:
                        if (auswNow > 0)
                            auswNow--;
                        else
                            auswNow = 20;
                        break;
                    case ConsoleKey.LeftArrow:
                        switch (auswNow)
                        {
                            case 0:
                            case 4:
                            case 8:
                            case 12:
                            case 16:
                                if (ausw[auswNow / 4, 0] > 0)
                                    ausw[auswNow / 4, 0]--;
                                else
                                    ausw[auswNow / 4, 0] = 3;
                                break;
                            case 20:break;
                            default:
                                if (ausw[auswNow / 4, auswNow % 4] > 1)
                                    ausw[auswNow / 4, auswNow % 4]--;
                                break;
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        switch (auswNow)
                        {
                            case 0:
                            case 4:
                            case 8:
                            case 12:
                            case 16:
                                if (ausw[auswNow / 4, 0] < 3)
                                    ausw[auswNow / 4, 0]++;
                                else
                                    ausw[auswNow / 4, 0] = 0;
                                break;
                            case 20: break;
                            default:
                                if (ausw[auswNow / 4, auswNow % 4] < 200)
                                    ausw[auswNow / 4, auswNow % 4]++;
                                break;
                        }
                        break;

                    case ConsoleKey.Backspace:
                        if (name.Length > 0)
                            name = name.Remove(name.Length - 1);
                        break;
                    default:
                        if (auswNow == 20)
                        {
                            name += key.KeyChar;
                        }
                        break;
                }
                #endregion
            }
            Flotte f1 = new Flotte(name, flotte);
            return null;
        }
        public void Fight()
        {

        }
        /// <summary>
        /// Lässt die Flotten zuende kämpfen. Feinde werden Zufällig ausgewählt.
        /// </summary>
        /// <returns></returns>
        public string FightToEnd()
        {
            Random r = new Random();
            while (true)
            {
                for (int i = 0; i < 5; i++)
                {
                    int hlp = 0;
                    if (f1.Destroyed == true)
                    {
                        return f2.GetName + " hat gewonnen";
                    }
                    else
                    {
                        while (true)
                        {
                            hlp = r.Next(0, 5);
                            if (f2.Ship(hlp).dead == false)
                            {
                                if (log)
                                {
                                    Console.WriteLine("Schiff {0} von {1} hat Schiff {2} von {3} angegriffen!\nSchaden: {4}, HP: {5}", i + 1, f1.GetName, hlp + 1, f2.GetName, f1.Ship(i).Shoot(f2.Ship(hlp)), f2.Ship(hlp).HP);
                                    Thread.Sleep(250);
                                }
                                else
                                {
                                    f1.Ship(i).Shoot(f2.Ship(hlp));
                                }
                                break;
                            }
                        }
                    }
                    if (f2.Destroyed == true)
                    {
                        return f1.GetName + " hat gewonnen";
                    }
                    else
                    {
                        while (true)
                        {
                            hlp = r.Next(0, 5);
                            if (f1.Ship(hlp).dead == false)
                            {
                                if (log)
                                {
                                    Console.WriteLine("Schiff {0} von {1} hat Schiff {2} von {3} angegriffen!\nSchaden: {4}, HP: {5}", i + 1, f2.GetName, hlp + 1, f1.GetName, f2.Ship(i).Shoot(f1.Ship(hlp)), f1.Ship(hlp).HP);
                                    Thread.Sleep(250);
                                }
                                else
                                {
                                    f2.Ship(i).Shoot(f1.Ship(hlp));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return default(string);
        }
    }
}
