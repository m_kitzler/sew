﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    sealed class Versorgungsschiff : Raumschiff
    {
        public Versorgungsschiff(int Lebenspunkte):base(Lebenspunkte)
        {
            hull = 40;
            dmg = 10;
            cost = (dmg + hull + (Lebenspunkte / 10)) - 10;
        }
    }
}
