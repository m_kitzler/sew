﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    abstract class Raumschiff
    {
        protected bool isDED = false;
        protected float hp;
        protected int hull,dmg,cost;
        public Raumschiff(int Lebenspunkte)
        {
            if (Lebenspunkte <= 250 && Lebenspunkte > 0)
            {
                hp = Lebenspunkte;
            }
            else
            {
                throw new Exception("Die Lebenspunkte müssen zwischen 1 und 250 liegen");
            }
        }
        public int Shoot(Raumschiff x)
        {
            return x.Hit(dmg);
        }
        public int Hit(int DMG)
        {
            int hlp = DMG * (1 - (hull / 100));
            hp -= hlp;
            if (hp <= 0)
            {
                isDED = true;
            }
            return hlp;
        }
        public float HP
        {
            get { return hp; }
        }
        public int HULL
        {
            get { return hull; }
        }
        public int DMG
        {
            get { return dmg; }
        }
        public int Cost
        {
            get { return cost; }
        }
        public bool dead
        {
            get { return isDED; }
        }
    }
}
