﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    class Program
    {
        static void Main(string[] args)
        {
            StarWar sw = new StarWar();
            Flotte f1 = new Flotte("Imperium", new Kampfschiff(150), new Versorgungsschiff(150), new MarkeEigenbau(100, 20, 20), new MarkeEigenbau(200, 10, 10), new MarkeEigenbau(100, 10, 35));
            sw.Flotte1 = f1;
            if (true) //Playerinput
            {
                sw.Flotte2 = sw.PInput();
                sw.Fight();
            }
            else
            {
                sw.ShowLog(true);
                Flotte f2 = new Flotte("Rebellen", new Kampfschiff(100), new Versorgungsschiff(200), new MarkeEigenbau(100,20,20), new MarkeEigenbau(200, 10, 10), new MarkeEigenbau(100, 10, 35));
                sw.Flotte2 = f2;
                Console.ReadKey(true);
                Console.WriteLine(sw.FightToEnd());
            }
        }
    }
}
