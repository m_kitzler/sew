﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWar
{
    class Flotte
    {
        string name;
        Raumschiff[] flotte = new Raumschiff[5];
        public Flotte (string Name,Raumschiff r1, Raumschiff r2, Raumschiff r3, Raumschiff r4, Raumschiff r5)
        {
            name = Name;
            flotte[0] = r1;
            flotte[1] = r2;
            flotte[2] = r3;
            flotte[3] = r4;
            flotte[4] = r5;
        }
        public Flotte(string Name, Raumschiff[] raumschiffe)
        {
            name = Name;
            flotte[0] = raumschiffe[0];
            flotte[1] = raumschiffe[1];
            flotte[2] = raumschiffe[2];
            flotte[3] = raumschiffe[3];
            flotte[4] = raumschiffe[4];
        }
        public string GetName
        {
            get { return name; }
        }
        public int Cost
        {
            get { return flotte[0].Cost + flotte[1].Cost + flotte[2].Cost + flotte[3].Cost + flotte[4].Cost; }
        }
        public Raumschiff Ship(int Indexer)
        {
            return flotte[Indexer];
        }
        public bool Destroyed
        {
            get
            {
                if (flotte[0].dead && flotte[1].dead && flotte[2].dead && flotte[3].dead && flotte[4].dead)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
