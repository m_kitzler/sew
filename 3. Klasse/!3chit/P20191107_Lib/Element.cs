﻿using System;

namespace P20191107_Lib
{
    public class Element<T> : IComparable where T : IComparable
    {
        public T Value { get; set; }
        public Element<T> NextElement { get; set; }
        public Element(T value)
        {
            Value = value;
        }

        public int CompareTo(object obj)
        {
            if (Value.GetType() == obj.GetType())
                return Value.CompareTo((obj as Element<T>).Value);
            throw new StackOverflowException();
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
