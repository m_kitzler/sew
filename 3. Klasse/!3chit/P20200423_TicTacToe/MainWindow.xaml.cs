﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace P20200423_TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string player = "X";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Resources["field" + (sender as TextBlock).Tag] = player;
            player = (player == "O") ? "X" : "O";
            (sender as TextBlock).IsEnabled = false;
            string winner = CheckFields();
            if (winner != null)
            {
                MessageBox.Show(winner + " hat Gewonnen!");
                for (int i = 1; i <= 9; i++)
                {
                    Resources["field" + i] = null;
                    foreach (TextBlock tb in new TextBlock[] { tb1, tb2, tb3, tb4, tb5, tb6, tb7, tb8, tb9 })
                    {
                        tb.IsEnabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gibt Char des Gewinners zurück
        /// </summary>
        /// <returns></returns>
        private string CheckFields()
        {
            for (int x = 1; x <= 7; x += 3)
            {
                if (Resources["field" + x] != null && Resources["field" + x] == Resources["field" + (x + 1)] && Resources["field" + x] == Resources["field" + (x + 2)])
                {
                    return (string)Resources["field" + x];
                }
            }
            for (int y = 1; y <= 3; y++)
            {
                if (Resources["field" + y] != null && Resources["field" + y] == Resources["field" + (y + 3)] && Resources["field" + y] == Resources["field" + (y + 6)])
                {
                    return (string)Resources["field" + y];
                }
            }
            if (Resources["field1"] != null && Resources["field1"] == Resources["field5"] && Resources["field1"] == Resources["field9"])
            {
                return (string)Resources["field1"];
            }
            if (Resources["field3"] != null && Resources["field3"] == Resources["field5"] && Resources["field3"] == Resources["field7"])
            {
                return (string)Resources["field3"];
            }
            return null;
        }
    }
}
