﻿using P20191107_Lib;
using System;

namespace P20191205
{
    class Program
    {
        static void Main(string[] args)
        {
            DynList<Person> dl = new DynList<Person>(new Person("Max", "Muster"));
            dl.AddAtEnd(new Person("Cooler", "Name"));
            dl.AddAtEnd(new Person("Anderer", "Name"));
            dl.AddAtEnd(new Person("Noch Andererer", "Name"));
            Console.WriteLine(dl.Print());
            Console.WriteLine(dl.RevPrint());
            foreach (Person p in dl)
            {
                Console.WriteLine(p);
            }
        }
    }
}
