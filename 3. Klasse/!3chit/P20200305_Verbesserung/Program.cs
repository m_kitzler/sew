﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20200305_Verbesserung
{
    public static class Extensions
    {
        public static string ToB(this string s)     //Meine Lösung :)
        {
            return s.Replace("a", "aba").Replace("e", "ebe").Replace("i", "ibi").Replace("o", "obo").Replace("u", "ubu");
            //string[] chars = new string[]{ "a", "e", "i", "o", "u" };
            //foreach(string c in chars)
            //{
            //    s = s.Replace(c, c + "b" + c);
            //}
            //return s;
        }

        public static string ToB2(this string s)    //Musterlösung
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in s)
            {
                switch (item)
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        sb.Append(item).Append('b').Append(item);
                        break;
                    default:
                        sb.Append(item);
                        break;
                }
            }
            return sb.ToString();
        }

        public static string ToB3(this string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in s)
            {
                if (item == 'a' || item == 'e' || item == 'i' || item == 'o' || item == 'u')
                {
                    sb.Append(item).Append('b').Append(item);
                }
                else
                {
                    sb.Append(item);
                }
            }
            return sb.ToString();
        }
    }

    public class Person
    {
        public string Name;
        public int AM, D, E;
        public override string ToString()
        {
            return Name + ":" + AM + ":" + D + ":" + E;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Person> inter = new List<Person>();
            inter.Add(new Person { Name = "Martin", AM = 3, D = 4, E = 2 });
            inter.Add(new Person { Name = "Susi", AM = 5, D = 4, E = 5 });
            inter.Add(new Person { Name = "Peter", AM = 3, D = 1, E = 2 });
            inter.Add(new Person { Name = "Gaaaaaaaaaaaaaabriel", AM = 3, D = 2, E = 2 });

            inter.Sort((a, b) => { return ((a.AM + a.D + a.E) / 3).CompareTo((b.AM + b.D + b.E) / 3); });
            for (int i = 0; i < inter.Count; i++)
            {
                Console.WriteLine(inter[i].Name);
            }
            Console.WriteLine("\nBsp 2\n");
            StringBuilder tob = new StringBuilder();
            string hlp = "TamaraTamaraTamaraTamaraTamaraTamaraTamaraTamaraTamaraTamara";
            for (int i = 0; i < 10000; i++)
            {
                tob.Append(hlp);
            }

            DateTime start = DateTime.Now;
            for (int i = 0; i < 10; i++)
            {
                start = DateTime.Now;
                tob.ToString().ToB();
                Console.WriteLine("ToB:\t" + (DateTime.Now - start).ToString());

                start = DateTime.Now;
                tob.ToString().ToB2();
                Console.WriteLine("ToB2:\t" + (DateTime.Now - start).ToString());

                start = DateTime.Now;
                tob.ToString().ToB3();
                Console.WriteLine("ToB3:\t" + (DateTime.Now - start).ToString() + "\n");
            }
        }
    }
}
