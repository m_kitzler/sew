﻿using System;
using System.Runtime.Serialization;

namespace P20191010
{
    [Serializable]
    class Dog : Animal
    {
        public Dog(SerializationInfo info, StreamingContext con) : base(info, con)
        {
            animalType = "Dog";
        }

        public Dog(string name, DateTime dateOfBirth, DateTime dateOfAdmission, DateTime dateOfVacc, string special = "") : base(name, dateOfBirth, dateOfAdmission, dateOfVacc, special)
        {
            animalType = "Dog";
        }
    }
}
