﻿using System;
using System.Runtime.Serialization;

namespace P20191010
{
    [Serializable]
    class Bird : Animal
    {
        public Bird(SerializationInfo info, StreamingContext con) : base(info, con)
        {
            animalType = "Bird";
        }

        public Bird(string name, DateTime dateOfBirth, DateTime dateOfAdmission, DateTime dateOfVacc, string special = "") : base(name, dateOfBirth, dateOfAdmission, dateOfVacc, special)
        {
            animalType = "Bird";
        }
    }
}
