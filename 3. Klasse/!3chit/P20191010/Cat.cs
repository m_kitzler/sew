﻿using System;
using System.Runtime.Serialization;

namespace P20191010
{
    [Serializable]
    class Cat : Animal
    {
        public Cat(SerializationInfo info, StreamingContext con) : base(info, con)
        {
            animalType = "Cat";
        }

        public Cat(string name, DateTime dateOfBirth, DateTime dateOfAdmission, DateTime dateOfVacc, string special = "") : base(name, dateOfBirth, dateOfAdmission, dateOfVacc, special)
        {
            animalType = "Cat";
        }
    }
}
