﻿using System;
using System.Runtime.Serialization;

namespace P20191010
{
    [Serializable]
    abstract class Animal : ISerializable
    {
        protected string animalType, name, special;
        protected DateTime dateOfBirth, dateOfAdmission, dateOfVacc;
        public Animal(SerializationInfo info, StreamingContext con)
        {
            animalType = info.GetString("AnimalType");
            name = info.GetString("Name");
            dateOfBirth = info.GetDateTime("DateOfBirth");
            dateOfAdmission = info.GetDateTime("DateOfAdmission");
            dateOfVacc = info.GetDateTime("DateOfVacc");
            special = info.GetString("Special");
        }

        public Animal(string name, DateTime dateOfBirth, DateTime dateOfAdmission, DateTime dateOfVacc, string special = "")
        {
            this.name = name;
            this.dateOfBirth = dateOfBirth;
            this.dateOfAdmission = dateOfAdmission;
            this.dateOfVacc = dateOfVacc;
            this.special = special;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext con)
        {
            info.AddValue("AnimalType", animalType, typeof(string));
            info.AddValue("Name", name, typeof(string));
            info.AddValue("DateOfBirth", dateOfBirth, typeof(DateTime));
            info.AddValue("DateOfAdmission", dateOfAdmission, typeof(DateTime));
            info.AddValue("DateOfVacc", dateOfVacc, typeof(DateTime));
            info.AddValue("Special", special, typeof(string));
        }

        public override string ToString()
        {
            return animalType + ";" + name + ";" + dateOfBirth.ToString("dd/MM/yyyy") + ";" + dateOfAdmission.ToString("dd/MM/yyyy") + ";" + dateOfVacc.ToString("dd/MM/yyyy") + ";" + special;
        }
    }
}
