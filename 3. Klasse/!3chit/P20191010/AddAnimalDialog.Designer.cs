﻿namespace P20191010
{
    partial class AddAnimalDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.dateTimePickerDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateOfAdmission = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateOfVacc = new System.Windows.Forms.DateTimePicker();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSpecial = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelData = new System.Windows.Forms.Panel();
            this.buttonAddSp = new System.Windows.Forms.Button();
            this.panelData.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(308, 250);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(12, 250);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // dateTimePickerDateOfBirth
            // 
            this.dateTimePickerDateOfBirth.Checked = false;
            this.dateTimePickerDateOfBirth.CustomFormat = " ";
            this.dateTimePickerDateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDateOfBirth.Location = new System.Drawing.Point(142, 56);
            this.dateTimePickerDateOfBirth.Name = "dateTimePickerDateOfBirth";
            this.dateTimePickerDateOfBirth.ShowCheckBox = true;
            this.dateTimePickerDateOfBirth.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDateOfBirth.TabIndex = 2;
            this.dateTimePickerDateOfBirth.ValueChanged += new System.EventHandler(this.DateTimePickerDateOf_ValueChanged);
            // 
            // dateTimePickerDateOfAdmission
            // 
            this.dateTimePickerDateOfAdmission.CustomFormat = "Long";
            this.dateTimePickerDateOfAdmission.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDateOfAdmission.Location = new System.Drawing.Point(142, 82);
            this.dateTimePickerDateOfAdmission.Name = "dateTimePickerDateOfAdmission";
            this.dateTimePickerDateOfAdmission.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDateOfAdmission.TabIndex = 3;
            // 
            // dateTimePickerDateOfVacc
            // 
            this.dateTimePickerDateOfVacc.Checked = false;
            this.dateTimePickerDateOfVacc.CustomFormat = " ";
            this.dateTimePickerDateOfVacc.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDateOfVacc.Location = new System.Drawing.Point(142, 108);
            this.dateTimePickerDateOfVacc.Name = "dateTimePickerDateOfVacc";
            this.dateTimePickerDateOfVacc.ShowCheckBox = true;
            this.dateTimePickerDateOfVacc.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDateOfVacc.TabIndex = 4;
            this.dateTimePickerDateOfVacc.ValueChanged += new System.EventHandler(this.DateTimePickerDateOf_ValueChanged);
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Cat",
            "Dog",
            "Bird"});
            this.comboBoxType.Location = new System.Drawing.Point(142, 3);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(200, 21);
            this.comboBoxType.TabIndex = 5;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(142, 30);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(200, 20);
            this.textBoxName.TabIndex = 6;
            // 
            // textBoxSpecial
            // 
            this.textBoxSpecial.Location = new System.Drawing.Point(142, 134);
            this.textBoxSpecial.Name = "textBoxSpecial";
            this.textBoxSpecial.Size = new System.Drawing.Size(200, 20);
            this.textBoxSpecial.TabIndex = 7;
            this.textBoxSpecial.Tag = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Typ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Geburtsdatum:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Aufnahmedatum:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Impfdatum:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Besonderheiten:";
            // 
            // panelData
            // 
            this.panelData.AutoScroll = true;
            this.panelData.Controls.Add(this.buttonAddSp);
            this.panelData.Controls.Add(this.comboBoxType);
            this.panelData.Controls.Add(this.label6);
            this.panelData.Controls.Add(this.dateTimePickerDateOfBirth);
            this.panelData.Controls.Add(this.label5);
            this.panelData.Controls.Add(this.dateTimePickerDateOfAdmission);
            this.panelData.Controls.Add(this.label4);
            this.panelData.Controls.Add(this.dateTimePickerDateOfVacc);
            this.panelData.Controls.Add(this.label3);
            this.panelData.Controls.Add(this.textBoxName);
            this.panelData.Controls.Add(this.label2);
            this.panelData.Controls.Add(this.textBoxSpecial);
            this.panelData.Controls.Add(this.label1);
            this.panelData.Location = new System.Drawing.Point(12, 12);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(370, 232);
            this.panelData.TabIndex = 15;
            // 
            // buttonAddSp
            // 
            this.buttonAddSp.Location = new System.Drawing.Point(113, 132);
            this.buttonAddSp.Name = "buttonAddSp";
            this.buttonAddSp.Size = new System.Drawing.Size(23, 23);
            this.buttonAddSp.TabIndex = 15;
            this.buttonAddSp.Text = "+";
            this.buttonAddSp.UseVisualStyleBackColor = true;
            this.buttonAddSp.Click += new System.EventHandler(this.ButtonAddSp_Click);
            // 
            // AddAnimalDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 283);
            this.Controls.Add(this.panelData);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddAnimalDialog";
            this.Text = "Tier hinzufügen";
            this.panelData.ResumeLayout(false);
            this.panelData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateOfBirth;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateOfAdmission;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateOfVacc;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSpecial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Button buttonAddSp;
    }
}