﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P27022020_Lift
{
    public class Person
    {
        public int Weight { get; private set; }
        public string Name { get; private set; }
        
        public Person(string Name, int Weight)
        {
            this.Name = Name;
            this.Weight = Weight;
        }
    }
}
