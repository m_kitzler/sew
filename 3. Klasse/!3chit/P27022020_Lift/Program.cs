﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P27022020_Lift
{
    class Program
    {
        static void Main(string[] args)
        {
            Elevator e = new Elevator("EEEEEE", 200);
            e.CriticalWeight += ALARM;
            Person p1 = new Person("Max", 20);
            Person p2 = new Person("Max1", 80);
            Person p3 = new Person("Max2", 60);
            Person p4 = new Person("Max3", 70);
            e.Zusteigen(p3);
            e.Zusteigen(p4);
            e.Zusteigen(p1);
            e.Zusteigen(p2);
        }

        /// <summary>
        /// Gibt einen Alarm wieder, wenn wer das Gewicht überschreitet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ALARM(object sender, CritWeightEventArgs e)
        {
            Console.Beep();
            Console.WriteLine("{0} hat das Gewicht überschritten! Gewicht:{1}", e.culprit.Name, (sender as Elevator).CurrWeight);
        }
    }
}
