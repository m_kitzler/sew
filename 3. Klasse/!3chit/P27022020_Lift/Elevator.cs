﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P27022020_Lift
{
    public class CritWeightEventArgs : EventArgs
    {
        public Person culprit { get; set; } //Wer als letztes eingestiegen ist
        public CritWeightEventArgs(Person p)
        {
            culprit = p;
        }
    }
    class Elevator
    {
        private string designation;
        private int maxWeight;
        public int CurrWeight { get; private set; } = 0;
        private List<Person> people = new List<Person>();
        public delegate void CritWeightEventHandler(object sender, CritWeightEventArgs e);
        public event CritWeightEventHandler CriticalWeight;

        public Elevator(string designation, int maxWeight)
        {
            this.designation = designation;
            this.maxWeight = maxWeight;
        }

        /// <summary>
        /// Lässt eine Person zusteigen. Wenn Grenz für das Gewicht überschritten wird, wird wer optimalste rausgeschmissen.
        /// </summary>
        /// <param name="p"></param>
        public void Zusteigen(Person p)
        {
            people.Add(p);                                                                  //Die Person wird hinzugefügt
            CurrWeight += p.Weight;                                                         //Gewicht wird aktualisiert
            if (CurrWeight > maxWeight)                                                     //Überprüfen auf Gewichtsüberschreitung
            {                                                                               //Klamma auf
                CriticalWeight?.Invoke(this, new CritWeightEventArgs(p));                   //Löst das CriticalWeigth-Event (falls vorhanden) aus
                int indexSmallestWeight = -1;                                               //Initialisiert die Variable für den Index der idealen Person
                for (int i = 0; i < people.Count; i++)                                      //for-Schleife für alle Personen in people
                {                                                                           //Klamma auf
                    if (people[i].Weight > CurrWeight - maxWeight && (indexSmallestWeight >= 0 ? people[i].Weight < people[indexSmallestWeight].Weight : true)) //Prüft, ob Person besser geeignet zu aussteigen ist
                    {                                                                       //Klamma auf
                        indexSmallestWeight = i;                                            //Setzt die ideale Person auf die jetztige
                    }                                                                       //Klamma zu
                }                                                                           //Klamma zu
                Console.WriteLine(people[indexSmallestWeight].Name + " muss aussteigen");   //Gibt aus, wer aussteigen muss
                people.Remove(people[indexSmallestWeight]);                                 //lässt Person aussteigen
            }                                                                               //Klamma zu
        }
    }
}
