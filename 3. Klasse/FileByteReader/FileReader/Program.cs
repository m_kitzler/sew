﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace FileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Path: ");
            string readPath = @"D:\roms\nds\MarkeEigenbau\Mario Kart.nds"; /*FileFinder.ReadLine();*/
            Console.WriteLine();
            Console.Write("Dest: ");
            string destPath = @"C:\Users\Miau\Desktop\mk.nds";
            Console.WriteLine();
            Console.Write("Byte-Array size: ");
            int byteArraySize = Convert.ToInt32(Console.ReadLine());
            Console.Write("Pause between Array-copying[ms]: ");
            int sleepTime = Convert.ToInt32(Console.ReadLine());
            Console.Write("Debug[Y/N]: ");
            bool debug = new Regex(@"y|Y|yes|true").IsMatch(Console.ReadLine());
            Console.Clear();
            Console.CursorVisible = false;
            if (File.Exists(destPath))
            {
                Console.WriteLine("File already exists!");
                Console.ReadKey();
            }
            else
            {
                FileStream dest = new FileStream(destPath, FileMode.CreateNew);
                using (FileStream read = new FileStream(readPath, FileMode.Open))
                {
                    long length = read.Length;
                    long rest = length % byteArraySize;
                    long progPart = (long)Math.Floor((double)((length - rest) / 10));
                    Console.WriteLine("Length: " + length);
                    for (int pos = 0; pos < length - rest; pos += byteArraySize)
                    {
                        byte[] currBytes = new byte[byteArraySize];
                        read.Read(currBytes, 0, byteArraySize);
                        dest.Write(currBytes, 0, byteArraySize);
                        Console.SetCursorPosition(0, 1);
                        Console.WriteLine(GetProgression(pos, 10, progPart));
                        if (sleepTime > 0)
                        {
                            Thread.Sleep(sleepTime);
                        }
                        if (debug)
                        {
                            Console.SetCursorPosition(0, 2);
                            Console.WriteLine("Reading: " + read.Position);
                            foreach (byte b in currBytes)
                            {
                                string hlp = b.ToString("X");
                                Console.Write(" " + ((hlp.Length < 2) ? hlp + "0" : hlp));
                            }
                        }
                    }
                    Console.WriteLine();
                    byte[] lastBytes = new byte[rest];
                    read.Read(lastBytes, 0, (int)rest);
                    dest.Write(lastBytes, 0, (int)rest);
                    Console.SetCursorPosition(0, 1);
                    Console.WriteLine(GetProgression(10, 10, 1));
                    Console.ForegroundColor = ConsoleColor.Green;
                    if (debug)
                    {
                        foreach (byte b in lastBytes)
                        {
                            string hlp = b.ToString("X");
                            Console.Write(" " + ((hlp.Length < 2) ? hlp + "0" : hlp));
                        }
                    }
                }
                dest.Close();
                Console.WriteLine("\nDone!");
                Console.ReadKey(true);
            }
        }

        private static string GetProgression(long progrss, int parts , long progPart)
        {
            long partProgress = progrss / progPart;
            string rtnstr = "[";
            for (int i = 0; i < parts; i++)
            {
                if (partProgress - i > 0)
                    rtnstr += "█";
                else
                    rtnstr += " ";
            }
            return rtnstr + "]";
        }
    }
    
    public static class FileFinder
    {
        public static string ReadLine()
        {
            string input = "";
            int count = 0;
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.Tab:
                        DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(input));
                        break;
                    case ConsoleKey.Backspace:
                        count = 0;
                        if (input.Length > 0)
                        {
                            input = input.Remove(input.Length - 1);
                            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                            ConsoleColor Fore = Console.ForegroundColor;
                            Console.ForegroundColor = Console.BackgroundColor;
                            Console.Write(".");
                            Console.ForegroundColor = Fore;
                            Console.SetCursorPosition(Console.CursorLeft - 2, Console.CursorTop);
                        }
                        break;
                    default:
                        count = 0;
                        input += key.KeyChar;
                        break;
                }
            } while (key.Key != ConsoleKey.Enter);
            return input;
        }
    }
}
