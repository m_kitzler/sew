﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DecStream
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            s.Bind(new IPEndPoint(IPAddress.Any, 1337));
            s.Listen(1);
            Console.WriteLine("Listening...");
            s.BeginAccept(Handle, s);
            while (true)
            {
                
            }
        }
        static void Handle(IAsyncResult a)
        {
            Socket client = ((Socket)a.AsyncState).EndAccept(a);
            byte[] buffer = new byte[1];
            if (client.Connected)
            {
                Console.WriteLine("Connected");
                Aes aes = Aes.Create();
                //TODO: key mit rsa aushandeln
                //aes.GenerateKey
                aes.Key = new byte[32]; aes.IV = new byte[16];
                using (CryptoStream cs = new CryptoStream(new NetworkStream(client), aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        while (client.Connected)
                        {
                            Console.Write(sr.ReadLine());
                            //cs.BeginRead(buffer, 0, 1, (IAsyncResult result) => Console.Write(Encoding.UTF8.GetString((byte[])result.AsyncState)), buffer);
                            //client.Receive(buffer);
                            //Console.Write(Encoding.ASCII.GetString(buffer));
                        }
                    }
                }
            }
            Console.WriteLine("Disconnected");
        }
    }
}
