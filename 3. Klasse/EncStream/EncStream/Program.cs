﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace EncStream
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey(true);
            Console.WriteLine("Connecting...");
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            s.Connect("127.0.0.1", 1337);
            
            if (s.Connected)
            {
                Console.WriteLine("Connected");
                Aes aes = Aes.Create();
                //Console.WriteLine("Key: " + aes.Key + " IV: " + aes.IV);
                aes.Key = new byte[32]; aes.IV = new byte[16];
                using (CryptoStream cs = new CryptoStream(new NetworkStream(s), aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        while (s.Connected)
                        {
                            sw.Write(/*Console.ReadKey(true).KeyChar*/"Hello World!");
                            //cs.Write(Encoding.UTF8.GetBytes(Console.ReadKey(true).KeyChar + ""));
                            //s.Send(Encoding.ASCII.GetBytes(Console.ReadKey(true).KeyChar + ""));
                        }
                    }
                }
            }
            Console.WriteLine("Disconnected");
        }
    }
}
