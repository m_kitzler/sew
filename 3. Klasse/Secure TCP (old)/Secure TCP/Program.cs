﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Secure_TCP
{
    class Program
    {
        const int bufferSize = 1024;
        static NegotiateStream ns = null;
        static byte[] data = new byte[bufferSize];
        static StringBuilder message = new StringBuilder();
        static void Main(string[] args)
        {

            Console.WriteLine("1 for Server, 2 for Client");
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("Als Server starten...");
                    TcpListener tl = new TcpListener(IPAddress.Loopback, 8888);
                    tl.Start();
                    Console.WriteLine("Server gestartet");
                    tl.BeginAcceptTcpClient(Acc, tl);
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine("Verbinde...");
                    TcpClient tc = new TcpClient();
                    try
                    {
                        tc.Connect(IPAddress.Loopback, 8888);
                    }
                    catch (SocketException)
                    {
                        Console.WriteLine("Verbindung konnte nicht hergestellt werden");
                    }
                    if (tc.Connected)
                    {
                        Console.WriteLine("Verbunden");
                        ns = new NegotiateStream(tc.GetStream());
                        ns.AuthenticateAsClient();
                        ns.BeginRead(data, 0, bufferSize, Rec, ns);

                        //Testsend
                        byte[] sendInfo = new byte[1];
                        sendInfo[0] = 1;                                                                            //ZU 2 ändern für TEST
                        byte[] send = Encoding.UTF8.GetBytes("Test von Client");
                        byte[] sendEnd = new byte[1];
                        sendEnd[0] = 255;  //sichergehen, dass letztes Byte nicht null
                        byte[] msg = new byte[send.Length + sendInfo.Length + sendEnd.Length];
                        sendInfo.CopyTo(msg, 0); send.CopyTo(msg, sendInfo.Length); sendEnd.CopyTo(msg, sendInfo.Length + send.Length);
                        ns.Write(msg, 0, msg.Length);
                    }
                    break;
            }
            while (true)
            {
                if (ns != null)
                {
                    byte[] send = Encoding.UTF8.GetBytes(Console.ReadLine());
                    if (ns != null)
                        ns.Write(send, 0, send.Length);
                }
            }
        }

        static void Acc(IAsyncResult async) 
        {
            TcpListener server = (TcpListener)async.AsyncState;
            TcpClient client = server.EndAcceptTcpClient(async);
            ns = new NegotiateStream(client.GetStream());
            ns.AuthenticateAsServer();
            ns.BeginRead(data, 0, bufferSize, Rec, ns);
        }

        static void Rec(IAsyncResult async)
        {
            try
            {
                NegotiateStream ns = (NegotiateStream)async.AsyncState;
                ns.EndRead(async);
                byte[] msgInfo = new byte[1];
                Array.Copy(data, msgInfo, 1);
                byte[] msgData = new byte[RealLength(data) - 1];    //-1 wegen Endbyte
                Array.Copy(data, 1, msgData, 0, RealLength(data) - 1);  //-2 wegen Anfang

                Console.WriteLine(msgInfo[0]);
                if (msgInfo[0] == 2)
                {
                    message.Append("TEST ");
                    message.Append(Encoding.UTF8.GetString(msgData));
                    message.Append(" TEST");
                    Console.WriteLine(message.ToString());
                    message.Clear();
                    data = new byte[bufferSize];
                    ns.BeginRead(data, 0, bufferSize, Rec, ns);
                }
                else
                {
                    message.Append(Encoding.UTF8.GetString(msgData));
                    Console.WriteLine(message.ToString());
                    message.Clear();
                    data = new byte[bufferSize];
                    ns.BeginRead(data, 0, bufferSize, Rec, ns);
                }
            }
            catch (IOException)
            {
                Console.WriteLine("Die Verbindung wurde geschlossen");
                ns = null;
            }
        }

        /// <summary>
        /// Gibt die Position des letzten Byte eines Arrays zurück, das nicht 0 ist.
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        static int RealLength(byte[] b)
        {
            for (int i = b.Length - 1; i >= 0; i--)
            {
                if (b[i] != 0)
                    return i + 1;
            }
            return 0;
        }
    }
}
