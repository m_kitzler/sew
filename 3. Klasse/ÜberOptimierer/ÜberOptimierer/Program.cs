﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ÜberOptimierer
{
    class Program
    {
        static void Main(string[] args)
        {
            int foundTimes = 0;
            bool exit = false;
            while (!exit)
            {
                int proc = GetPID();
                if (proc > 0)
                {
                    Process.GetProcessById(proc).Kill();
                    Console.WriteLine("\tKILLED");
                    foundTimes = 0;
                }
                else
                {
                    if (foundTimes > 0)
                        Console.CursorTop--;
                    Console.WriteLine("Not Found " + Status(foundTimes));
                    foundTimes++;
                    if (foundTimes > 4)
                        foundTimes = 1;
                }
                Task.Delay(5000).Wait();
            }
        }

        static string Status(int curr)
        {
            switch (curr)
            {
                case 1:
                    return "-";
                case 2:
                    return "\\";
                case 3:
                    return "|";
                case 4:
                    return "/";
            }
            return "";
        }

        static int GetPID()
        {
            Process p = new Process();
            p.StartInfo.Arguments = "/c tasklist /svc /fi \"IMAGENAME eq svchost.exe\"";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "cmd.exe";
            p.Start();
            do
            {
                string output = p.StandardOutput.ReadLine();
                Match m = new Regex(@"\d+ DoSvc").Match(output);
                if (m.Success)
                {
                    Console.Write("Found: " + m.Value);
                    return Convert.ToInt32(m.Value.Split(' ')[0]);
                }
            } while (!p.StandardOutput.EndOfStream);
            p.WaitForExit();
            return -1;
        }
    }
}
