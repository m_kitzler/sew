﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            double km; double l; double sumkm = 0; double suml = 0;
            bool weiter = true;
            string input;
            while (weiter)
            {
                l = 0;
                km = 0;
                Console.Write("Kilometer:");
                input = Console.ReadLine();                         //Kilometereinagbe
                if (input == "")                                    //
                    break;
                km = Convert.ToDouble(input);
                if (km > 0)
                {
                    Console.Write("Liter:");
                    l = Convert.ToDouble(Console.ReadLine());       //Litereingabe
                }
                if (l == 0)                                         //0 Liter eingeben zu fortfahren
                    weiter = false;
                sumkm += km;
                suml += l;
            }
            Console.Clear();
            double ergebnis = (suml / sumkm) * 100;
            Console.WriteLine("Verbrauch: " + ergebnis + "l/100Km");   //Ergebnis in Liter pro 100 Kilometer
        }
    }
}
