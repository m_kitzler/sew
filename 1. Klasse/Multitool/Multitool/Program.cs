﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multitool
{
    class Program
    {
        static void Main(string[] args)
        {
        //Console.ReadKey();
        a:
            int a;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Clear();
            Console.WriteLine("Funktion auswählen\n1: Notendurchschnitt\n2: Kennzeichenkürzel");
            a = Convert.ToInt32(Console.ReadLine());
            switch (a)
            {
                case 1:
                    int note;
                    int d;
                    int e;
                    int m;
                    Console.WriteLine("Was sind deine Noten?\nZurück mit 9");
                    Console.WriteLine("Deutsch:");
                    d = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine("Was sind deine Noten?\nZurück mit 9");
                    Console.WriteLine("Mathe:");
                    m = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine("Was sind deine Noten?\nZurück mit 9");
                    Console.WriteLine("Englisch");
                    e = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    note = (d + m + e) / 3;
                    switch (note)
                    {
                        case 1:
                            Console.WriteLine("Sehr Gut");
                            break;
                        case 2:
                            Console.WriteLine("Gut");
                            break;
                        case 3:
                            Console.WriteLine("Befriedigent");
                            break;
                        case 4:
                            Console.WriteLine("Genügend");
                            break;
                        case 5:
                            Console.WriteLine("Nicht Genügend");
                            break;
                        case 9:
                            goto a;
                        default:
                            Console.WriteLine("Ungültige Eingabe");
                            break;
                    }
                    Console.ReadLine();
                    goto a;
                case 2:
                    Console.WriteLine("Kennbuchstaben:");
                    string kennbuchstab = Console.ReadLine();
                    Console.Clear();
                    switch (kennbuchstab)
                    {
                        case "B":
                            Console.WriteLine("Burgenland");
                            break;
                        case "K":
                            Console.WriteLine("Kärnten");
                            break;
                        case "N":
                            Console.WriteLine("Niederösterreich");
                            break;
                        case "O":
                            Console.WriteLine("Oberösterreich");
                            break;
                        case "S":
                            Console.WriteLine("Salzburg");
                            break;
                        case "ST":
                            Console.WriteLine("Steiermark");
                            break;
                        case "T":
                            Console.WriteLine("Tirol");
                            break;
                        case "V":
                            Console.WriteLine("Vorarlberg");
                            break;
                        case "W":
                            Console.WriteLine("Wien");
                            break;
                        case "back":
                            goto a;
                        default:
                            Console.WriteLine("keine gültige Stadt");
                            Console.ReadLine();
                            goto a;
                    }
                    break;
            }
        }
    }
}

