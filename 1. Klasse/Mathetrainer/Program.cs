﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathelerner
{
    class Program
    {
        static void Main(string[] args)
        {
            Random Josef = new Random();
            double a = 0; double b = 0; int r = 0; int f = 0; int anzahl = 0; double userinput; double lösung = 0;
            Console.WriteLine("Welche Rechnungenarten?\n1: Multiplikation\n2: Division [WIP]\n3: Addition (1 - 100)\n4: Subtraktion (1 - 100)\n5: Alle");
            ConsoleKeyInfo Input = Console.ReadKey();
            string input = Input.Key.ToString();
            Console.Clear();
            Console.WriteLine("Wie viel Rechnungen willst du haben?");
            int rechnungen = Convert.ToInt32(Console.ReadLine());
            while (anzahl < rechnungen)
            {
                Console.Clear();
                switch (input)
                {
                    case "D1":
                        a = Josef.Next(1, 10); b = Josef.Next(1, 10); lösung = a * b;
                        Console.WriteLine("{0} x {1}", a, b);
                        break;
                    case "":
                        a = Josef.Next(2, 10); b = Josef.Next(1, 10); lösung = a / b;
                        Console.WriteLine("{0} / {1}", a, b);
                        break;
                    case "D3":
                        a = Josef.Next(1, 10); b = Josef.Next(1, 100); lösung = a + b;
                        Console.WriteLine("{0} + {1}", a, b);
                        break;
                    case "D4":
                        a = Josef.Next(1, 10); b = Josef.Next(1, 100); lösung = a - b;
                        Console.WriteLine("{0} - {1}", a, b);
                        break;
                    case "D5":
                        int zahl = Josef.Next(1, 4);
                        switch (zahl)
                        {
                            case 1:
                                a = Josef.Next(1, 10); b = Josef.Next(1, 10); lösung = a * b;
                                Console.WriteLine("{0} x {1}", a, b);
                                break;
                            case 2:
                                a = Josef.Next(2, 10); b = Josef.Next(1, 10); lösung = a - b;
                                Console.WriteLine("{0} - {1}", a, b);
                                break;
                            case 3:
                                a = Josef.Next(1, 10); b = Josef.Next(1, 100); lösung = a + b;
                                Console.WriteLine("{0} + {1}", a, b);
                                break;
                            case 0:
                                a = Josef.Next(1, 10); b = Josef.Next(1, 100); lösung = a / b;
                                Console.WriteLine("{0} / {1}", a, b);
                                break;
                        }
                        break;
                }
                userinput = Convert.ToInt32(Console.ReadLine());
                if (userinput == lösung) r++;
                else f++;
                anzahl++;
            }
            Console.Clear();
            Console.WriteLine("Du hast {0} von {1} Ergebnissen richtig.\nFehler: {2}", r, rechnungen, f);
        }
    }
}
