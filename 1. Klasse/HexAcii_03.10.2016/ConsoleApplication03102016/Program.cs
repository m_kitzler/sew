﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication03102016
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 65;             // Dezimalzahl 65 speichern
            char b = (char)a;       // den Buchstaben ASCII-Position 65 zuweisen
            Console.WriteLine(b);   // gibt A aus

            a = 0x42;               // Hexadezimalzahl 42 speichern
            b = (char)a;            // den Buchstaben ASCII-Position 66 zuweisen
            Console.WriteLine(b);   // gibt B aus
        }
    }
}
