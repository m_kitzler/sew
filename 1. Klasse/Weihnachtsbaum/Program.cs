﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _2.Hü
{
    class Program
    {
        /// <summary>
        /// Macht ein grünes Feld
        /// </summary>
        /// <param name="x"></param>
        static void Grün()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("_");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("_");
        }

        /// <summary>
        /// Macht ein blaues Feld
        /// </summary>
        /// <param name="x">
        /// Zufälliger Wert
        /// </param>
        static void Blau(int x)
        {
            if (x == 0)
            {
                Console.BackgroundColor = ConsoleColor.Blue;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("_*_");
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Blue;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("_");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("*");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("_");
            }
        }
        static void Main(string[] args)
        {
            Random r = new Random();
            int treeWidth = 11;
            int treeHeight = 2;
            int anzahlBlau = (int)Math.Ceiling((double)(treeWidth / 2));
            int anzahlGrün = (treeWidth % 2 == 0) ? 2 : 1;
            while (true)
            {
                int aB = anzahlBlau;    //Setzen/Zurücksetzen der Variablen
                int aG = anzahlGrün;
                #region Blätter
                for (int schleife3 = 0; aB >= 1; schleife3++)   //Läuft, bis Anteil Blau nur mehr 1
                {
                    for (int schleife2 = 0; schleife2 < treeHeight; schleife2++)    //Baumhöhe
                    {
                        for (int schleife = 0; schleife < aB; schleife++)   //Link Seite
                        {
                            Blau(r.Next(0, 2));
                        }
                        for (int schleife = 0; schleife < aG; schleife++)   //Blätter
                        {
                            Grün();
                        }
                        for (int schleife = 0; schleife < aB; schleife++)   //Rechte Seite
                        {
                            Blau(r.Next(0, 2));
                        }
                        Console.WriteLine();
                    }
                    aB--;
                    aG += 2;
                }
                #endregion
                #region Stamm
                for (int i = 0; i < treeHeight; i++)
                {
                    for (int schleife = 0; schleife < anzahlBlau; schleife++)   //Linke Seite
                    {
                        Blau(0);
                    }
                    for (int schleife = 0; schleife < anzahlGrün; schleife++)   //Stamm
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write("___");
                    }
                    for (int schleife = 0; schleife < anzahlBlau; schleife++)   //Rechte Seite
                    {
                        Blau(0);
                    }
                    Console.WriteLine();
                }
                #endregion
                #region Reset und Verzögerung
                Thread.Sleep(250);
                Console.SetCursorPosition(0, 0);
                #endregion
            }
        }
    }
}