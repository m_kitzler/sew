﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Tank
    {
        int füllstand = 0;
        int maxfüllstand = 69;
        public int t
        {
            set
            {
                if ((füllstand + value) > maxfüllstand)
                {
                    Console.WriteLine((maxfüllstand - füllstand) + "l wurden getankt und " + ((füllstand + value) - maxfüllstand) + "l blieben übrig.");
                }
                else
                {
                    füllstand += value;
                    Console.WriteLine(value + "l wurden getankt");
                }
            }
        }
        public int ta
        {
            get
            {
                return füllstand;
            }
        }
    }
}
