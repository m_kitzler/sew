﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using TicTacToeGame;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        OnlineTTT ttt = new OnlineTTT();
        PlayerSettings x = new PlayerSettings(Player.X);
        PlayerSettings o = new PlayerSettings(Player.O);
        Dictionary<Button, (byte, byte)> buttons;

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AllocConsole();

        public Form1()
        {
            InitializeComponent();
            SetLabel(ttt.CurrentPlayer);
            buttons = new Dictionary<Button, (byte, byte)>
            {
                { button1, (0, 0) }, { button2, (0, 1) }, { button3, (0, 2) }, { button4, (1, 0) }, { button5, (1, 1) }, { button6, (1, 2) }, { button7, (2, 0) }, { button8, (2, 1) }, { button9, (2, 2) }
            };
            ttt.OnReset += (o, e) => ResetButtons();
            ttt.OnSyncTurn += (o, e) => SetLabel((e as SyncTurnEventArgs).player);
            ttt.OnMove += MadeMove;
            ttt.OnConnectionEnd += (o, e) => {
                ConnCloseEventArgs cce = (ConnCloseEventArgs)e;
                if (cce.Message != null)
                    MessageBox.Show(cce.Message);
                Text = "TicTacToe";
                verbindungTrennenToolStripMenuItem.Enabled = false;
            };
        }

#region Eingabe
        private void button_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ValidateInput(buttons[(sender as Button)], Player.X);
            }
            else if (e.Button == MouseButtons.Right)
            {
                ValidateInput(buttons[(sender as Button)], Player.O);
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (x.mapping.ContainsKey(e.KeyCode))
            {
                ValidateInput(x.mapping[e.KeyCode], Player.X);
            }
            else if (o.mapping.ContainsKey(e.KeyCode))
            {
                ValidateInput(o.mapping[e.KeyCode], Player.O);
            }
        }

        private void ValidateInput((byte, byte) loc, Player player)
        {
            if (player == Player.X)
            {
                if (ttt.Connected)
                {
                    if (ttt.GetPlayer() == ttt.CurrentPlayer)
                    {
                        ttt.SendMove(loc);
                        SetField(loc);
                    }
                    else
                    {
                        labelTurn.Text = "Du bist noch nicht dran!";
                        Task.Run(async () => { await Task.Delay(1000); SetLabel(ttt.CurrentPlayer); });
                    }
                }
                else if (ttt.CurrentPlayer == Player.X)
                    SetField(loc);
            }
            else
            {
                if (!ttt.Connected && ttt.CurrentPlayer == Player.O)
                    SetField(loc);
            }
        }

        private void MadeMove(object o, EventArgs e)
        {
            SetField((e as MoveEventArgs).Coordinates);
        }
        #endregion

        private void SetLabel(Player player)
        {
            labelTurn.Text = (player == Player.X ? "X" : "O") + " ist am Zug";
        }

        /// <summary>
        /// Versucht auf loc zu setzten
        /// </summary>
        /// <param name="loc">Die Position am Spielfeld</param>
        private void SetField((int, int) loc)
        {
            if (ttt.ActiveGame && ttt.SetField(loc))
            {
                UpdateButtons();
                SetLabel(ttt.NextTurn());
                GameResult res = ttt.CheckForWin();
                switch (res)
                {
                    case GameResult.XWon:
                    case GameResult.OWon:
                        MessageBox.Show($"Spieler \"{(res == GameResult.XWon ? "X" : "O")}\" hat gewonnen!", (ttt.Role == ConnRole.Client ? "Client" : "Server"));
                        break;
                    case GameResult.Draw:
                        MessageBox.Show("Unentschieden!", (ttt.Role == ConnRole.Client ? "Client" : "Server"));
                        break;
                }
                if (res != GameResult.None)
                {
                    if (!ttt.Connected)
                        Reset();
                    else if (ttt.Role == ConnRole.Server)
                    {
                        Reset();
                        ttt.SendReset();
                        ttt.SendSyncTurn();
                    }
                }
            }
            else if (!ttt.ActiveGame)
            {
                labelTurn.Text = "Spiel zu ende";
                Task.Run(async () => { await Task.Delay(1000); SetLabel(ttt.CurrentPlayer); });
            }
            else
            {
                labelTurn.Text = "Feld besetzt!";
                Task.Run(async () => { await Task.Delay(1000); SetLabel(ttt.CurrentPlayer); });
            }
        }

        /// <summary>
        /// Passt den Text der Buttons an das Spielfeld an.
        /// </summary>
        private void UpdateButtons()
        {
            foreach (var entry in buttons)
            {
                Player p = ttt.Field[entry.Value.Item1, entry.Value.Item2];
                entry.Key.Text = (p == Player.None) ? "" : p.ToString();
            }
        }

        /// <summary>
        /// Löscht den Text aus den Form Buttons
        /// </summary>
        private void ResetButtons()
        {
            foreach (var b in buttons)
            {
                b.Key.Text = "";
            }
        }

        /// <summary>
        /// Startet das Spiel neu
        /// </summary>
        private void Reset()
        {
            ResetButtons();
            SetLabel(ttt.Reset());
        }

#region Menüband
        private void tastenbelegungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBindings fb = new FormBindings(x, o);
            fb.ShowDialog();
            x = fb.PlayerX;
            o = fb.PlayerO;
        }

        private void spielBeitretenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConnect fc = new FormConnect();
            fc.PanelConnect();
            fc.ShowDialog();
            if (fc.DialogResult != DialogResult.OK)
            {
                return;
            }
            // init
            ttt.HandleConnection(fc.opponent, ConnRole.Client, fc.UsedProtocol);
            verbindungTrennenToolStripMenuItem.Enabled = true;
            Text = "TicTacToe: Spieler O";
            Console.WriteLine("Client");
        }

        private void spielErstellenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConnect fc = new FormConnect();
            fc.PanelCreate();
            fc.ShowDialog();
            if (fc.DialogResult != DialogResult.OK)
            {
                return;
            }
            // init
            ttt.HandleConnection(fc.opponent, ConnRole.Server);
            verbindungTrennenToolStripMenuItem.Enabled = true;
            Text = "TicTacToe: Spieler X";
            Console.WriteLine("Server");
            Reset();
            ttt.SendReset();
            ttt.SendSyncTurn();
        }

        private void verbindungTrennenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ttt.CloseConnection();
        }

        private void konsoleÖffnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllocConsole();
        }
#endregion
    }
}
