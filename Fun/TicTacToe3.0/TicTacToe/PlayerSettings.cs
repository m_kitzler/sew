﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToeGame;

namespace TicTacToe
{
    public class PlayerSettings
    {
        public Dictionary<Keys, (byte, byte)> mapping { get; set; }

        /// <summary>
        /// Erstellt eine neue PlayerSettings Instanz.
        /// </summary>
        /// <param name="defaultKeybindings">Legt fest, ob die Standartwerte von Spieler X oder O geladen werden sollen.</param>
        public PlayerSettings(Player defaultKeybindings)
        {
            if (defaultKeybindings == Player.X)
            {
                mapping = new Dictionary<Keys, (byte, byte)>
                {
                    { Keys.Q, (0, 0) }, { Keys.W, (0, 1) }, { Keys.E, (0, 2) }, { Keys.A, (1, 0) }, { Keys.S, (1, 1) }, { Keys.D, (1, 2) }, { Keys.Y, (2, 0) }, { Keys.X, (2, 1) }, { Keys.C, (2, 2) }
                };
            }
            else if (defaultKeybindings == Player.O)
            {
                mapping = new Dictionary<Keys, (byte, byte)>
                {
                    { Keys.I, (0, 0) }, { Keys.O, (0, 1) }, { Keys.P, (0, 2) }, { Keys.K, (1, 0) }, { Keys.L, (1, 1) }, { Keys.Oem3, (1, 2) }, { Keys.Oemcomma, (2, 0) }, { Keys.OemPeriod, (2, 1) }, { Keys.OemMinus, (2, 2) }
                };
            }
            else
                mapping = new Dictionary<Keys, (byte, byte)> ();
        }
    }
}
