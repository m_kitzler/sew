﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace TicTacToe
{
    public partial class FormBindings : Form
    {
        Button[] Buttons;
        public PlayerSettings PlayerX { get; private set; }
        public PlayerSettings PlayerO { get; private set; }
        public FormBindings(PlayerSettings PlayerX, PlayerSettings PlayerO)
        {
            InitializeComponent();
            this.PlayerX = PlayerX;
            this.PlayerO = PlayerO;
            Buttons = new Button[] { button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16, button17, button18 };
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    Buttons[x + y * 3].Tag = x + y * 3;
                    Buttons[x + y * 3].Text = PlayerX.mapping.FirstOrDefault(z => z.Value == (y, x)).Key.ToString();
                    Buttons[x + y * 3 + 9].Tag = x + y * 3 + 9;
                    Buttons[x + y * 3 + 9].Text = PlayerO.mapping.FirstOrDefault(z => z.Value == (y, x)).Key.ToString();
                }
            }
        }

        private async void button_Click(object sender, EventArgs e)
        {
            //NOTIZ: async erlaubt das triggern von Events während Task.Delay

            int button = (int)(sender as Button).Tag;
            PlayerSettings player = (button >= 9 ? PlayerO : PlayerX);
            Keys oldKey = player.mapping.FirstOrDefault(x => x.Value == ((button - 9) / 3, (button - 9) % 3)).Key;
            (byte, byte) val = player.mapping[oldKey];

            // Warte, bis Taste gedrückt wurde
            Keys newKey = default(Keys);
            Buttons[button].Text = "Drücke eine Taste...";
            KeyEventHandler handler = (o, eve) => newKey = eve.KeyCode;
            Buttons[button].KeyDown += handler;
            DateTime start = DateTime.Now;
            while (newKey == default(Keys) && (DateTime.Now - start).TotalMilliseconds < 2000) { await Task.Delay(10); }
            Buttons[button].KeyDown -= handler;

            if (newKey != default(Keys))
            {
                player.mapping.Remove(oldKey);
                player.mapping[newKey] = val;
                Buttons[button].Text = newKey.ToString();
            }
            else
                Buttons[button].Text = oldKey.ToString();
        }
    }
}
