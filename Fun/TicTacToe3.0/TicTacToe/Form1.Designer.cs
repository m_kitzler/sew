﻿namespace TicTacToe
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mehrspielerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spielBeitretenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spielErstellenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verbindungTrennenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tastenbelegungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelTurn = new System.Windows.Forms.ToolStripStatusLabel();
            this.konsoleÖffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(10, 27);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(100, 100);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(116, 27);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(100, 100);
            this.button2.TabIndex = 0;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(222, 27);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(100, 100);
            this.button3.TabIndex = 0;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(10, 133);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(100, 100);
            this.button4.TabIndex = 0;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(116, 133);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(100, 100);
            this.button5.TabIndex = 0;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(222, 133);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(100, 100);
            this.button6.TabIndex = 0;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(10, 239);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button7.Size = new System.Drawing.Size(100, 100);
            this.button7.TabIndex = 0;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(116, 239);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button8.Size = new System.Drawing.Size(100, 100);
            this.button8.TabIndex = 0;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(222, 239);
            this.button9.Name = "button9";
            this.button9.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.button9.Size = new System.Drawing.Size(100, 100);
            this.button9.TabIndex = 0;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_MouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionenToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(334, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionenToolStripMenuItem
            // 
            this.optionenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mehrspielerToolStripMenuItem,
            this.tastenbelegungenToolStripMenuItem,
            this.konsoleÖffnenToolStripMenuItem});
            this.optionenToolStripMenuItem.Name = "optionenToolStripMenuItem";
            this.optionenToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.optionenToolStripMenuItem.Text = "Optionen";
            // 
            // mehrspielerToolStripMenuItem
            // 
            this.mehrspielerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spielBeitretenToolStripMenuItem,
            this.spielErstellenToolStripMenuItem,
            this.verbindungTrennenToolStripMenuItem});
            this.mehrspielerToolStripMenuItem.Name = "mehrspielerToolStripMenuItem";
            this.mehrspielerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mehrspielerToolStripMenuItem.Text = "Mehrspieler";
            // 
            // spielBeitretenToolStripMenuItem
            // 
            this.spielBeitretenToolStripMenuItem.Name = "spielBeitretenToolStripMenuItem";
            this.spielBeitretenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.spielBeitretenToolStripMenuItem.Text = "Spiel beitreten";
            this.spielBeitretenToolStripMenuItem.Click += new System.EventHandler(this.spielBeitretenToolStripMenuItem_Click);
            // 
            // spielErstellenToolStripMenuItem
            // 
            this.spielErstellenToolStripMenuItem.Name = "spielErstellenToolStripMenuItem";
            this.spielErstellenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.spielErstellenToolStripMenuItem.Text = "Spiel erstellen";
            this.spielErstellenToolStripMenuItem.Click += new System.EventHandler(this.spielErstellenToolStripMenuItem_Click);
            // 
            // verbindungTrennenToolStripMenuItem
            // 
            this.verbindungTrennenToolStripMenuItem.Enabled = false;
            this.verbindungTrennenToolStripMenuItem.Name = "verbindungTrennenToolStripMenuItem";
            this.verbindungTrennenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.verbindungTrennenToolStripMenuItem.Text = "Verbindung trennen";
            this.verbindungTrennenToolStripMenuItem.Click += new System.EventHandler(this.verbindungTrennenToolStripMenuItem_Click);
            // 
            // tastenbelegungenToolStripMenuItem
            // 
            this.tastenbelegungenToolStripMenuItem.Name = "tastenbelegungenToolStripMenuItem";
            this.tastenbelegungenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tastenbelegungenToolStripMenuItem.Text = "Tastenbelegungen";
            this.tastenbelegungenToolStripMenuItem.Click += new System.EventHandler(this.tastenbelegungenToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AllowMerge = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelTurn});
            this.statusStrip1.Location = new System.Drawing.Point(0, 364);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(334, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelTurn
            // 
            this.labelTurn.Name = "labelTurn";
            this.labelTurn.Size = new System.Drawing.Size(185, 17);
            this.labelTurn.Text = "Hier könnte ihre Werbung stehen!";
            // 
            // konsoleÖffnenToolStripMenuItem
            // 
            this.konsoleÖffnenToolStripMenuItem.Name = "konsoleÖffnenToolStripMenuItem";
            this.konsoleÖffnenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.konsoleÖffnenToolStripMenuItem.Text = "Konsole öffnen";
            this.konsoleÖffnenToolStripMenuItem.Click += new System.EventHandler(this.konsoleÖffnenToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 386);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(350, 425);
            this.Name = "Form1";
            this.Text = "TicTacToe";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel labelTurn;
        private System.Windows.Forms.ToolStripMenuItem optionenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mehrspielerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spielBeitretenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spielErstellenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tastenbelegungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verbindungTrennenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konsoleÖffnenToolStripMenuItem;
    }
}

