﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToeGame;

namespace TicTacToe
{
    public partial class FormConnect : Form
    {
        public TcpClient opponent { get; private set; }
        public TTTProtocol UsedProtocol { get; private set; }
        TcpListener server;

        public FormConnect()
        {
            InitializeComponent();
            comboBoxProtocol.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (TTTProtocol t in Enum.GetValues(typeof(TTTProtocol)))
            {
                comboBoxProtocol.Items.Add(t);
            }
            comboBoxProtocol.SelectedIndex = 0;
            UsedProtocol = (TTTProtocol)comboBoxProtocol.SelectedIndex;

            //placeholder text
            textBoxIP.GotFocus += DeleteText;
            textBoxIP.LostFocus += AddText;
        }

        private void AddText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxIP.Text))
            {
                textBoxIP.Text = "localhost";
            }
        }

        private void DeleteText(object sender, EventArgs e)
        {
            if (textBoxIP.Text == "localhost")
            {
                textBoxIP.Text = "";
            }
        }

        public void PanelConnect()
        {
            panel1.Visible = true;
        }

        public void PanelCreate()
        {
            panel2.Visible = true;
        }

        private async void buttonConnect_Click(object sender, EventArgs e)
        {
            UseWaitCursor = true;
            try
            {
                opponent = new TcpClient();
                await opponent.ConnectAsync(textBoxIP.Text, Int32.Parse(textBoxPort.Text));
                UseWaitCursor = false;
                DialogResult = DialogResult.OK;
            }
            catch (SocketException exc)
            {
                MessageBox.Show($"Fehler: {exc.Message}");
                UseWaitCursor = false;
                DialogResult = DialogResult.Abort;
            }
            Close();
        }

        private async void buttonCreateServer_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = true;
            server = new TcpListener(IPAddress.Any, Int32.Parse(textBoxServerPort.Text));
            server.Start();
            try
            {
                opponent = await server.AcceptTcpClientAsync();
                server.Stop();
                DialogResult = DialogResult.OK;
            }
            catch (Exception exc)
            {
                if (exc.HResult != -2146232798) //TODO: besseren Weg, abbruch zu erkennen
                {
                    DialogResult = DialogResult.Abort;
                }
                else
                {
                    throw exc;
                }
            }
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            server.Stop();
        }

        private void comboBoxProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            UsedProtocol = (TTTProtocol)comboBoxProtocol.SelectedIndex;
        }
    }
}
