﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToeGame
{
    public enum TTTProtocol
    {
        MK,
        Macho   //Noch nicht impelemtiert
    }

    public enum ConnRole
    {
        Client,
        Server,
        Disconnected
    }

    public class MoveEventArgs : EventArgs
    {
        public (int, int) Coordinates { get; private set; }
        public MoveEventArgs((int, int)coordinates)
        {
            Coordinates = coordinates;
        }
    }

    public class SyncTurnEventArgs : EventArgs
    {
        public Player player { get; private set; }
        public SyncTurnEventArgs(Player player)
        {
            this.player = player;
        }
    }

    public class ConnCloseEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public ConnCloseEventArgs(string Message)
        {
            this.Message = Message;
        }
    }

    public class OnlineTTT : TicTacToe
    {
        TcpClient opponent;
        BinaryWriter bw;
        BinaryReader br;
        Task receiver;
        CancellationTokenSource receiverCts = new CancellationTokenSource();
        public ConnRole Role { get; private set; } = ConnRole.Disconnected;
        public bool Connected
        {
            get
            {
                return Role != ConnRole.Disconnected;
            }
        }
        TTTProtocol usedProtocol;

        public event EventHandler OnReset;
        public event EventHandler OnSyncTurn;
        public event EventHandler OnMove;
        public event EventHandler OnConnectionEnd;

        public void HandleConnection(TcpClient opponent, ConnRole role, TTTProtocol protocol = TTTProtocol.MK)
        {
            this.opponent = opponent;
            bw = new BinaryWriter(opponent.GetStream());
            br = new BinaryReader(opponent.GetStream());
            receiver = new Task(() => HandleReceive(receiverCts.Token));
            receiver.Start();
            Role = role;
            usedProtocol = protocol;
        }

#region Commands
        public void Ping()
        {
            SendData(new byte[] { 0 });
        }

        public void ACK()
        {
            SendData(new byte[] { 1 });
        }

        public void SendReset()
        {
            if (Role == ConnRole.Server)
            {
                SendData(new byte[] { 2 });
                Console.WriteLine("[Sent] Reset");
            }
            else
            {
                throw new Exception("Only Server can Reset the Field");
            }
        }

        public void SendSyncTurn()
        {
            if (Role == ConnRole.Server)
            {
                byte p = (byte)(CurrentPlayer == Player.X ? 0 : 1);
                SendData(new byte[] { 3, p });
                Console.WriteLine("[Sent] SyncTurn");
            }
            else
            {
                throw new Exception("Only Server can sync current Turn state");
            }
        }

        public void SendMove((byte x, byte y) data)
        {
            SendData(new byte[] { 4, data.x, data.y });
            Console.WriteLine("[Sent] Move");
        }
#endregion

        /// <summary>
        /// Ruft ab, ob diese Instanz Spieler X oder O ist.
        /// Normalerweise ist der Server immer X.
        /// </summary>
        /// <returns></returns>
        public Player GetPlayer()
        {
            if (Role == ConnRole.Disconnected)
                throw new Exception("GetPlayer can only be called if connected");
            return (Role == ConnRole.Server ? Player.X : Player.O);
        }

        private void SendData(byte[] buffer)
        {
            if (buffer.Length < 256)
            {
                Console.WriteLine($"Sending data: {buffer[0]}");
                bw.Write((byte)buffer.Length);
                bw.Write(buffer);
                bw.Flush();
            }
            else
            {
                throw new Exception("Data can't be longer than 255 Bytes");
            }
        }

        public void CloseConnection(string msg = null)
        {
            receiverCts.Cancel();
            br.Dispose();
            bw.Dispose();
            opponent.Dispose();
            Role = ConnRole.Disconnected;
            OnConnectionEnd?.Invoke(this, new ConnCloseEventArgs(msg));
        }

        private void HandleReceive(CancellationToken ct)
        {
            if (usedProtocol == TTTProtocol.MK)
            {
                while (!ct.IsCancellationRequested)
                {
                    byte msgLength;
                    try
                    {
                        msgLength = br.ReadByte();
                    }
                    catch
                    {
                        if (!ct.IsCancellationRequested)
                            CloseConnection("Verbindung unterbrochen");
                        continue;
                    }

                    byte[] msg = new byte[msgLength];
                    try
                    {
                        msg = br.ReadBytes(msgLength);
                    }
                    catch
                    {
                        if (!ct.IsCancellationRequested)
                            CloseConnection("Verbindung unterbrochen");
                        continue;
                    }

                    Console.WriteLine($"Command Received: {msg[0]}");  //DEBUG

                    if (msg != default(byte[]))
                    {
                        IntepretCommand(msg);
                    }
                }
            }
        }

        /// <summary>
        /// Intepretiert ein Bytearray als Befehl.
        /// </summary>
        /// <param name="cmd">
        /// Der Befehl als Bytearray. Format:
        /// Reset:      { 2 }
        /// SyncTurn:   { 3,[Spieler] }     0 = Spieler X
        /// Move:       { 4,[x],[y] }
        /// </param>
        private void IntepretCommand(byte[] cmd)
        {
            switch (cmd[0])
            {
                case 0:   //Ping
                    ACK();
                    break;
                case 1:   //ACK
                    break;
                case 2:   //Reset
                    Console.WriteLine("[Received] Reset");
                    if (Role == ConnRole.Client)
                    {
                        Reset();
                        OnReset?.Invoke(this, new EventArgs());
                        Console.WriteLine("Reset Field");
                    }
                    break;
                case 3:   //SyncTurn[Player]
                    Console.WriteLine("[Received] SyncTurn");
                    if (Role == ConnRole.Client)
                    {
                        Player p = (cmd[1] == 0 ? Player.X : Player.O);
                        CurrentPlayer = p;
                        OnSyncTurn?.Invoke(this, new SyncTurnEventArgs(p));
                        Console.WriteLine($"Synced turn to: {p}");
                    }
                    break;
                case 4:   //Move[X][Y]
                    Console.WriteLine("[Received] Move");
                    if (GetPlayer() != CurrentPlayer)   //Move akzeptieren, wenn man selber nicht dran ist
                    {
                        OnMove?.Invoke(this, new MoveEventArgs((cmd[1], cmd[2])));
                        Console.WriteLine($"Received Move: ({cmd[1]}, {cmd[2]})");
                    }
                    break;
            }
        }
    }
}
