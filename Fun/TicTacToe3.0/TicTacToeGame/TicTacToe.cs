﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeGame
{
    public enum Player
    {
        None,
        X,
        O
    }

    public enum GameResult
    {
        XWon,
        OWon,
        Draw,
        None
    }

    public class TicTacToe
    {
        public Player[,] Field { get; private set; }
        public Player CurrentPlayer { get; protected set; }
        public bool ActiveGame { get; private set; } = false;

        public TicTacToe()
        {
            Reset();
        }

        /// <summary>
        /// Ändert den jetztigen Spieler.
        /// </summary>
        /// <returns>Spieler, der nach Aufruf dran ist.</returns>
        public Player NextTurn()
        {
            CurrentPlayer = (CurrentPlayer == Player.X ? Player.O : Player.X);
            return CurrentPlayer;
        }

        /// <summary>
        /// Setzt das Spielfeld zurück.
        /// </summary>
        /// <param name="currentPlayer">Der Spieler, der dran sein möchte.</param>
        /// <returns>Spieler, der im neuen Spiel dran ist.</returns>
        public Player Reset(Player currentPlayer = Player.None)
        {
            Field = new Player[3, 3];
            CurrentPlayer = (currentPlayer != Player.None ? currentPlayer : (new Random().Next(0, 2) == 0 ? Player.X : Player.O));
            ActiveGame = true;
            return CurrentPlayer;
        }


        /// <summary>
        /// Setzt ein Feld auf den derzeitigen Spieler.
        /// </summary>
        /// <returns>True, wenn Feld erfolgreich gesetzt wurde.</returns>
        public bool SetField(int x, int y)
        {
            if (ActiveGame && Field[x, y] == default(Player))
            {
                Field[x, y] = CurrentPlayer;
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Setzt ein Feld auf den derzeitigen Spieler.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>True, wenn Feld erfolgreich gesetzt wurde.</returns>
        public bool SetField((int x, int y) data)
        {
            return SetField(data.x, data.y);
        }

        /// <summary>
        /// Prüft, ob jemand gewonnen hat.
        /// </summary>
        /// <returns>Ein GameResult enum.</returns>
        public GameResult CheckForWin()
        {
            Player[] check = { Player.X, Player.O };
            foreach (Player s in check)
            {
                if (Field[0, 0] == Field[0, 1] && Field[0, 1] == Field[0, 2] && Field[0, 2] == s || //horizontal
                    Field[1, 0] == Field[1, 1] && Field[1, 1] == Field[1, 2] && Field[1, 2] == s ||
                    Field[2, 0] == Field[2, 1] && Field[2, 1] == Field[2, 2] && Field[2, 2] == s ||
                    Field[0, 0] == Field[1, 0] && Field[1, 0] == Field[2, 0] && Field[2, 0] == s || //vertikal
                    Field[0, 1] == Field[1, 1] && Field[1, 1] == Field[2, 1] && Field[2, 1] == s ||
                    Field[0, 2] == Field[1, 2] && Field[1, 2] == Field[2, 2] && Field[2, 2] == s ||
                    Field[0, 0] == Field[1, 1] && Field[1, 1] == Field[2, 2] && Field[2, 2] == s || //schräg
                    Field[2, 0] == Field[1, 1] && Field[1, 1] == Field[0, 2] && Field[0, 2] == s)
                {
                    ActiveGame = false;
                    return (s == Player.X ? GameResult.XWon : GameResult.OWon);
                }
            }
            if (Field[0, 0] != default(Player) && Field[0, 1] != default(Player) && Field[0, 2] != default(Player) &&
                Field[1, 0] != default(Player) && Field[1, 1] != default(Player) && Field[1, 2] != default(Player) &&
                Field[2, 0] != default(Player) && Field[2, 1] != default(Player) && Field[2, 2] != default(Player))
            {
                ActiveGame = false;
                return GameResult.Draw;
            }
            return GameResult.None;
        }
    }
}
