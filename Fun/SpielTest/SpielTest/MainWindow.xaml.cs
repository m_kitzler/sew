﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpielTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            mainGrid.Width = SystemParameters.PrimaryScreenWidth;
            mainGrid.Height = SystemParameters.PrimaryScreenHeight;
            LocationChanged += NewLocation;
        }

        private void NewLocation(object sender, EventArgs e)
        {
            mainGrid.Margin = new Thickness(-Left, -Top, 0, 0);
        }
    }
}
