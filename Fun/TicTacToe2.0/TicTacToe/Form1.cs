﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        int TurnX = (new Random()).Next(0, 2);  //1 entspricht true
        Player x = new Player("DefX");
        Player o = new Player("DefY");
        public Form1()
        {
            InitializeComponent();
            SetTurn(TurnX);
        }

        private void SetTurn(int value)
        {
            labelTurn.Text = (value == 1 ? "X" : "O") + " ist am Zug";
        }

        private void button_MouseClick(object sender, MouseEventArgs e)
        {
            if (TurnX == 1 && e.Button == MouseButtons.Left)
            {
                Check((sender as Button), "X");
                CheckForWin();;
            }
            else if (TurnX == 0 && e.Button == MouseButtons.Right)
            {
                Check((sender as Button), "O");
                CheckForWin();;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Button[] buttons = { button1, button2, button3, button4, button5, button6, button7, button8, button9 };
            if (TurnX == 1 && x.mapping.Contains(e.KeyCode))
            {
                for (int i = 0; i < 9; i++)
                {
                    if (e.KeyCode == x.mapping[i])
                    {
                        Check(buttons[i],"X");
                        CheckForWin();;
                    }
                }
            }
            else if (TurnX == 0 && o.mapping.Contains(e.KeyCode))
            {
                for (int i = 0; i < 9; i++)
                {
                    if (e.KeyCode == o.mapping[i])
                    {
                        Check(buttons[i],"O");
                        CheckForWin();;
                    }
                }
            }
        }

        private void Check(Button b, string set)
        {
            if (b.Text == "")
            {
                b.Text = set;
                TurnX = (TurnX == 1) ? 0 : 1;
                SetTurn(TurnX);
            }
        }

        private void CheckForWin()
        {
            string[] check = { "X", "O" };
            foreach (string s in check)
            {
                if (button1.Text == s && button2.Text == s && button3.Text == s || //horizontal
                    button4.Text == s && button5.Text == s && button6.Text == s ||
                    button7.Text == s && button8.Text == s && button9.Text == s ||
                    button1.Text == s && button4.Text == s && button7.Text == s || //vertikal
                    button2.Text == s && button5.Text == s && button8.Text == s ||
                    button3.Text == s && button6.Text == s && button9.Text == s ||
                    button1.Text == s && button5.Text == s && button9.Text == s || //schräg
                    button3.Text == s && button5.Text == s && button7.Text == s)
                {
                    MessageBox.Show("Spieler \"" + s + "\" hat gewonnen!");
                    Reset();
                    break;
                }
            }
            if (button1.Text != "" && button2.Text != "" && button3.Text != "" &&
                button4.Text != "" && button5.Text != "" && button6.Text != "" &&
                button7.Text != "" && button8.Text != "" && button9.Text != "")
            {
                MessageBox.Show("Unentschieden!");
                Reset();
            }
        }

        private void Reset()
        {
            Button[] buttons = { button1, button2, button3, button4, button5, button6, button7, button8, button9 };
            foreach (Button b in buttons)
            {
                b.Text = "";
            }
            TurnX = (new Random()).Next(0, 2);
            SetTurn(TurnX);
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2(x, o);
            f2.Show();
            x = f2.PlayerX;
            o = f2.PlayerO;
        }
    }
}
