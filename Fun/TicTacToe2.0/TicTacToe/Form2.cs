﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form2 : Form
    {
        Keys NewKey;
        Button[] Buttons;
        public Player PlayerX { get; private set; }
        public Player PlayerO { get; private set; }
        public Form2(Player PlayerX, Player PlayerO)
        {
            InitializeComponent();
            this.PlayerX = PlayerX;
            this.PlayerO = PlayerO;
            Buttons = new Button[] { button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16, button17, button18 };
            for (int i = 0; i < 9; i++)
            {
                Buttons[i].Tag = i;
                Buttons[i].Text = PlayerX.mapping[i].ToString();
            }
            for (int i = 9; i < 18; i++)
            {
                Buttons[i].Tag = i;
                Buttons[i].Text = PlayerO.mapping[i - 9].ToString();
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Keys Temp = NewKey;
            int button = (int)(sender as Button).Tag;
            NewKey = (button < 9) ? PlayerX.mapping[button] : PlayerO.mapping[button - 9];
            Buttons[button].Text = "Drücke eine Taste...";
            Buttons[button].KeyDown += (object o, KeyEventArgs eve) => NewKey = eve.KeyCode;
            while (NewKey == Temp) { Thread.Sleep(10); }
            if (NewKey != Temp)
            {
                if (button < 9)
                {
                    PlayerX.mapping[button] = NewKey;
                }
                else
                {
                    PlayerO.mapping[button - 9] = NewKey;
                }
                Buttons[button].Text = NewKey.ToString();
            }
        }
    }
}
