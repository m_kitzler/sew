﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public class Player
    {
        public Keys[] mapping { get; set; } = new Keys[9];
        public Player(string Default)
        {
            if (Default == "DefX")
            {
                Keys[] map = { Keys.Q, Keys.W, Keys.E, Keys.A, Keys.S, Keys.D, Keys.Y, Keys.X, Keys.C };
                Array.Copy(map, mapping, 9);
            }
            if (Default == "DefY")
            {
                Keys[] map = { Keys.I, Keys.O, Keys.P, Keys.K, Keys.L, Keys.Oem3, Keys.Oemcomma, Keys.OemPeriod, Keys.OemMinus };
                Array.Copy(map, mapping, 9);
            }
        }
    }
}
