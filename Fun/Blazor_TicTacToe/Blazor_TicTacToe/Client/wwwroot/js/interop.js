﻿function showModal(modalId) {
    $("#" + modalId).modal("show")
}

function hideModal(modalId) {
    $("#" + modalId).modal("hide")
}

function toggleModal(modalId) {
    $("#" + modalId).modal("toggle")
}