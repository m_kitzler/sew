﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Console.ForegroundColor = ConsoleColor.Green;
            #region Variablen und so
            bool nochmal = true;
            bool restartgame = true;
            bool spiel = true;
            bool einzelspieler = false;
            bool computerspielzug = false;
            Random r = new Random();
            ConsoleKeyInfo Gamemodeausw;
            string gamemodeausw = "";
            ConsoleKeyInfo Eingabe;
            string eingabe = "";
            ConsoleKeyInfo Endauswahl;
            string endauswahl = "";
            string[] spielfeld = {"0", "0", "0", "0", "0", "0", "0", "0", "0"};
            string sp1 = "Spieler 1";
            string sp2name = "Spieler 2";
            string sp2 = sp2name;
            int zwspeicher = 100;
            int aktuellerzug;
            int forschleife;
            #endregion
            while (restartgame)
            {
                nochmal = true;
                #region Spielmoduseingabe
                for (int x = 1; x != 0; x--)
                {
                    Console.Write("Tic Tac Toe\n1) Einzelspieler\n2) Mehrspieler");
                    Gamemodeausw = Console.ReadKey();
                    gamemodeausw = Gamemodeausw.Key.ToString();
                    switch (gamemodeausw)
                    {
                        case "D1":
                            einzelspieler = true;
                            break;
                        case "D2":
                            einzelspieler = false;
                            break;
                        default:
                            x++;
                            Console.Clear();
                            Console.Write("Tic Tac Toe\n1) Einzelspieler\n2) Mehrspieler\nKeine gültige Eingabe");
                            Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                    Console.Clear();
                }
                #endregion
                #region Spiel
                while (nochmal)
                {
                    aktuellerzug = r.Next(1, 3);
                    if (einzelspieler == true)
                        sp2 = "Computer";
                    else
                        sp2 = sp2name;
                    while (spiel)
                    {
                        Eingabe = default(ConsoleKeyInfo);
                        #region Eingabe Spieler 1
                        if (aktuellerzug == 1)
                        {
                            for (forschleife = 1; forschleife != 0; forschleife--)
                            {
                                Console.Clear();
                                Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                Console.WriteLine("\nSpieler 1 ist dran");
                                Eingabe = Console.ReadKey();
                                eingabe = Eingabe.Key.ToString();
                                switch (eingabe)
                                {
                                    case "NumPad7":
                                        zwspeicher = 0;
                                        break;
                                    case "NumPad8":
                                        zwspeicher = 1;
                                        break;
                                    case "NumPad9":
                                        zwspeicher = 2;
                                        break;
                                    case "NumPad4":
                                        zwspeicher = 3;
                                        break;
                                    case "NumPad5":
                                        zwspeicher = 4;
                                        break;
                                    case "NumPad6":
                                        zwspeicher = 5;
                                        break;
                                    case "NumPad1":
                                        zwspeicher = 6;
                                        break;
                                    case "NumPad2":
                                        zwspeicher = 7;
                                        break;
                                    case "NumPad3":
                                        zwspeicher = 8;
                                        break;
                                }
                                if (spielfeld[zwspeicher] == "0")
                                    spielfeld[zwspeicher] = "x";
                                else
                                {
                                    forschleife++;
                                    Console.Clear();
                                    Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                    anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                    Console.WriteLine("\nEingabe ungültig!");
                                    Thread.Sleep(1000);
                                    Console.Clear();
                                }
                            }
                            aktuellerzug = 2;
                        }
                        #endregion
                        #region Eingabe Spieler 2 / Computer
                        else
                        if (einzelspieler == false)
                        {
                            #region Spieler 2
                            if (aktuellerzug == 2)
                            {
                                for (forschleife = 1; forschleife != 0; forschleife--)
                                {
                                    Console.Clear();
                                    Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                    anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                    Console.WriteLine("\n{0} ist dran", sp2);
                                    Eingabe = Console.ReadKey();
                                    eingabe = Eingabe.Key.ToString();
                                    switch (eingabe)
                                    {
                                        case "NumPad7":
                                            zwspeicher = 0;
                                            break;
                                        case "NumPad8":
                                            zwspeicher = 1;
                                            break;
                                        case "NumPad9":
                                            zwspeicher = 2;
                                            break;
                                        case "NumPad4":
                                            zwspeicher = 3;
                                            break;
                                        case "NumPad5":
                                            zwspeicher = 4;
                                            break;
                                        case "NumPad6":
                                            zwspeicher = 5;
                                            break;
                                        case "NumPad1":
                                            zwspeicher = 6;
                                            break;
                                        case "NumPad2":
                                            zwspeicher = 7;
                                            break;
                                        case "NumPad3":
                                            zwspeicher = 8;
                                            break;
                                    }
                                    if (spielfeld[zwspeicher] == "0")
                                        spielfeld[zwspeicher] = "o";
                                    else
                                    {
                                        forschleife++;
                                        Console.Clear();
                                        Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                        anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                        Console.WriteLine("\nEingabe ungültig!");
                                        Thread.Sleep(1000);
                                        Console.Clear();
                                    }
                                }
                                aktuellerzug = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            #region Computer
                            if (aktuellerzug == 2)
                            {
                                Console.Clear();
                                Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                Console.WriteLine("\n{0} ist dran", sp2);
                                Thread.Sleep(1000);
                                for (forschleife = 1; forschleife != 0; forschleife--)
                                {
                                    #region Gewinnen
                                    //erste Zeile
                                    if (spielfeld[0] == "o" && spielfeld[1] == "o" && spielfeld[2] == "0")
                                    {
                                        spielfeld[2] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[1] == "o" && spielfeld[2] == "o" && spielfeld[0] == "0")
                                    {
                                        spielfeld[0] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[0] == "o" && spielfeld[2] == "o" && spielfeld[1] == "0")
                                    {
                                        spielfeld[1] = "o";
                                        computerspielzug = true;
                                    }
                                    //zweite Zeile
                                    else
                                    if (spielfeld[3] == "o" && spielfeld[4] == "o" && spielfeld[5] == "0")
                                    {
                                        spielfeld[5] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[4] == "o" && spielfeld[5] == "o" && spielfeld[3] == "0")
                                    {
                                        spielfeld[3] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[3] == "o" && spielfeld[5] == "o" && spielfeld[4] == "0")
                                    {
                                        spielfeld[4] = "o";
                                        computerspielzug = true;
                                    }
                                    //dritte Zeile
                                    else
                                    if (spielfeld[6] == "o" && spielfeld[7] == "o" && spielfeld[8] == "0")
                                    {
                                        spielfeld[8] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[7] == "o" && spielfeld[8] == "o" && spielfeld[6] == "0")
                                    {
                                        spielfeld[6] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[6] == "o" && spielfeld[8] == "o" && spielfeld[7] == "0")
                                    {
                                        spielfeld[7] = "o";
                                        computerspielzug = true;
                                    }
                                    //erste Spalte
                                    else
                                    if (spielfeld[0] == "o" && spielfeld[3] == "o" && spielfeld[6] == "0")
                                    {
                                        spielfeld[6] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[3] == "o" && spielfeld[6] == "o" && spielfeld[0] == "0")
                                    {
                                        spielfeld[0] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[0] == "o" && spielfeld[6] == "o" && spielfeld[3] == "0")
                                    {
                                        spielfeld[3] = "o";
                                        computerspielzug = true;
                                    }
                                    //zweite Spalte
                                    else
                                    if (spielfeld[1] == "o" && spielfeld[4] == "o" && spielfeld[7] == "0")
                                    {
                                        spielfeld[7] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[4] == "o" && spielfeld[7] == "o" && spielfeld[1] == "0")
                                    {
                                        spielfeld[1] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[1] == "o" && spielfeld[7] == "o" && spielfeld[4] == "0")
                                    {
                                        spielfeld[4] = "o";
                                        computerspielzug = true;
                                    }
                                    //dritte Spalte
                                    else
                                    if (spielfeld[2] == "o" && spielfeld[5] == "o" && spielfeld[8] == "0")
                                    {
                                        spielfeld[8] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[5] == "o" && spielfeld[8] == "o" && spielfeld[2] == "0")
                                    {
                                        spielfeld[2] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[2] == "o" && spielfeld[8] == "o" && spielfeld[5] == "0")
                                    {
                                        spielfeld[5] = "o";
                                        computerspielzug = true;
                                    }
                                    //diagonal oben links unten rechts
                                    else
                                    if (spielfeld[0] == "o" && spielfeld[4] == "o" && spielfeld[8] == "0")
                                    {
                                        spielfeld[8] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[4] == "o" && spielfeld[8] == "o" && spielfeld[0] == "0")
                                    {
                                        spielfeld[0] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[0] == "o" && spielfeld[4] == "o" && spielfeld[8] == "0")
                                    {
                                        spielfeld[8] = "o";
                                        computerspielzug = true;
                                    }
                                    //diagonal oben rechts unten links
                                    else
                                    if (spielfeld[2] == "o" && spielfeld[4] == "o" && spielfeld[6] == "0")
                                    {
                                        spielfeld[6] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[4] == "o" && spielfeld[6] == "o" && spielfeld[2] == "0")
                                    {
                                        spielfeld[2] = "o";
                                        computerspielzug = true;
                                    }
                                    else
                                    if (spielfeld[2] == "o" && spielfeld[6] == "o" && spielfeld[4] == "0")
                                    {
                                        spielfeld[4] = "o";
                                        computerspielzug = true;
                                    }
                                    #endregion
                                    #region Niederlage verhindern
                                    if (computerspielzug == false)
                                    {
                                        //erste Zeile
                                        if (spielfeld[0] == "x" && spielfeld[1] == "x" && spielfeld[2] == "0")
                                            spielfeld[2] = "o";
                                        else
                                        if (spielfeld[1] == "x" && spielfeld[2] == "x" && spielfeld[0] == "0")
                                            spielfeld[0] = "o";
                                        else
                                        if (spielfeld[0] == "x" && spielfeld[2] == "x" && spielfeld[1] == "0")
                                            spielfeld[1] = "o";
                                        //zweite Zeile
                                        else
                                        if (spielfeld[3] == "x" && spielfeld[4] == "x" && spielfeld[5] == "0")
                                            spielfeld[5] = "o";
                                        else
                                        if (spielfeld[4] == "x" && spielfeld[5] == "x" && spielfeld[3] == "0")
                                            spielfeld[3] = "o";
                                        else
                                        if (spielfeld[3] == "x" && spielfeld[5] == "x" && spielfeld[4] == "0")
                                            spielfeld[4] = "o";
                                        //dritte Zeile
                                        else
                                        if (spielfeld[6] == "x" && spielfeld[7] == "x" && spielfeld[8] == "0")
                                            spielfeld[8] = "o";
                                        else
                                        if (spielfeld[7] == "x" && spielfeld[8] == "x" && spielfeld[6] == "0")
                                            spielfeld[6] = "o";
                                        else
                                        if (spielfeld[6] == "x" && spielfeld[8] == "x" && spielfeld[7] == "0")
                                            spielfeld[7] = "o";
                                        //erste Spalte
                                        else
                                        if (spielfeld[0] == "x" && spielfeld[3] == "x" && spielfeld[6] == "0")
                                            spielfeld[6] = "o";
                                        else
                                        if (spielfeld[3] == "x" && spielfeld[6] == "x" && spielfeld[0] == "0")
                                            spielfeld[0] = "o";
                                        else
                                        if (spielfeld[0] == "x" && spielfeld[6] == "x" && spielfeld[3] == "0")
                                            spielfeld[3] = "o";
                                        //zweite Spalte
                                        else
                                        if (spielfeld[1] == "x" && spielfeld[4] == "x" && spielfeld[7] == "0")
                                            spielfeld[7] = "o";
                                        else
                                        if (spielfeld[4] == "x" && spielfeld[7] == "x" && spielfeld[1] == "0")
                                            spielfeld[1] = "o";
                                        else
                                        if (spielfeld[1] == "x" && spielfeld[7] == "x" && spielfeld[4] == "0")
                                            spielfeld[4] = "o";
                                        //dritte Spalte
                                        else
                                        if (spielfeld[2] == "x" && spielfeld[5] == "x" && spielfeld[8] == "0")
                                            spielfeld[8] = "o";
                                        else
                                        if (spielfeld[5] == "x" && spielfeld[8] == "x" && spielfeld[2] == "0")
                                            spielfeld[2] = "o";
                                        else
                                        if (spielfeld[2] == "x" && spielfeld[8] == "x" && spielfeld[5] == "0")
                                            spielfeld[5] = "o";
                                        //diagonal oben links unten rechts
                                        else
                                        if (spielfeld[0] == "x" && spielfeld[4] == "x" && spielfeld[8] == "0")
                                            spielfeld[8] = "o";
                                        else
                                        if (spielfeld[4] == "x" && spielfeld[8] == "x" && spielfeld[0] == "0")
                                            spielfeld[0] = "o";
                                        else
                                        if (spielfeld[0] == "x" && spielfeld[4] == "x" && spielfeld[8] == "0")
                                            spielfeld[8] = "o";
                                        //diagonal oben rechts unten links
                                        else
                                        if (spielfeld[2] == "x" && spielfeld[4] == "x" && spielfeld[6] == "0")
                                            spielfeld[6] = "o";
                                        else
                                        if (spielfeld[4] == "x" && spielfeld[6] == "x" && spielfeld[2] == "0")
                                            spielfeld[2] = "o";
                                        else
                                        if (spielfeld[2] == "x" && spielfeld[6] == "x" && spielfeld[4] == "0")
                                            spielfeld[4] = "o";
                                        #endregion
                                        #region Random
                                        else
                                        {
                                            zwspeicher = r.Next(0, 9);
                                            if (spielfeld[zwspeicher] == "0")
                                                spielfeld[zwspeicher] = "o";
                                            else
                                            {
                                                forschleife++;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                computerspielzug = false;
                                aktuellerzug = 1;
                            }
                            #endregion
                        }
                        #endregion
                        #region Auswertung
                        if ((spielfeld[0] == "x" && spielfeld[1] == "x" && spielfeld[2] == "x") || (spielfeld[3] == "x" && spielfeld[4] == "x" && spielfeld[5] == "x") || (spielfeld[6] == "x" && spielfeld[7] == "x" && spielfeld[8] == "x") || (spielfeld[0] == "x" && spielfeld[3] == "x" && spielfeld[6] == "x") || (spielfeld[1] == "x" && spielfeld[4] == "x" && spielfeld[7] == "x") || (spielfeld[2] == "x" && spielfeld[5] == "x" && spielfeld[8] == "x") || (spielfeld[0] == "x" && spielfeld[4] == "x" && spielfeld[8] == "x") || (spielfeld[2] == "x" && spielfeld[4] == "x" && spielfeld[6] == "x"))
                        {
                            Console.Clear();
                            Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                            anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                            Console.WriteLine("\n{0} hat Gewonnen!", sp1);
                            spiel = false;
                        }
                        else
                        if ((spielfeld[0] == "o" && spielfeld[1] == "o" && spielfeld[2] == "o") || (spielfeld[3] == "o" && spielfeld[4] == "o" && spielfeld[5] == "o") || (spielfeld[6] == "o" && spielfeld[7] == "o" && spielfeld[8] == "o") || (spielfeld[0] == "o" && spielfeld[3] == "o" && spielfeld[6] == "o") || (spielfeld[1] == "o" && spielfeld[4] == "o" && spielfeld[7] == "o") || (spielfeld[2] == "o" && spielfeld[5] == "o" && spielfeld[8] == "o") || (spielfeld[0] == "o" && spielfeld[4] == "o" && spielfeld[8] == "o") || (spielfeld[2] == "o" && spielfeld[4] == "o" && spielfeld[6] == "o"))
                        {
                            Console.Clear();
                            Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                            anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                            Console.WriteLine("\n{0} hat Gewonnen!", sp2);
                            spiel = false;
                        }
                        else
                        if ((spielfeld[0] == "o" || spielfeld[0] == "x") && (spielfeld[1] == "o" || spielfeld[1] == "x") && (spielfeld[2] == "o" || spielfeld[2] == "x") && (spielfeld[3] == "o" || spielfeld[3] == "x") && (spielfeld[4] == "o" || spielfeld[4] == "x") && (spielfeld[5] == "o" || spielfeld[5] == "x") && (spielfeld[6] == "o" || spielfeld[6] == "x") && (spielfeld[7] == "o" || spielfeld[7] == "x") && (spielfeld[8] == "o" || spielfeld[8] == "x"))
                        {
                            Console.Clear();
                            Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                            anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                            Console.WriteLine("\nUnentschieden!");
                            spiel = false;
                        }
                        #endregion
                    }
                    #endregion
                    #region Nach Spiel
                    for (int x = 1; x != 0; x--)
                    {
                        Console.WriteLine("\n1)Nochmal\n2)Hauptmenü\n3)Beenden");
                        Endauswahl = Console.ReadKey();
                        endauswahl = Endauswahl.Key.ToString();
                        Console.Clear();
                        switch (endauswahl)
                        {
                            case "D1":
                                nochmal = true;
                                spiel = true;
                                for (int i = 0; i < 9; i++)
                                    spielfeld[i] = "0";
                                break;
                            case "D2":
                                nochmal = false;
                                spiel = true;
                                for (int i = 0; i < 9; i++)
                                    spielfeld[i] = "0";
                                break;
                            case "D3":
                                nochmal = false;
                                restartgame = false;
                                break;
                            default:
                                Console.Clear();
                                Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                Console.WriteLine("\nKeine gültige Eingabe");
                                Thread.Sleep(1000);
                                Console.Clear();
                                Console.WriteLine("{0}  X\t\t\tO  {1}\n", sp1, sp2);
                                anzeige(spielfeld[0], spielfeld[1], spielfeld[2], spielfeld[3], spielfeld[4], spielfeld[5], spielfeld[6], spielfeld[7], spielfeld[8]);
                                x++;
                                break;
                        }
                    }
                }
                #endregion
            }
        }
        static void anzeige (string a, string b, string c, string d, string e, string f, string g, string h, string i)
        {
            #region erste Zeile
            //erste Ziele der ersten Zeile
            if (a == "x")
                Console.Write("\\_/");
            else
            if (a == "o")
                Console.Write("|¯|");
            else
                Console.Write("7  ");
            Console.Write(" | ");
            if (b == "x")
                Console.Write("\\_/");
            else
            if (b == "o")
                Console.Write("|¯|");
            else
                Console.Write("8  ");
            Console.Write(" | ");
            if (c == "x")
                Console.Write("\\_/");
            else
            if (c == "o")
                Console.Write("|¯|");
            else
                Console.Write("9  ");
            Console.WriteLine("");
            //zweite Zeile der ersten Zeile
            if (a == "x")
                Console.Write("/ \\");
            else
            if (a == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (b == "x")
                Console.Write("/ \\");
            else
            if (b == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (c == "x")
                Console.Write("/ \\");
            else
            if (c == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            //dritte Zeile der ersten Zeile
            Console.Write("____|_____|____\n");
            #endregion
            #region zweite Zeile
            //erste Ziele der zweiten Zeile
            if (d == "x")
                Console.Write("\\_/");
            else
            if (d == "o")
                Console.Write("|¯|");
            else
                Console.Write("4  ");
            Console.Write(" | ");
            if (e == "x")
                Console.Write("\\_/");
            else
            if (e == "o")
                Console.Write("|¯|");
            else
                Console.Write("5  ");
            Console.Write(" | ");
            if (f == "x")
                Console.Write("\\_/");
            else
            if (f == "o")
                Console.Write("|¯|");
            else
                Console.Write("6  ");
            Console.WriteLine("");
            //zweite Zeile der zweiten Zeile
            if (d == "x")
                Console.Write("/ \\");
            else
            if (d == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (e == "x")
                Console.Write("/ \\");
            else
            if (e == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (f == "x")
                Console.Write("/ \\");
            else
            if (f == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            //dritte Zeile der zweiten Zeile
            Console.Write("____|_____|____\n");
            #endregion
            #region dritte Zeile
            //erste Ziele der dritten Zeile
            if (g == "x")
                Console.Write("\\_/");
            else
            if (g == "o")
                Console.Write("|¯|");
            else
                Console.Write("1  ");
            Console.Write(" | ");
            if (h == "x")
                Console.Write("\\_/");
            else
            if (h == "o")
                Console.Write("|¯|");
            else
                Console.Write("2  ");
            Console.Write(" | ");
            if (i == "x")
                Console.Write("\\_/");
            else
            if (i == "o")
                Console.Write("|¯|");
            else
                Console.Write("3  ");
            Console.WriteLine("");
            //zweite Zeile der dritten Zeile
            if (g == "x")
                Console.Write("/ \\");
            else
            if (g == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (h == "x")
                Console.Write("/ \\");
            else
            if (h == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (i == "x")
                Console.Write("/ \\");
            else
            if (i == "o")
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            #endregion
        }
    }
}
