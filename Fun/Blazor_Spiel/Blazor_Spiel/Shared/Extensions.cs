﻿using System;

namespace Blazor_Spiel.Shared
{
    public static class Extensions
    {
        public static string ToFormatted(this double d)
        {
            return d.ToString("0.##").Replace(",", ".");
        }

        /// <summary>
        /// Calculates the cosine of a degree angle.
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static double Cos(this int angle)
        {
            return Math.Cos(Math.PI * (angle / 180.0));
        }

        /// <summary>
        /// Calculates the sine of a degree angle.
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static double Sin(this int angle)
        {
            return Math.Sin(Math.PI * (angle / 180.0));
        }

        public static string DisplayPower(this Tank t)
        {
            return $"<path stroke=\"grey\" stroke-width=\"0.5\" d=\"M {t.Coords} m 0 -2 m {(t.Angle.Cos() * 4).ToFormatted()} {(t.Angle.Sin() * -4).ToFormatted()} l {(t.Angle.Cos() * 4).ToFormatted()} {(t.Angle.Sin() * -4).ToFormatted()} 0\" />" +
                $"<path stroke=\"red\" stroke-width=\"0.5\" d=\"M {t.Coords} m 0 -2 m {(t.Angle.Cos() * 4).ToFormatted()} {(t.Angle.Sin() * -4).ToFormatted()} l {(t.Angle.Cos() * (t.PowerPercent / 100.0) * 4).ToFormatted()} {(t.Angle.Sin() * (t.PowerPercent / 100.0) * -4).ToFormatted()} 0\" />";
        }
    }
}
