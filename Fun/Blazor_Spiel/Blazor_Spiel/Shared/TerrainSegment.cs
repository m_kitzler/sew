﻿using System;

namespace Blazor_Spiel.Shared
{
    public enum TerrainType
    {
        Line,
        Bezier
    }

    public class TerrainSegment
    {
        public TerrainType Type { get; private set; }
        public Coordinates[] Coords { get; private set; }
        public double Start
        {
            get
            {
                return Coords[0].X;
            }
        }
        public double End
        {
            get
            {
                return Coords[Coords.Length - 1].X;
            }
        }

        public static TerrainSegment Line(Coordinates start, Coordinates end)
        {
            return new TerrainSegment() { Type = TerrainType.Line, Coords = new Coordinates[] { start, end } };
        }

        public static TerrainSegment Bezier(Coordinates start, Coordinates startModifier, Coordinates endModifier, Coordinates end)
        {
            return new TerrainSegment() { Type = TerrainType.Bezier, Coords = new Coordinates[] { start, startModifier, endModifier, end } };
        }

        public double this[double x]
        {
            get
            {
                switch (Type)
                {
                    case TerrainType.Line:
                        return (Coords[1].Y - Coords[0].Y) / (Coords[1].X - Coords[0].X) * x + Coords[0].Y;
                    case TerrainType.Bezier:
                        return CubicBezierY(Start, End, x, Coords[0].Y, Coords[1].Y, Coords[2].Y, Coords[3].Y);
                    default:
                        return -1;
                }
            }
        }

        public double CubicBezierY(double start, double end, double at, double y1, double y2, double y3, double y4)
        {
            double t = (at - start) / (end - start);
            return Math.Pow(1 - t, 3) * y1 + 3 * t * Math.Pow(1 - t, 2) * y2 + 3 * Math.Pow(t, 2) * (1 - t) * y3 + Math.Pow(t, 3) * y4;
        }

        // Credit: https://stackoverflow.com/a/25759171
        /// <summary>
        /// Calculate a bezier height Y of a parameter in the range [start..end]
        /// </summary>
        public double CalculateBezierHeightInInterval(double start, double end, double param, double y1, double y2, double y3, double y4)
        {
            return CalculateBezierHeight((param - start) / (end - start), y1, y2, y3, y4);
        }

        /// <summary>
        /// Calculate a bezier height Y of a parameter in the range [0..1]
        /// </summary>
        public double CalculateBezierHeight(double t, double y1, double y2, double y3, double y4)
        {
            double tPower3 = t * t * t;
            double tPower2 = t * t;
            double oneMinusT = 1 - t;
            double oneMinusTPower3 = oneMinusT * oneMinusT * oneMinusT;
            double oneMinusTPower2 = oneMinusT * oneMinusT;
            double Y = oneMinusTPower3 * y1 + (3 * oneMinusTPower2 * t * y2) + (3 * oneMinusT * tPower2 * y3) + tPower3 * y4;
            return Y;
        }

        public string ToPath()
        {
            switch (Type)
            {
                case TerrainType.Line:
                    return $"L {Coords[1]}";
                case TerrainType.Bezier:
                    return $"C {Coords[1]}, {Coords[2]}, {Coords[3]}";
                default:
                    return null;
            }
        }

        public string ToPathWithStart()
        {
            return $"M {Coords[0]} {ToPath()}";
        }
    }
}
