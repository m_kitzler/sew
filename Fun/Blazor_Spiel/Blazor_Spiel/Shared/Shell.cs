﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blazor_Spiel.Shared
{
    public class Shell : AEntity
    {
        public Shell(Coordinates coords, Func<APhysicsObject, int, bool> movementValidation, double speedX, double speedY) : base(coords, movementValidation, speedX, speedY)
        {

        }

        public override string ToPath()
        {
            return $"<circle cx=\"{Coords.X.ToFormatted()}\" cy=\"{Coords.Y.ToFormatted()}\" r=\"0.2\"/>";
        }
    }
}
