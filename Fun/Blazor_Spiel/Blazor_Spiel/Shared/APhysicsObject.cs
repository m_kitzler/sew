﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blazor_Spiel.Shared
{
    public abstract class APhysicsObject
    {
        public Coordinates Coords { get; private set; }
        public double SpeedX { get; set; }
        public double SpeedY { get; set; }
        public Func<APhysicsObject, int, bool> MovementValidation { get; set; }
        public bool IsGrounded { get; private set; } = false;
        public event EventHandler OnGrounded;

        public APhysicsObject(Coordinates coords, Func<APhysicsObject, int, bool> movementValidation = null, double speedX = 0, double speedY = 0)
        {
            Coords = coords;
            MovementValidation = movementValidation;
            SpeedX = speedX;
            SpeedY = speedY;
        }

        public virtual void Tick(int tickCount)
        {
            bool stop = false, ig = false;
            for (int i = 1; i <= 3; i++)
            {
                if (MovementValidation == null || MovementValidation(this, i))
                {
                    Coords.X += SpeedX;
                    Coords.Y += SpeedY;
                    break;
                }
                else
                {
                    stop = true;
                }
            }
            if (stop)
            {
                SpeedX = 0;
                SpeedY = 0;
                ig = true;
                OnGrounded?.Invoke(this, null);
            }
            SpeedY += 9.81 / 60;
            IsGrounded = ig;
        }
    }
}
