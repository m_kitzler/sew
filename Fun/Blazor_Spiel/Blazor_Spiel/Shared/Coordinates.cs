﻿using System;

namespace Blazor_Spiel.Shared
{
    public class Coordinates
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double YInversionBase { get; set; }

        public Coordinates(double x, double y, double yInversionBase = 0)
        {
            X = x;
            Y = y;
            YInversionBase = yInversionBase;    //TODO
        }

        public double DistanceTo(Coordinates c)
        {
            double diffX = Math.Abs(X - c.X), diffY = Math.Abs(Y - c.Y);
            return Math.Sqrt(Math.Pow(diffX, 2) + Math.Pow(diffY, 2));  //Pythagoras
        }


        public override string ToString()
        {
            return $"{X.ToFormatted()} {Y.ToFormatted()}";
        }
    }
}
