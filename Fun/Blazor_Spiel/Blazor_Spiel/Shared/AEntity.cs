﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blazor_Spiel.Shared
{
    public abstract class AEntity : APhysicsObject
    {
        public AEntity(Coordinates coords, Func<APhysicsObject, int, bool> movementValidation = null, double speedX = 0, double speedY = 0) : base(coords, movementValidation, speedX, speedY)
        {

        }

        public abstract string ToPath();
    }
}
