﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blazor_Spiel.Shared
{
    public enum EGameResult
    {
        Player1Won,
        Player2Won,
        Draw
    }
}
