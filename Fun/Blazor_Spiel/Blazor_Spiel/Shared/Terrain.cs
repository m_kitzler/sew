﻿using System;
using System.Linq;
using System.Text;

namespace Blazor_Spiel.Shared
{
    public class Terrain
    {
        public double Width { get; private set; }
        public double Height { get; private set; }
        private TerrainSegment[] segments;
        public Coordinates Spawn1 { get; private set; }
        public Coordinates Spawn2 { get; private set; }

        public static Terrain RandomTerrain(double width, double height)
        {
            if (width < 70)
                throw new Exception("Spielfeldweite muss mindestens 70 sein.");

            int segCount = (int)((width - 20) / 50);
            segCount *= 2;  //weil zwei Segmente pro Hügel

            Terrain ret = new Terrain(width, height, 8/*segCount + 2*/);

            Coordinates lastCoords = new Coordinates(20, height - 10);
            ret.segments[0] = TerrainSegment.Line(new Coordinates(0, height - 10), lastCoords);  //Spawn 1
            Random r = new Random();
            // Temp
            for (int i = 1; i < 7; i++)
            {
                Coordinates startCoords = lastCoords;
                Coordinates startMod = new Coordinates(startCoords.X + r.Next(10, 20), startCoords.Y);
                lastCoords = (i == 6) ? new Coordinates(width - 20, height - 10) : new Coordinates(20 + 35 * i, (i % 2 != 0)
                    ? (i == 1 || i == 5) ? r.Next(60, 80) : r.Next(40, 70)
                    : r.Next((int)(height / 2), (int)(height - 5)));
                Coordinates endMod = new Coordinates(lastCoords.X - r.Next(10, 20), lastCoords.Y);
                ret.segments[i] = TerrainSegment.Bezier(startCoords, startMod, endMod, lastCoords);
            }
            // Braucht nu oagn Algorithmus
            /*
            Coordinates lastCoords = new Coordinates(20, height - 10);
            for (int i = 1; i < 7; i++)
            {
                Coordinates startCoords = lastCoords;
                lastCoords = new Coordinates(r.Next(20, 40), r.Next()); //x: 30 +- 10
                //ret.segments[i] = (i % 2 != 0)
                //    ? TerrainSegment.Bezier(new Coordinates())
                //    : TerrainSegment.Bezier();
            }*/
            ret.segments[ret.segments.Length - 1] = TerrainSegment.Line(lastCoords, new Coordinates(width, height - 10));  //Spawn 2

            ret.Spawn1 = new Coordinates(10, height - 10);
            ret.Spawn2 = new Coordinates(width - 10, height - 10);

            return ret;
        }

        public Terrain(double width, double height, int segmentCount)
        {
            Width = width;
            Height = height;
            segments = new TerrainSegment[segmentCount];
        }

        public double this[double x]
        {
            get
            {
                foreach (TerrainSegment ts in segments)
                {
                    if (ts.Start <= x && ts.End >= x)
                        return ts[x];
                }
                return -1;
            }
        }

        public string ToPath(bool truePath)
        {
            //TODO: Mask (https://developer.mozilla.org/en-US/docs/Web/SVG/Element/mask)
            string pathD = truePath
                ? GetCollPath(5)
                : $"{segments[0].ToPathWithStart()} {segments.Skip(1).Aggregate("", (text, seg) => text + " " + seg.ToPath())} L {Width} {Height} L 0 {Height} z";
            return $"<path fill=\"green\" mask=\"url(#terrainMask)\" d=\"{pathD}\"/>";
        }

        string GetCollPath(double resolution)
        {
            StringBuilder sb = new StringBuilder("M 0 ");
            sb.Append(this[0].ToFormatted());
            for (double x = resolution; x <= Width; x += resolution)
            {
                sb.Append(" L ");
                sb.Append(x.ToFormatted());
                sb.Append(" ");
                sb.Append(this[x].ToFormatted());
            }
            sb.Append($"L {Width.ToFormatted()} {Height.ToFormatted()} L 0 {Height.ToFormatted()} z");
            return sb.ToString();
        }
    }
}
