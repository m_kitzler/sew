﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blazor_Spiel.Shared
{
    class Explosion : AEntity
    {
        private int frameCount = 0;
        private const int animationLength = 30;
        public double Radius { get; private set; }
        public double Damage { get; private set; }
        public event EventHandler OnEnd;
        public Explosion(Coordinates coords, double radius, double damage) : base(coords)
        {
            Radius = radius;
            Damage = damage;
        }

        public override void Tick(int tickCount)
        {
            if (frameCount < animationLength)
                frameCount++;
            else
                OnEnd?.Invoke(this, null);
        }

        public override string ToPath()
        {
            return ((frameCount <= 120)
                ? $"<circle fill=\"red\" cx=\"{Coords.X.ToFormatted()}\" cy=\"{Coords.Y.ToFormatted()}\" r=\"{(Radius * (1 - (Math.Abs((double)frameCount - (animationLength / 2)) / (animationLength / 2)))).ToFormatted()}\"/>"
                : "") +
                ((frameCount <= 60)
                ? $"<circle fill=\"yellow\" cx=\"{Coords.X.ToFormatted()}\" cy=\"{Coords.Y.ToFormatted()}\" r=\"{(Radius * (1 - (Math.Abs((double)frameCount - (animationLength / 4)) / (animationLength / 4)))).ToFormatted()}\"/>"
                : "");
        }
    }
}
