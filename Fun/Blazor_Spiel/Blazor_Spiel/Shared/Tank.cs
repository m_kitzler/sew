﻿using System;

namespace Blazor_Spiel.Shared
{
    public class Tank : APhysicsObject
    {
        private const double healthbarWidth = 6;
        private const double healthbarHeight = 0.5;
        private int health = 100;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                if (health <= 0)
                {
                    health = 0;
                    OnDeath?.Invoke(this, null);
                }
                else
                    health = value;
            }
        }
        private int angle;
        public int Angle {
            get
            {
                return angle;
            }
            set
            {
                if (value < 0)
                    angle = 0;
                else if (value > 180)
                    angle = 180;
                else
                    angle = value;
            }
        }
        private const double shotPower = 8;
        private int powerPercent = 50;
        public int PowerPercent
        {
            get
            {
                return powerPercent;
            }
            set
            {
                if (value < 0)
                    powerPercent = 0;
                else if (value > 100)
                    powerPercent = 100;
                else
                    powerPercent = value;
            }
        }
        public Func<APhysicsObject, int, bool> ShellMovementValidation { get; set; }
        public event EventHandler<Shell> OnFire;
        public event EventHandler OnDeath;


        public Tank(Coordinates coords, int angle, Func<APhysicsObject, int, bool> movementValidation, Func<APhysicsObject, int, bool> shellMovementValidation) : base(coords, movementValidation)
        {
            Angle = angle;
            ShellMovementValidation = shellMovementValidation;
        }

        public void Move(Direction d)
        {
            switch (d)
            {
                case Direction.Left:
                    SpeedX = -0.5;
                    break;
                case Direction.Right:
                    SpeedX = 0.5;
                    break;
                default:
                    break;
            }
        }

        public void Aim(int amount)
        {
            Angle += amount;
        }

        public void ChangePower(int amount)
        {
            PowerPercent += amount;
        }

        public void Fire()
        {
            OnFire?.Invoke(this, new Shell(new Coordinates(Coords.X, Coords.Y - 2), ShellMovementValidation, Angle.Cos() * shotPower * (PowerPercent / 100.0), Angle.Sin() * -shotPower * (PowerPercent / 100.0)));
        }


        public string ToPath()
        {
            return $"<path stroke=\"black\" stroke-width=\"0.3\" fill=\"grey\" d=\"M {Coords} m -1.6 -1 q 0 1, 1 1 l 1.2 0 q 1 0, 1 -1\" />" +
                $"<path d=\"M {Coords} m -2 -0.6 c 0 -1.2, 0 -1.2, 1 -1.2 c 0 -0.7, 0 -0.7, 1 -0.7 s 1 0, 1 0.7 c 1 0, 1 0, 1 1.2 z\" />" +
                $"<path stroke=\"black\" stroke-width=\"0.3\"  d=\"M {Coords} m 0 -2 l {(Angle.Cos() * 2.5).ToFormatted()} {(Angle.Sin() * -2.5).ToFormatted()}\" />" +
                $"<path stroke=\"green\" stroke-width=\"{healthbarHeight.ToFormatted()}\" d=\"M {Coords} m -{(healthbarWidth / 2).ToFormatted()} -4  l {(healthbarWidth * (Health / 100.0)).ToFormatted()} 0\" />" +
                $"<path stroke=\"red\" stroke-width=\"{healthbarHeight.ToFormatted()}\" d=\"M {Coords} m -{(healthbarWidth / 2).ToFormatted()} -4 m {(healthbarWidth * (Health / 100.0)).ToFormatted()} 0 l {(healthbarWidth * (1 - (Health / 100.0))).ToFormatted()} 0\" />";
        }

        public override string ToString()
        {
            return $"(Health: {Health}, Angle: {Angle}, Power: {PowerPercent}%)";
        }
    }
}
