﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace Blazor_Spiel.Shared
{
    public class Controller
    {
        public int TickCount
        {
            get
            {
                return tickCount;
            }
            private set
            {
                if (tickCount + value > 60)
                {
                    tickCount = 0 + value - 1;
                }
                else
                    tickCount += value;
            }
        }
        public bool GameRunning
        {
            get
            {
                return t.Enabled;
            }
        }
        public double Width { get; private set; }
        public double Height { get; private set; }
        public Terrain Field { get; private set; }
        public Tank Player1 { get; private set; }
        public Tank Player2 { get; private set; }
        public Tank CurrentPlayer { get; private set; }
        public List<AEntity> Entities { get; private set; } = new List<AEntity>();
        public event EventHandler UIUpdate;
        public event EventHandler<EGameResult> OnGameEnd;

        private const int fps = 60;
        private Timer t = new Timer(1000 / 60);
        private int tickCount = 0;
        private Random r = new Random();

        public Controller(double fieldWidth, double fieldHeight)
        {
            t.AutoReset = true;
            t.Elapsed += Tick;

            Width = fieldWidth;
            Height = fieldHeight;
            Field = Terrain.RandomTerrain(Width, Height);
            Player1 = new Tank(Field.Spawn1, 0, ValidateTankMovement, ValidateMovement);
            Player2 = new Tank(Field.Spawn2, 180, ValidateTankMovement, ValidateMovement);
            Player1.OnFire += ShellFired;
            Player2.OnFire += ShellFired;
            Player1.OnDeath += (s, e) => {
                OnGameEnd?.Invoke(this, EGameResult.Player2Won);
                Pause();
            };
            Player2.OnDeath += (s, e) => {
                OnGameEnd?.Invoke(this, EGameResult.Player1Won);
                Pause();
            };

            CurrentPlayer = (r.Next(1, 3) == 1) ? Player1 : Player2;
        }

        public void Start()
        {
            t.Start();
        }

        public void Pause()
        {
            t.Stop();
        }

        private Coordinates MoveTank(Tank t, Axis a, double amount)
        {
            switch (a)
            {
                case Axis.Y:
                    if (amount > 0 && t.Coords.Y + amount < Field[t.Coords.X])
                    {
                        t.Coords.Y += amount;
                    }
                    break;
                case Axis.X:
                    if (t.Coords.Y - Field[t.Coords.X + amount] < 0.5)
                    {
                        t.Coords.X += amount;
                        double temp = Field[t.Coords.X];
                        if (t.Coords.Y > temp)  //Höhe setzten beim Bergauffahren
                            t.Coords.Y = temp;
                    }
                    break;
            }
            return t.Coords;
        }

        private bool ValidateMovement(APhysicsObject po, int step)
        {
            return po.Coords.Y + po.SpeedY / step < Field[po.Coords.X + po.SpeedX / step];
        }

        private bool ValidateTankMovement(APhysicsObject po, int step)
        {
            if (po.SpeedX != 0)
            {
                double slopeHeight = po.Coords.Y - Field[po.Coords.X + po.SpeedX];
                if (slopeHeight <= 1.0)
                {
                    po.Coords.X += po.SpeedX;
                    if (slopeHeight > 0)
                        po.Coords.Y = Field[po.Coords.X];
                }
                po.SpeedX = 0;
            }

            if (po.Coords.Y + po.SpeedY / step < Field[po.Coords.X])
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private void ShellFired(object sender, Shell shell)
        {
            shell.OnGrounded += (sender1, e) =>
            {
                Shell s = sender1 as Shell;
                Explosion exp = new Explosion(s.Coords, 6, 50);
                Entities.Remove(s);
                exp.OnEnd += (sender2, e1) =>
                {
                    Entities.Remove(sender2 as Explosion);
                };
                Entities.Add(exp);

                // Damage: 50 mit linearen Abfall je nach Entfernung
                Console.WriteLine(Player1.Coords.DistanceTo(exp.Coords));
                foreach (Tank t in new Tank[]{Player1, Player2})
                {
                    double dist = t.Coords.DistanceTo(exp.Coords);
                    if (dist < exp.Radius)
                        t.Health -= (int)Math.Ceiling(exp.Damage * ( 1 - (dist / exp.Radius) ));
                }
            };
            Entities.Add(shell);

            CurrentPlayer = (CurrentPlayer == Player1) ? Player2 : Player1;
        }

        private void Tick(object sender, ElapsedEventArgs e)
        {
            Player1.Tick(TickCount);
            Player2.Tick(TickCount);
            Entities.ForEach(s => s.Tick(TickCount));

            UIUpdate?.Invoke(this, null);

            TickCount++;
        }
    }
}
