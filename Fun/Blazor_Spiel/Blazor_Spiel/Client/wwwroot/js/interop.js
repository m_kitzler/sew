﻿function focusElement(element) {
    if (element instanceof HTMLElement) {
        setTimeout(function () { element.focus() }, 1);
    }
}

document.addEventListener('keydown', function (event) {
    console.log(event);
    DotNet.invokeMethodAsync("Blazor_Spiel", 'KeyDownAsync', event);
}, true);