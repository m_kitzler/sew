﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InvadeR
{
    class Player
    {
        public Semaphore consoleSem;
        int pos = 115;
        public Player(Semaphore consoleSem = null)
        {
            this.consoleSem = consoleSem;
            Print();
        }
        void Print()
        {
            consoleSem?.WaitOne();
            Console.SetCursorPosition(pos+2, 66);
            Console.Write("█");
            Console.SetCursorPosition(pos+1, 67);
            Console.Write("███");
            Console.SetCursorPosition(pos, 68);
            Console.Write("█████");
            consoleSem?.Release();
        }
        public void Move(ConsoleKeyInfo input, List<Projectile> projectiles)
        {
            if (input.Key == ConsoleKey.A && pos > 0)
            {
                pos--;
                consoleSem?.WaitOne();
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(pos + 3, 66);
                Console.Write("█");
                Console.SetCursorPosition(pos + 4, 67);
                Console.Write("█");
                Console.SetCursorPosition(pos + 5, 68);
                Console.Write("█");
                Console.ForegroundColor = ConsoleColor.White;
                consoleSem?.Release();
            }
            if (input.Key == ConsoleKey.D && pos < 232)
            {
                pos++;
                consoleSem?.WaitOne();
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(pos + 1, 66);
                Console.Write("█");
                Console.SetCursorPosition(pos, 67);
                Console.Write("█");
                Console.SetCursorPosition(pos - 1, 68);
                Console.Write("█");
                Console.ForegroundColor = ConsoleColor.White;
                consoleSem?.Release();
            }
            if (input.Key == ConsoleKey.Spacebar)
            {
                projectiles.Add(new Projectile('|', "up", pos + 2, 65));
            }
            Print();
        }
    }
}
