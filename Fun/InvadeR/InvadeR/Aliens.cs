﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InvadeR
{
    class Aliens //Feld: x0 y6 bis x236 y68
    {
        static int moveCounter = 0;
        static int[,] enemys = new int[116,3]; //X, Y, beenShot 1 Reihe: 58 hälfte 29
        public static void Reset()
        {
            for (int i = 0; i < 116; i++)
            {
                enemys[i, 2] = 0;
                if (i < 58)
                {
                    enemys[i, 1] = 7;
                    enemys[i, 0] = (i < 29) ? ((i + 1) * 4) - 2 : ((i + 1) * 4) - 1;
                }
                else
                {
                    enemys[i, 1] = 9;
                    enemys[i, 0] = enemys[i - 58, 0];
                }
            }
        }
        public static void Print(Semaphore consoleSem)
        {
            for (int i = 0; i < 116; i++)
            {
                consoleSem.WaitOne();
                Console.SetCursorPosition(enemys[i, 0], enemys[i, 1]);
                Console.Write("███");
                consoleSem.Release();
            }
        }
        public static bool Scan(int X, int Y)
        {
            for (int i = 0; i < 116; i++)
            {
                if ((X == enemys[i,0] || X == enemys[i, 0] + 1 || X == enemys[i, 0] + 2) && Y == enemys[i,1] && enemys[i,2] == 0)
                {
                    enemys[i, 2] = 1;
                    return true;
                }
            }
            return false;
        }
        public static void Move()
        {
            moveCounter++;
            for (int i = 0; i < 116; i++)
            {
                switch (moveCounter % 4)
                {
                    case 0:
                        enemys[i, 0]++;
                        break;
                    case 1:
                    case 3:
                        enemys[i, 1]++;
                        break;
                    case 2:
                        enemys[i, 0]--;
                        break;
                }
            }
        }
    }
}
