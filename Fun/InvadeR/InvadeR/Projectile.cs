﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InvadeR
{
    class Projectile
    {
        char ammo;
        string direction;
        int[] coordinates = new int[2];
        public Projectile (char ammochar, string facing, int X, int Y)
        {
            ammo = ammochar;
            direction = facing;
            coordinates[0] = X;
            coordinates[1] = Y;
        }
        public bool Tick()
        {
            if (direction == "up")
            {
                coordinates[1]--;
                if (coordinates[1] < 65)
                {
                    Console.SetCursorPosition(coordinates[0], coordinates[1] + 1);
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(ammo);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                if (coordinates[1] > 6)
                {
                    return true;
                }
                else if (Aliens.Scan(coordinates[0], coordinates[1])) //enemy hit scan
                {
                    return false;
                }
                else
                    return false;
            }
            if (direction == "down")
            {
                coordinates[1]++;
                if (!false)
                {
                    Console.SetCursorPosition(coordinates[0], coordinates[1] - 1);
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(ammo);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                if (coordinates[1] < 68)
                {
                    return true;
                }
                /*else if () //player hit scan
                {

                }*/
                else
                    return false;
            }
            return false;
        }
        public void Print(Semaphore consoleSem)
        {
            consoleSem.WaitOne();
            Console.SetCursorPosition(coordinates[0], coordinates[1]);
            Console.Write(ammo);
            consoleSem.Release();
        }
    }
}
