﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace InvadeR
{
    class Program
    {
        [DllImport("kernel32.dll", ExactSpelling = true)]

        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;

        static List<Projectile> projectiles = new List<Projectile>();
        static Semaphore consoleSem;

        static void Main(string[] args)
        {
            #region StartUp
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            ShowWindow(ThisConsole, MAXIMIZE);
            Console.CursorVisible = false;
            #endregion

            consoleSem = new Semaphore(0, 1);

            Console.SetCursorPosition(236, 68);
            Console.Write("█");
            Console.SetCursorPosition(0, 6);
            Console.Write("█");
            Aliens.Reset();

            var fTask = Task.Run(() => Exec());
            int counter = 0;
            while (true)
            {
                Thread.Sleep(50);
                for (int i = 0; i < projectiles.Count; i++)
                {
                    if (!projectiles[i].Tick())
                    {
                        projectiles.RemoveAt(i);
                    }
                    else
                        projectiles[i].Print(consoleSem);
                }
                counter++;
                if (counter % 20 == 0)
                {
                    counter = 0;
                    Aliens.Move();
                    Aliens.Print(consoleSem);
                }
            }
        }
        static void Exec()
        {
            Player p = new Player(consoleSem);
            while (true)
            {
                p.Move(Console.ReadKey(true),projectiles);
                Thread.Sleep(50);
            }
        }
    }
}
