﻿using System;
using TetrisDLL;

namespace TetrisProgram
{
    class Program
    {
        #region Console
        (int XFieldConsoleLocation,
        int YFieldConsoleLocation,
        int XConsoleDrawMultiplier,
        int YConsoleDrawMultiplier,
        int XNBConsoleLocation,
        int YNBConsoleLocation,
        int XHBConsoleLocation,
        int YHBConsoleLocation) ConsoleVars = (0, 0, 2, 1, -1, -1, -1, -1);
        #endregion
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Console.SetCursorPosition(11, 41);
            Console.Write("X");
            Tetris t = new Tetris(10, 20);
            t.Start();

            bool exit = false;
            while (!exit)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.S:
                    case ConsoleKey.DownArrow:
                        t.Move(Direction.Down, true);
                        break;
                    case ConsoleKey.A:
                    case ConsoleKey.LeftArrow:
                        t.Move(Direction.Left);
                        break;
                    case ConsoleKey.D:
                    case ConsoleKey.RightArrow:
                        t.Move(Direction.Right);
                        break;
                    case ConsoleKey.Q:
                        t.RotateCounterClockwise();
                        break;
                    case ConsoleKey.E:
                        t.RotateClockwise();
                        break;
                    case ConsoleKey.W:
                    case ConsoleKey.UpArrow:
                        if (XHBConsoleLocation >= 0 && YHBConsoleLocation >= 0)
                        {
                            BlockType hlp = brickHold.GetBlock();
                            brickHold.SetBlock(brick.GetBlock());
                            brick.SetBlock(hlp);
                        }
                        break;
                    case ConsoleKey.Escape:
                        exit = true;
                        t.Pause();
                        break;
                }
            }
        }

        private void Print()
        {
            public void Print(int ScalingX, int ScalingY)
            {
                foreach (Block b in blocks)
                {
                    if (b != null && b.Y > 0)   //Wenn Block gesetzt und nicht null
                    {
                        Console.ForegroundColor = b.Type;
                        for (int y = 0; y < ScalingY; y++)
                        {
                            for (int x = 0; x < ScalingX; x++)
                            {
                                Console.SetCursorPosition((b.X * ScalingX) + x, (b.Y * ScalingY) + y);
                                Console.Write("█");
                            }
                        }
                    }
                }
            }
            Console.Clear();
            for (int y = 0; y < field.GetLength(1); y++)
            {
                for (int x = 0; x < field.GetLength(0); x++)
                {
                    Console.ForegroundColor = field[x, y];
                    for (int yScale = 0; yScale < YConsoleDrawMultiplier; yScale++)
                    {
                        for (int xScale = 0; xScale < XConsoleDrawMultiplier; xScale++)
                        {
                            Console.SetCursorPosition(((XFieldConsoleLocation + x) * XConsoleDrawMultiplier) + xScale, ((YFieldConsoleLocation + y) * YConsoleDrawMultiplier) + yScale);
                            Console.Write("█");
                        }
                    }
                }
            }
            brick.Print(XConsoleDrawMultiplier, YConsoleDrawMultiplier);
            if (XHBConsoleLocation >= 0 && YHBConsoleLocation >= 0)
            {
                brickHold.Print(XConsoleDrawMultiplier, YConsoleDrawMultiplier);
            }
            if (XNBConsoleLocation >= 0 && YNBConsoleLocation >= 0)
            {
                brickNext.Print(XConsoleDrawMultiplier, YConsoleDrawMultiplier);
            }
        }
    }
}
