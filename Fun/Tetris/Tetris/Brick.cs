﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Brick
    {
        private ConsoleColor[,] block = new ConsoleColor[5, 5];
        private bool blockRotatable = true;
        private Block currBlockType;
        public void NewRandom()
        {
            currBlockType = Block.Blue + (new Random()).Next(0, 7);
            block = GetBlock(currBlockType);
        }
        private ConsoleColor[,] GetBlock(Block b)
        {
            blockRotatable = !(b == Block.Yellow);
            ConsoleColor[,] next = new ConsoleColor[5, 5];
            switch (b)
            {
                case Block.Blue:
                    next[1, 3] = ConsoleColor.Blue;
                    next[2, 1] = ConsoleColor.Blue;
                    next[2, 2] = ConsoleColor.Blue;
                    next[2, 3] = ConsoleColor.Blue;
                    break;
                case Block.Cyan:
                    next[2, 1] = ConsoleColor.Cyan;
                    next[2, 2] = ConsoleColor.Cyan;
                    next[2, 3] = ConsoleColor.Cyan;
                    next[2, 4] = ConsoleColor.Cyan;
                    break;
                case Block.Green:
                    next[1, 2] = ConsoleColor.Green;
                    next[1, 3] = ConsoleColor.Green;
                    next[2, 1] = ConsoleColor.Green;
                    next[2, 2] = ConsoleColor.Green;
                    break;
                case Block.Magenta:
                    next[1, 2] = ConsoleColor.Magenta;
                    next[2, 1] = ConsoleColor.Magenta;
                    next[2, 2] = ConsoleColor.Magenta;
                    next[2, 3] = ConsoleColor.Magenta;
                    break;
                case Block.Orange:
                    next[1, 1] = ConsoleColor.DarkYellow;
                    next[2, 1] = ConsoleColor.DarkYellow;
                    next[2, 2] = ConsoleColor.DarkYellow;
                    next[2, 3] = ConsoleColor.DarkYellow;
                    break;
                case Block.Red:
                    next[1, 1] = ConsoleColor.Red;
                    next[1, 2] = ConsoleColor.Red;
                    next[2, 2] = ConsoleColor.Red;
                    next[2, 3] = ConsoleColor.Red;
                    break;
                case Block.Yellow:
                    next[1, 1] = ConsoleColor.Yellow;
                    next[1, 2] = ConsoleColor.Yellow;
                    next[2, 1] = ConsoleColor.Yellow;
                    next[2, 2] = ConsoleColor.Yellow;
                    break;
            }
            return next;
        }
        public void RotateClockwise()
        {
            //block[0, 0] Zwischenspeicher
            #region äußerste Ring
            block[0, 0] = block[0, 2];
            block[0, 2] = block[2, 0];
            block[2, 0] = block[4, 2];
            block[4, 2] = block[2, 4];
            block[2, 4] = block[0, 0];
            #endregion
            #region innere Ring
            block[0, 0] = block[1, 1];
            block[1, 1] = block[2, 1];
            block[2, 1] = block[3, 1];
            block[3, 1] = block[3, 2];
            block[3, 2] = block[3, 3];
            block[3, 3] = block[2, 3];
            block[2, 3] = block[1, 3];
            block[1, 3] = block[1, 2];
            block[1, 2] = block[0, 0];
            #endregion
        }
        private void Move(Direction d)
        {

        }
    }
}
