﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Tetris
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.0 Version
            Console.CursorVisible = false;
            bool gameRunning = false;
            bool exit = false;
            string name = ""; //Baustein:▐, █, ▌ Blinkanimation: ░
            while (!exit)
            {
                if (!gameRunning)
                {
                    #region Menü
                    display.tetrislogo();
                    Console.WriteLine("1) Start\n2) Name ändern\n3) Beenden");
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.D1:
                            game = true;
                            menu = false;
                            break;
                        case ConsoleKey.D2:
                            Console.Clear();
                            display.tetrislogo();
                            Console.WriteLine("Geben sie einen neuen Namen ein:");
                            Console.CursorVisible = true;
                            name = Console.ReadLine();
                            Console.CursorVisible = false;
                            Console.Clear();
                            display.tetrislogo();
                            Console.WriteLine("Name wurde erfolgreich geändert!");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                        case ConsoleKey.D3:
                            menu = false;
                            exit = true;
                            break;
                    }
                    Console.Clear();
                    #endregion
                }
                else
                {
                    int[,] spielfeld = new int[10, 20];
                    int[,] block = new int[4, 3]; //4 für jeden Block, 3 für x, y und Blockart
                                                  //1: Hellblau Länglich  2: Dunkelblau L     3: Orange L     4: Gelb Block   5: Grün Irgendw.    6: Lila T       7: Rot Irgendw.
                                                  //   ██ ██ ██ ██                ██            ██                  ██ ██              ██ ██             ██             ██ ██
                                                  //                        ██ ██ ██            ██ ██ ██            ██ ██           ██ ██             ██ ██ ██             ██ ██

                    System.Timers.Timer t = new System.Timers.Timer(1000);
                    t.Elapsed += (t1, t2) => TimerInterval(t1, t2, ref block, spielfeld);
                    t.AutoReset = true;
                    t.Start();
                    display.field(spielfeld);
                    display.block(block);
                    while (gameRunning)
                    {
                        switch (Console.ReadKey(true).Key)
                        {
                            case ConsoleKey.UpArrow:

                                break;
                            case ConsoleKey.DownArrow:

                                break;
                            case ConsoleKey.LeftArrow:
                                if (block[0, 0] > 0 || block[1, 0] > 0 || block[2, 0] > 0 || block[3, 0] > 0)
                                {
                                    block[0, 0]--;
                                    block[1, 0]--;
                                    block[2, 0]--;
                                    block[3, 0]--;
                                    display.block(block, Direction.Left);
                                }
                                break;
                            case ConsoleKey.RightArrow:
                                if ((block[0, 0] < 9 || block[1, 0] < 9 || block[2, 0] < 9 || block[3, 0] < 9) && )
                                {
                                    block[0, 0]++;
                                    block[1, 0]++;
                                    block[2, 0]++;
                                    block[3, 0]++;
                                    display.block(block, Direction.Right);
                                }
                                break;
                        }
                        if (blockactive == false)
                        {
                            switch (new Random().Next(0, 7))
                            {
                                #region Blockarten
                                case 0:
                                    block[0, 0] = 3; block[0, 1] = 0;
                                    block[1, 0] = 2; block[1, 1] = 0;
                                    block[2, 0] = 4; block[2, 1] = 0;
                                    block[3, 0] = 5; block[3, 1] = 0;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 0;
                                    }
                                    break;
                                case 1:
                                    block[0, 0] = 2; block[0, 1] = 0;
                                    block[1, 0] = 2; block[1, 1] = 1;
                                    block[2, 0] = 3; block[2, 1] = 1;
                                    block[3, 0] = 4; block[3, 1] = 1;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 1;
                                    }
                                    break;
                                case 2:
                                    block[0, 0] = 4; block[0, 1] = 0;
                                    block[1, 0] = 4; block[1, 1] = 1;
                                    block[2, 0] = 3; block[2, 1] = 1;
                                    block[3, 0] = 2; block[3, 1] = 1;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 2;
                                    }
                                    break;
                                case 3:
                                    block[0, 0] = 3; block[0, 1] = 0;
                                    block[1, 0] = 4; block[1, 1] = 0;
                                    block[2, 0] = 4; block[2, 1] = 1;
                                    block[3, 0] = 3; block[3, 1] = 1;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 3;
                                    }
                                    break;
                                case 4:
                                    block[0, 0] = 3; block[0, 1] = 0;
                                    block[1, 0] = 2; block[1, 1] = 0;
                                    block[2, 0] = 4; block[2, 1] = 0;
                                    block[3, 0] = 5; block[3, 1] = 0;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 4;
                                    }
                                    break;
                                case 5:
                                    block[0, 0] = 3; block[0, 1] = 0;
                                    block[1, 0] = 2; block[1, 1] = 0;
                                    block[2, 0] = 4; block[2, 1] = 0;
                                    block[3, 0] = 5; block[3, 1] = 0;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 5;
                                    }
                                    break;
                                case 6:
                                    block[0, 0] = 3; block[0, 1] = 0;
                                    block[1, 0] = 2; block[1, 1] = 0;
                                    block[2, 0] = 4; block[2, 1] = 0;
                                    block[3, 0] = 5; block[3, 1] = 0;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        block[i, 2] = 6;
                                    }
                                    break;
                                    #endregion
                            }
                            blockactive = true;
                        }
                    }
                    t.Stop();
                }
            }
        }
        static void TimerInterval(Object t1, EventArgs t2, ref int[,] block, int[,] spielfeld)
        {
            block[0, 1]++;
            block[1, 1]++;
            block[2, 1]++;
            block[3, 1]++;
            display.block(block, Direction.Down);
        }
    }
}