﻿using System;
using System.Collections;

namespace TetrisDLL
{
    class Brick : IEnumerable, IEnumerator
    {
        public static Block[] GetBlock(BlockType type)
        {
            Block[] next = new Block[4];
            switch (type)
            {
                case BlockType.Blue:
                    next[0] = new Block(BlockType.Blue, -1, 1);
                    next[1] = new Block(BlockType.Blue, -1, 0);
                    next[2] = new Block(BlockType.Blue, 0, 0);
                    next[3] = new Block(BlockType.Blue, 1, 0);
                    break;
                case BlockType.Cyan:
                    next[0] = new Block(BlockType.Cyan, -1, 0);
                    next[1] = new Block(BlockType.Cyan, 0, 0);
                    next[2] = new Block(BlockType.Cyan, 1, 0);
                    next[3] = new Block(BlockType.Cyan, 2, 0);
                    break;
                case BlockType.Green:
                    next[0] = new Block(BlockType.Green, 0, 1);
                    next[1] = new Block(BlockType.Green, 1, 1);
                    next[2] = new Block(BlockType.Green, -1, 0);
                    next[3] = new Block(BlockType.Green, 0, 0);
                    break;
                case BlockType.Magenta:
                    next[0] = new Block(BlockType.Magenta, 0, 1);
                    next[1] = new Block(BlockType.Magenta, -1, 0);
                    next[2] = new Block(BlockType.Magenta, 0, 0);
                    next[3] = new Block(BlockType.Magenta, 1, 0);
                    break;
                case BlockType.Orange:
                    next[0] = new Block(BlockType.Orange, -1, 1);
                    next[1] = new Block(BlockType.Orange, -1, 0);
                    next[2] = new Block(BlockType.Orange, 0, 0);
                    next[3] = new Block(BlockType.Orange, 1, 0);
                    break;
                case BlockType.Red:
                    next[0] = new Block(BlockType.Red, -1, 1);
                    next[1] = new Block(BlockType.Red, 0, 1);
                    next[2] = new Block(BlockType.Red, 0, 0);
                    next[3] = new Block(BlockType.Red, 1, 0);
                    break;
                case BlockType.Yellow:
                    next[0] = new Block(BlockType.Yellow, -1, 1);
                    next[1] = new Block(BlockType.Yellow, 0, 1);
                    next[2] = new Block(BlockType.Yellow, -1, 0);
                    next[3] = new Block(BlockType.Yellow, 0, 0);
                    break;
            }
            return next;
        }


        private Block[] blocks { get; set; } = new Block[4];
        private bool brickRotatable = true;
        public BlockType Type { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        int position = -1;
        public object Current => blocks[position];

        public Brick(BlockType type, int x, int y = 0)
        {
            X = x;
            Y = y;
            Type = type;
            brickRotatable = !(Type == BlockType.Yellow);
            blocks = GetBlock(Type);
        }

        public Block this[int index]
        {
            get { return blocks[index]; }
        }

        /// <summary>
        /// Prüft, ob Drehung möglich ist und dreht Stein.
        /// </summary>
        /// <remarks>
        /// Gibt true zurück, wenn erfolgreich.
        /// </remarks>
        /// <param name="field">Feld auf dem sich der Stein befindet</param>
        /// <param name="counterClockwise">Gegen den Uhrzeigersinn</param>
        /// <returns></returns>
        public bool Rotate(Block[,] field, bool counterClockwise)
        {
            if (brickRotatable)
            {
                bool errorUp = false, errorDown = false, errorLeft = false, errorRight = false;
                (int X, int Y)[] tempRelPos = new (int, int)[4];
                int tempX = X, tempY = Y;

                // Prüft, ob und wo Fehler ist
                for (int i = 0; i < 4; i++)
                {
                    tempRelPos[i] = blocks[i].Rotate(counterClockwise);
                    if (tempRelPos[i].X + tempX < 0 || tempRelPos[i].X + tempX >= field.GetLength(0) || tempRelPos[i].Y + tempY < 0 || tempRelPos[i].Y + tempY >= field.GetLength(1) || field[tempRelPos[i].X + tempX, tempRelPos[i].Y + tempY] != null)
                    {
                        if (tempRelPos[i].Y < 0)
                            errorUp = true;
                        if (tempRelPos[i].Y > 0)
                            errorDown = true;
                        if (tempRelPos[i].X < 0)
                            errorLeft = true;
                        if (tempRelPos[i].X > 0)
                            errorRight = true;
                    }
                }

                // Versucht, Stein zu verschieben
                if (errorUp ^ errorDown)
                    tempY = tempY + (errorUp ? -1 : 1);
                if (errorLeft ^ errorRight)
                    tempX = tempX + (errorRight ? -1 : 1);

                // Wenn immer noch nicht geht, abbrechen
                bool error = false;
                for (int i = 0; i < 4 && !error; i++)
                {
                    tempRelPos[i] = blocks[i].Rotate(counterClockwise);
                    if (tempRelPos[i].X + tempX < 0 || tempRelPos[i].X + tempX >= field.GetLength(0) || tempRelPos[i].Y + tempY < 0 || tempRelPos[i].Y + tempY >= field.GetLength(1) || field[tempRelPos[i].X + tempX, tempRelPos[i].Y + tempY] != null)
                    {
                        error = true;
                    }
                }

                // Setzen, wenn erfolgreich
                if (!error)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        blocks[i].relX = tempRelPos[i].X;
                        blocks[i].relY = tempRelPos[i].Y;
                    }
                    X = tempX;
                    Y = tempY;
                }
                return error;
            }
            else
                return false;
        }

        /// <summary>
        /// Bewegt den Block, wobei auf inkorrekte Bewegungen geprüft wird.
        /// hardDrop lässt den Block direkt zu Boden fallen.
        /// </summary>
        /// <remarks>
        /// Gibt true zurück, wenn erfolgreich.
        /// </remarks>
        /// <param name="d">Richtung</param>
        /// <param name="field">Feld auf dem sich der Stein befindet</param>
        /// <param name="hardDrop">Lässt Stein komplett herunterfallen</param>
        /// <returns></returns>
        public bool Move(Direction d, Block[,] field, bool hardDrop = false)
        {
            bool error;
            switch (d)
            {
                case Direction.Down:
                    do
                    {
                        error = false;
                        foreach (Block b in blocks)
                        {
                            if (b.relY + Y + 1 >= field.GetLength(1) || field[b.relX  + X, b.relY + Y + 1] != null)
                            {
                                error = true;
                            }
                        }
                        if (!error)
                            Y++;
                    } while (hardDrop);
                    return error;
                case Direction.Left:
                    error = false;
                    foreach (Block b in blocks)
                    {
                        if (b.relX + X - 1 < 0 || field[b.relX + X - 1, b.relY + Y] != null)
                        {
                            error = true;
                        }
                    }
                    if (!error)
                        X--;
                    return error;
                case Direction.Right:
                    error = false;
                    foreach (Block b in blocks)
                    {
                        if (b.relX + X + 1 >= field.GetLength(0) || field[b.relX + X + 1, b.relY + Y] != null)
                        {
                            error = true;
                        }
                    }
                    if (!error)
                        X++;
                    return error;
                default:
                    return false;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            position++;
            return (position < blocks.Length);
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
