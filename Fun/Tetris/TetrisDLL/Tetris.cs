﻿using System;
using System.Timers;

namespace TetrisDLL
{
    public enum Direction { None, Up, Down, Left, Right }
    public enum BlockType { None, Blue, Cyan, Green, Magenta, Orange, Red, Yellow }
    public class Tetris
    {
        Random r = new Random();
        Timer t = new Timer();
        int fieldWidth, fieldHeight;
        int xStartCoordinate;

        Block[,] field;
        Brick brick = null;
        BlockType brickHold = BlockType.None, brickNext;        //TODO: hold block

        int level;
        /// <summary>
        /// Setzt die Geschwindigkeit je nach Level.
        /// </summary>
        public int Level
        {
            get
            {
                return level;
            }
            private set
            {
                level = value;
                t.Interval = 1000 - (50 * level - 1);
            }
        }
        int rowsCleared;
        bool moved;
        bool alreadySwitched;
        bool gameOver;

        public Tetris(int fieldWidth, int fieldHeight)
        {
            this.fieldWidth = fieldWidth;
            this.fieldHeight = fieldHeight;
            xStartCoordinate = (int)Math.Floor((double)(fieldWidth / 2));

            t.Elapsed += tElapsed;
            t.AutoReset = true;

            Reset();
        }

        private void tElapsed(object o, ElapsedEventArgs e) //TODO: Clearrow
        {
            if (brick == null)
            {
                brick = new Brick(brickNext, xStartCoordinate);
                foreach(Block b in brick)
                {
                    if (field[brick.X + b.relX, brick.Y + b.relY] != null)
                    {
                        gameOver = true;
                        break;
                    }
                }
                if (gameOver)
                {
                    Pause();
                }
                else
                {
                    brickNext = (BlockType)r.Next(1, 8);
                    moved = true;
                    alreadySwitched = false;
                }
            }
            else if (!moved)
            {
                foreach(Block b in brick)
                {
                    field[brick.X + b.relX, brick.Y + b.relY] = b;
                }
                brick = null;
            }
            else
                moved = brick.Move(Direction.Down, field);
        }

        public void Reset()
        {
            field = new Block[fieldWidth, fieldHeight];
            brickNext = (BlockType)r.Next(1, 8);
            
            Level = 1;
            rowsCleared = 0;
            moved = true;
            alreadySwitched = false;
            gameOver = false;
        }

        public void Start()
        {
            if (!gameOver)
            {
                t.Start();
            }
        }

        public void Pause()
        {
            t.Stop();
        }

        public bool Move(Direction d, bool harddrop = false) => brick?.Move(d, field, harddrop) ?? false;

        public bool RotateClockwise() => brick?.Rotate(field, false) ?? false;

        public bool RotateCounterClockwise() => brick?.Rotate(field, true) ?? false;
    }
}
