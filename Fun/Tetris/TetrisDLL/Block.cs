﻿namespace TetrisDLL
{
    class Block
    {
        public BlockType Type { get; private set; }
        public int relX { get; set; }
        public int relY { get; set; }
        public Block(BlockType type, int relativeX, int relativeY)
        {
            Type = type;
            relX = relativeX;
            relY = relativeY;
        }

        /// <summary>
        /// Gibt neue relative Position nach Drehung zurück.
        /// </summary>
        /// <param name="cc">Gegen den Uhrzeigersinn</param>
        /// <returns></returns>
        public (int X, int Y) Rotate(bool cc)
        {
            int newX = cc ? relY : -relY;
            int newY = cc ? -relX : relX;
            return (newX, newY);
        }
    }
}
