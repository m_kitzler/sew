﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(120, 30);
            Console.CursorVisible = false;
            ConsoleKeyInfo input;
            string Input;
            int Choice = 1;
            int highscore = 0;
            ConsoleColor colormenu = ConsoleColor.White;
            ConsoleColor colorsnake = ConsoleColor.White;
            ConsoleColor colorbg = ConsoleColor.Black;
            ConsoleColor colorpoints = ConsoleColor.White;
            string name = "Player";
            bool menu = true;
            bool control = true;
            while (menu)
            {
                while (control)
                {
                    Console.WriteLine("Snake\t\t\tHighscore: {0}\n", highscore);
                    if (Choice == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine("Spiel starten");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    if (Choice == 2)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine("Name ändern");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    if (Choice == 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine("Farben ändern");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    if (Choice == 4)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine("Beenden");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    input = Console.ReadKey(true);
                    Console.Clear();
                    Input = input.Key.ToString();
                    switch (Input)
                    {
                        case "UpArrow":
                            if (Choice == 1)
                                Choice = 4;
                            else
                                Choice--;
                            break;
                        case "DownArrow":
                            if (Choice == 4)
                                Choice = 1;
                            else
                                Choice++;
                            break;
                        case "Enter":
                            control = false;
                            break;
                    }
                }
                control = true;
                switch (Choice)
                {
                    case 1:
                        game(name, colormenu, colorsnake, colorbg, colorpoints, ref highscore);
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case 2:
                        Console.WriteLine("Gib deinen Namen ein:");
                        Console.CursorVisible = true;
                        name = Console.ReadLine();
                        Console.CursorVisible = false;
                        break;
                    case 3:
                        color(ref colormenu, ref colorsnake, ref colorbg, ref colorpoints);
                        break;
                    case 4:
                        menu = false;
                        break;
                }
                Console.Clear();
            }
        }
        static void display(int[,] snakelenght, int score, int[] point, bool pointexist, string name, ConsoleColor colormenu, ConsoleColor colorsnake, ConsoleColor colorbg, ConsoleColor colorpoints, string moving)
        {
            Console.SetCursorPosition(0, 0);
            Console.Write("Punkte: {0}\t{1}", score - 1, name);
            Console.SetCursorPosition(0, 1);
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------");
            for (int i = score; i > -1; i--)
            {
                Console.ForegroundColor = colorsnake;
                Console.SetCursorPosition(snakelenght[i, 0], snakelenght[i, 1]);
                Console.Write("█");
            }
            if (pointexist == true)
            {
                Console.ForegroundColor = colorpoints;
                Console.SetCursorPosition(point[0], point[1]);
                Console.Write("O");
            }
            if (moving != "nix")
            {
                Console.ForegroundColor = colorbg;
                Console.SetCursorPosition(snakelenght[score + 1, 0], snakelenght[score + 1, 1]);
                Console.Write("█");
            }
            Console.ForegroundColor = colormenu;
        }
        static void game(string name, ConsoleColor colormenu, ConsoleColor colorsnake, ConsoleColor colorbg, ConsoleColor colorpoints, ref int highscore)
        {
            Console.BackgroundColor = colorbg;
            Console.Clear();
            Random r = new Random();
            ConsoleKeyInfo input;
            bool gameover = false;
            bool pointexist = false;
            bool pause = true;
            string moving = "nix";
            int score = 1;
            int x = 58, y = 13;
            int[,] snakelenght = new int[1000000, 2];
            int[] point = new int[2];
            while (gameover == false)
            {
                #region Game
                display(snakelenght, score, point, pointexist, name, colormenu, colorsnake, colorbg, colorpoints, moving);
                #region Movement
                if (Console.KeyAvailable)
                {
                    Thread.Sleep(15);
                    input = Console.ReadKey(true);
                    if (input.Key == ConsoleKey.UpArrow && moving != "down")
                    {
                        moving = "up";
                    }
                    else if (input.Key == ConsoleKey.DownArrow && moving != "up")
                    {
                        moving = "down";
                    }
                    else if (input.Key == ConsoleKey.LeftArrow && moving != "right")
                    {
                        moving = "left";
                    }
                    else if (input.Key == ConsoleKey.RightArrow && moving != "left")
                    {
                        moving = "right";
                    }
                    else if (input.Key == ConsoleKey.P)
                    {
                        Console.Clear();
                        display(snakelenght, score, point, pointexist, name, colormenu, colorsnake, colorbg, colorpoints, moving);
                        while (pause)
                        {
                            Console.SetCursorPosition(114, 0);
                            Console.Write("Pause");
                            input = Console.ReadKey(true);
                            if (input.Key == ConsoleKey.P)
                                pause = false;
                            else if (input.Key == ConsoleKey.Escape)
                            {
                                pause = false;
                                gameover = true;
                            }
                        }
                        Console.Clear();
                        display(snakelenght, score, point, pointexist, name, colormenu, colorsnake, colorbg, colorpoints, moving);
                        pause = true;
                    }
                    else if (input.Key == ConsoleKey.Escape)
                    {
                        gameover = true;
                    }
                    else if (input.Key == ConsoleKey.Enter)
                    {
                        Console.SetCursorPosition(25, 0);
                        Console.CursorVisible = true;
                        string command = Console.ReadLine();
                        Console.CursorVisible = false;
                        int value;
                        string[] split = command.Split(' ');
                        command = split[0];
                        Int32.TryParse(split[1], out value);
                        if (command == "/score")
                            score = value + 1;
                        Console.Clear();
                        display(snakelenght, score, point, pointexist, name, colormenu, colorsnake, colorbg, colorpoints, moving);
                    }
                }
                else
                {
                    Thread.Sleep(50);
                }
                {
                    switch (moving)
                    {
                        case "up":
                            y--;
                            break;
                        case "down":
                            y++;
                            break;
                        case "left":
                            x--;
                            break;
                        case "right":
                            x++;
                            break;
                    }
                }
                #endregion
                #region points
                if (pointexist == false)
                {
                    point[0] = r.Next(1, 119);
                    point[1] = r.Next(3, 30);
                    pointexist = true;
                }
                if (point[0] == x && point[1] == y)
                {
                    score++;
                    pointexist = false;
                }
                #endregion
                #region Snakelenght
                snakelenght[0, 0] = x;
                snakelenght[0, 1] = y;
                for (int i = score + 1; i > 0; i--)
                {
                    snakelenght[i, 0] = snakelenght[i - 1, 0];
                    snakelenght[i, 1] = snakelenght[i - 1, 1];
                }
                #endregion
                #region End
                if (y < 2 || y > 29 || x < 0 || x > 119)
                {
                    gameover = true;
                }
                for (int i = score; i > 1; i--)
                {
                    if (snakelenght[i, 0] == x && snakelenght[i, 1] == y && score > 1)
                        gameover = true;
                }
                #endregion
                #endregion
            }
            if (score - 1 > highscore)
                highscore = score - 1;
        }
        static void color(ref ConsoleColor colormenu, ref ConsoleColor colorsnake, ref ConsoleColor colorbg, ref ConsoleColor colorpoints)
        {
            int[,] Choice = { { 1, 0, 0, 0 }, { 8, 8, 1, 8 } };  //Choice[0,0] ist Optionsauswahle, Choice[1,x] ist Farbauswahl + Werte
            bool control = true;                                       //Bsp: Choice[1,2] = 4 währe Hintergründfarboption mit Farbe Grau
            Choice[0, 0] = 1;
            ConsoleKeyInfo input;
            string Input;
            string[] Colorname = { "Schwarz", "Blau", "Cyan", "Grau", "Grün", "Magenta", "Rot", "Weiß", "Gelb" };
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            while (control)
            {
                #region Menucolor
                Console.Write("Schriftfarbe:\t\t<");
                if (Choice[0, 0] == 1)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(Colorname[Choice[1, 0] - 1]);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                    Console.Write(Colorname[Choice[1, 0] - 1]);
                Console.Write(">\n");
                #endregion
                #region Snakecolor
                Console.Write("Schlangenfarbe:\t\t<");
                if (Choice[0, 0] == 2)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(Colorname[Choice[1, 1] - 1]);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                    Console.Write(Colorname[Choice[1, 1] - 1]);
                Console.Write(">\n");
                #endregion
                #region Backgroundcolor
                Console.Write("Hintergrundfarbe:\t<");
                if (Choice[0, 0] == 3)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(Colorname[Choice[1, 2] - 1]);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                    Console.Write(Colorname[Choice[1, 2] - 1]);
                Console.Write(">\n");
                #endregion
                #region Pointcolor
                Console.Write("Punktfarbe:\t\t<");
                if (Choice[0, 0] == 4)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(Colorname[Choice[1, 3] - 1]);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                    Console.Write(Colorname[Choice[1, 3] - 1]);
                Console.Write(">\n");
                #endregion
                #region Back
                if (Choice[0, 0] == 5)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("Back");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                    Console.Write("Back");
                #endregion
                input = Console.ReadKey(true);
                Input = input.Key.ToString();
                switch (Input)
                {
                    case "UpArrow":
                        if (Choice[0, 0] == 1)
                            Choice[0, 0] = 5;
                        else
                            Choice[0, 0]--;
                        break;
                    case "DownArrow":
                        if (Choice[0, 0] == 5)
                            Choice[0, 0] = 1;
                        else
                            Choice[0, 0]++;
                        break;
                    case "LeftArrow":
                        if (Choice[1, Choice[0, 0] - 1] == 1 && Choice[0, 0] != 5)
                            Choice[1, Choice[0, 0] - 1] = 9;
                        else if (Choice[0, 0] != 5)
                            Choice[1, Choice[0, 0] - 1]--;
                        break;
                    case "RightArrow":
                        if (Choice[1, Choice[0, 0] - 1] == 9 && Choice[0, 0] != 5)
                            Choice[1, Choice[0, 0] - 1] = 1;
                        else if (Choice[0, 0] != 5)
                            Choice[1, Choice[0, 0] - 1]++;
                        break;
                    case "Enter":
                        if (Choice[0, 0] == 5)
                            control = false;
                        break;
                }
                switch (Choice[0, 0])
                {
                    case 1:
                        colorchoice(Choice[1, 0], ref colormenu);
                        break;
                    case 2:
                        colorchoice(Choice[1, 1], ref colorsnake);
                        break;
                    case 3:
                        colorchoice(Choice[1, 2], ref colorbg);
                        break;
                    case 4:
                        colorchoice(Choice[1, 3], ref colorpoints);
                        break;
                }
                Console.Clear();
            }
        }
        static void colorchoice(int getcolorid, ref ConsoleColor outcolor)
        {
            switch (getcolorid)
            {
                case 1:
                    outcolor = ConsoleColor.Black;
                    break;
                case 2:
                    outcolor = ConsoleColor.Blue;
                    break;
                case 3:
                    outcolor = ConsoleColor.Cyan;
                    break;
                case 4:
                    outcolor = ConsoleColor.Gray;
                    break;
                case 5:
                    outcolor = ConsoleColor.Green;
                    break;
                case 6:
                    outcolor = ConsoleColor.Magenta;
                    break;
                case 7:
                    outcolor = ConsoleColor.Red;
                    break;
                case 8:
                    outcolor = ConsoleColor.White;
                    break;
                case 9:
                    outcolor = ConsoleColor.Yellow;
                    break;
            }
        }
    }
}
