﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        static double currentopacity = 1;
        //static List<string> users = new List<string>();
        static UdpClient Receiver = new UdpClient(8888);
        public Form1()
        {
            InitializeComponent();
            Receiver.BeginReceive(new AsyncCallback(rec), null);
            AcceptButton = buttonSend;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (writeBox.Text != "")
            {
                string message = "[" + DateTime.Now + "]" + "[" + usernBox.Text + "]" + writeBox.Text + "\r\n";
                using (UdpClient Send = new UdpClient())
                {
                    Send.EnableBroadcast = true;
                    Send.Send(Encoding.ASCII.GetBytes(message), Encoding.ASCII.GetBytes(message).Length, new IPEndPoint(IPAddress.Broadcast, 8888));
                }
                writeBox.Clear();
            }
        }

        private void rec(IAsyncResult res)
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 8888);
            string message = Encoding.ASCII.GetString(Receiver.EndReceive(res, ref RemoteIpEndPoint));
            if (message.Split(' ')[0] == "//" && message.Split(' ')[1] == "getusers")
            {
                //using (UdpClient Send = new UdpClient())
                //{
                //    string respond = usernBox.Text;
                //    Send.EnableBroadcast = true;
                //    Send.Send(Encoding.ASCII.GetBytes(respond), Encoding.ASCII.GetBytes(respond).Length, new IPEndPoint(IPAddress.Broadcast, 8888));
                //}
            }
            else
            {
                chatBox.AppendText(message);
                FlashWindow.Flash(this);
            }
            Receiver.BeginReceive(new AsyncCallback(rec), null);
        }

        private void usernBox_TextChanged(object sender, EventArgs e)
        {
            //using (UdpClient Send = new UdpClient())
            //{
            //    string message = usernBox.Text;
            //    Send.EnableBroadcast = true;
            //    Send.Send(Encoding.ASCII.GetBytes(message), Encoding.ASCII.GetBytes(message).Length, new IPEndPoint(IPAddress.Broadcast, 8888));
            //}
            //if (writeenabled)
            //{
            //    writeBox.Enabled = true;
            //}
            if (usernBox.Text != "")
            {
                writeBox.Enabled = true;
                writeBox.Clear();
                buttonSend.Enabled = true;
            }
            else
            {
                writeBox.Enabled = false;
                writeBox.Text = "Choose username first!";
                buttonSend.Enabled = false;
            }
        }

        private void opacityBar_Scroll(object sender, EventArgs e)
        {
            Opacity = (opacityBar.Value / (double)10);
            currentopacity = Opacity;
        }

        private void opacityBar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                if (Opacity == 0)
                {
                    Opacity = currentopacity;
                }
                else
                {
                    Opacity = 0;
                }
            }
        }

        private void changeFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.ShowDialog();
            chatBox.Font = fontDialog1.Font;
            writeBox.Font = fontDialog1.Font;
        }
    }
}
