﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    public partial class FormBall : Form
    {
        FormPlayer fp1 = new FormPlayer(Position.Left, Keys.W, Keys.S);
        FormPlayer fp2 = new FormPlayer(Position.Right, Keys.Up, Keys.Down);
        double angle = 0, stepDistance = 40;

        public FormBall()
        {
            InitializeComponent();
        }

        #region Standarthandler
        private void FormBall_Load(object sender, EventArgs e)
        {
            Height = 50;
            Width = 50;
            fp1.FormClosing += CloseHandler;
            fp2.FormClosing += CloseHandler;
            fp1.Show();
            fp2.Show();
            ResetPosition();
        }

        /// <summary>
        /// Schließt Form, wenn einer der Spieler-Forms geschlossen wird.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void CloseHandler(object o, EventArgs e)
        {
            Close();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void FormBall_Deactivate(object sender, EventArgs e)
        {
            Pause();
        }

        private void timerMove_Tick(object sender, EventArgs e)
        {
            if (!Move())    //Wenn nicht um volle Distanz bewegt werden konnte
            {
                RecalculateAngle();
            }
        }

        private void buttonStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (timerMove.Enabled)
            {
                switch (e.KeyCode)
                {
                    case Keys.P:
                        Pause();
                        break;
                }
            }
        }
        #endregion

        /// <summary>
        /// Startet oder "Unpausiert" Spiel
        /// </summary>
        private void Start()
        {
            if (!timerMove.Enabled)
            {
                buttonStart.Text = "";
                fp1.Start();
                fp2.Start();
                timerMove.Start();
            }
        }

        /// <summary>
        /// Pausiert das Spiel
        /// </summary>
        private void Pause()
        {
            if (timerMove.Enabled)
            {
                timerMove.Stop();
                fp1.Stop();
                fp2.Stop();
                buttonStart.Text = "Weiter";
            }
        }

        private void ResetPosition()
        {
            fp1.ResetPosition();
            fp2.ResetPosition();
            Location = new Point((Screen.PrimaryScreen.Bounds.Width - Size.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Size.Height) / 2);
            angle = 180;//new Random().Next(0, 2) * 180 + new Random().Next(-15, 15);
        }

        /// <summary>
        /// Berechnung der zu hinzuzufügenden X- und Y-Koordinaten.
        /// </summary>
        /// <returns></returns>
        private void CalculateMove(out int xAdd, out int yAdd, double distance)
        {
            xAdd = (int)(Math.Cos(angle / 180.0 * Math.PI) * distance);
            yAdd = (int)(Math.Sin(angle / 180.0 * Math.PI) * distance) * -1;
        }

        /// <summary>
        /// Bewegt den Ball. Gibt true zurück, wenn um die ganze Distanz bewegt werden konnte oder den Bildschirmrand erreicht wurde.
        /// </summary>
        /// <returns></returns>
        new private bool Move()
        {
            bool fullMove = true;
            int xAdd, yAdd; CalculateMove(out xAdd, out yAdd, stepDistance);
            if (Location.X + xAdd <= 0)  //Auswertung
            {
                fp2.Score(1);
                ResetPosition();
                return fullMove;
            }
            else if (Location.X + xAdd + Size.Width >= Screen.PrimaryScreen.Bounds.Width)
            {
                fp1.Score(1);
                ResetPosition();
                return fullMove;
            }
            for (double quarterStep = 3; quarterStep >= 0 && (this.Collides(fp1, xAdd, yAdd).Any ||      //Bei Colission weiter mit Quarter-Steps, wird 0 wenn bewegung nicht möglich
                this.Collides(fp2, xAdd, yAdd).Any || Location.Y + yAdd < 0 ||
                Location.Y + Size.Height + yAdd > Screen.PrimaryScreen.Bounds.Height); quarterStep--)
            {
                fullMove = false;
                CalculateMove(out xAdd, out yAdd, (stepDistance / 4) * quarterStep);
            }
            Location = new Point(Location.X + xAdd, Location.Y + yAdd);
            return fullMove;
        }

        /// <summary>
        /// Berechnet den Winkel für den nächsten Move neu.
        /// </summary>
        private void RecalculateAngle()
        {
            int xAdd, yAdd; CalculateMove(out xAdd, out yAdd, stepDistance);    //Nur um zu sehen, wo es Kollidiert
            #region  Abprallen von oberen und unteren Rand, bzw Spielern
            if (Location.Y + yAdd <= 0 || Location.Y + yAdd + Size.Height >= Screen.PrimaryScreen.Bounds.Height
                || this.Collides(fp1, xAdd, yAdd).Top || this.Collides(fp1, xAdd, yAdd).Bottom
                || this.Collides(fp2, xAdd, yAdd).Top || this.Collides(fp2, xAdd, yAdd).Bottom)
            {
                angle = 360 - angle;
                CalculateMove(out xAdd, out yAdd, stepDistance);
            }
            #endregion
            #region Seitliches abprallen von Spieler
            const int mod1 = 10, mod2 = 20;
            //5 Bereiche der Spieler:
            //||    +mod2        -mod2    ||
            //||    +mod1        -mod1    ||
            //||    +0°            +0°    ||
            //||    -mod1        +mod1    ||
            //||    -mod2        +mod2    ||
            if (this.Collides(fp1, xAdd, yAdd).Left)
            {
                angle = 360 - angle + (angle > 180 ? 180 : -180);
                int ballY = Location.Y + yAdd + (Size.Height / 2);
                if (ballY < fp1.Y - fp1.Height * 0.3)
                    angle += mod2;
                else if (ballY < fp1.Y - fp1.Height * 0.1)
                    angle += mod1;
                else if (ballY > fp1.Y + fp1.Height * 0.3)
                    angle -= mod2;
                else if (ballY > fp1.Y + fp1.Height * 0.1)
                    angle -= mod1;
                angle = angle % 360;
                if (angle < 0)
                    angle += 360;
            }
            else if (this.Collides(fp2, xAdd, yAdd).Right)
            {
                angle = 360 - angle + (angle > 180 ? 180 : -180);
                int ballY = Location.Y + yAdd + (Size.Height / 2);
                if (ballY < fp2.Y - fp2.Height * 0.3)
                    angle -= mod2;
                else if (ballY < fp2.Y - fp2.Height * 0.1)
                    angle -= mod1;
                else if (ballY > fp2.Y + fp2.Height * 0.3)
                    angle += mod2;
                else if (ballY > fp2.Y + fp2.Height * 0.1)
                    angle += mod1;
                angle = angle % 360;
                if (angle < 0)
                    angle += 360;
            }
            #endregion
        }
    }
}
