﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    public partial class FormPlayer : Form
    {
        [DllImport("user32.dll")]
        private static extern int GetKeyboardState(byte[] keystate);
        private int startPosX, startPosY;
        public int X { get { return Left + Width / 2; } }
        public int Y { get { return Top + Height / 2; } }
        private int score = 0;
        private byte[] keys = new byte[256];
        private readonly Keys MoveUpKey, MoveDownKey;

        public FormPlayer(Position position, Keys KeyUp, Keys KeyDown)
        {
            InitializeComponent();
            startPosX = (position == Position.Left) ? 0 : Screen.PrimaryScreen.Bounds.Width - Width;
            startPosY = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;
            MoveUpKey = KeyUp;
            MoveDownKey = KeyDown;
            ResetPosition();
        }

        private void timerScanForKeys_Tick(object sender, EventArgs e)
        {
            GetKeyboardState(keys);
            if (keys[(int)MoveUpKey] >= 128 && Top > 0)
            {
                Top -= 20;
            }
            if (keys[(int)MoveDownKey] >= 128 && Top < Screen.PrimaryScreen.Bounds.Height - Height)
            {
                Top += 20;
            }
        }

        private void FormPlayer_Load(object sender, EventArgs e)
        {
            //Location = new Point(X, Y);
        }

        public void Start()
        {
            timerScanForKeys.Start();
        }

        public void Stop()
        {
            timerScanForKeys.Stop();
        }

        public void ResetPosition()
        {
            Left = startPosX;
            Top = startPosY;
        }

        /// <summary>
        /// Fügt dem Score den bestimmten Wert hinzu.
        /// </summary>
        /// <param name="increasement"></param>
        public void Score(int increasement)
        {
            score += increasement;
            scoreLabel.Text = $"Score:\n{score}";
        }
    }
}
