﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormBall());
        }
    }

    public enum Position
    {
        Left,
        Right
    }

    public static class Extensions
    {
        //public static bool CollidesTop(this Form currentFrom, Form form, int xAdd, int yAdd)
        //{
        //    bool onSameWidth = currentFrom.Location.X <= form.Location.X + xAdd && form.Location.X + xAdd <= currentFrom.Location.X + currentFrom.Size.Width;
        //    if (onSameWidth && (form.Location.Y + yAdd + form.Size.Height) >= currentFrom.Location.Y && (form.Location.Y + yAdd + form.Size.Height) < currentFrom.Location.Y + currentFrom.Size.Height / 2)
        //        return true;
        //    else
        //        return false;
        //}

        //public static bool CollidesBottom(this Form currentFrom, Form form, int xAdd, int yAdd)
        //{
        //    bool onSameWidth = currentFrom.Location.X <= form.Location.X + xAdd && form.Location.X + xAdd <= currentFrom.Location.X + currentFrom.Size.Width;
        //    if (onSameWidth && (form.Location.Y + yAdd) <= currentFrom.Location.Y + currentFrom.Size.Height && (form.Location.Y + yAdd) > currentFrom.Location.Y + currentFrom.Size.Height / 2)
        //        return true;
        //    else
        //        return false;
        //}

        //public static bool CollidesLeft(this Form currentFrom, Form form, int xAdd, int yAdd)
        //{
        //    bool onSameHieght = currentFrom.Location.Y <= form.Location.Y + yAdd && form.Location.Y + yAdd <= currentFrom.Location.Y + currentFrom.Size.Height;
        //    if (onSameHieght && (form.Location.X + xAdd + form.Size.Width) >= currentFrom.Location.X && (form.Location.X + xAdd + form.Size.Width) < currentFrom.Location.X + currentFrom.Size.Width / 2)
        //        return true;
        //    else
        //        return false;
        //}

        //public static bool CollidesRight(this Form currentFrom, Form form, int xAdd, int yAdd)
        //{
        //    bool onSameHieght = currentFrom.Location.Y <= form.Location.Y + yAdd && form.Location.Y + yAdd <= currentFrom.Location.Y + currentFrom.Size.Height;
        //    if (onSameHieght && (form.Location.X + xAdd) <= currentFrom.Location.X + currentFrom.Size.Width && (form.Location.X + xAdd) > currentFrom.Location.X + currentFrom.Size.Width / 2)
        //        return true;
        //    else
        //        return false;
        //}

        //public static bool CollidesOld(this Form currentForm, Form form, int xAdd, int yAdd)
        //{
        //    return CollidesTop(currentForm, form, xAdd, yAdd) || CollidesBottom(currentForm, form, xAdd, yAdd) || CollidesLeft(currentForm, form, xAdd, yAdd) || CollidesRight(currentForm, form, xAdd, yAdd);
        //}

        /// <summary>
        /// Überprüft, ob das Form mit einem Anderen kollidieren würde, wenn xAdd und yAdd addiert werden.
        /// </summary>
        /// <param name="currentForm"></param>
        /// <param name="form"></param>
        /// <param name="xAdd"></param>
        /// <param name="yAdd"></param>
        /// <returns></returns>
        public static (bool Right, bool Left, bool Top, bool Bottom, bool Any) Collides(this Form currentForm, Form form, int xAdd, int yAdd)
        {
            int currentFormRight = currentForm.Location.X + currentForm.Size.Width, currentFormBottom = currentForm.Location.Y + currentForm.Size.Height, formRight = form.Location.X + form.Size.Width, formBottom = form.Location.Y + form.Size.Height;

            bool rightBound = currentFormRight + xAdd >= form.Location.X && currentFormRight + xAdd <= formRight;
            bool leftBound = currentForm.Location.X + xAdd <= formRight && currentForm.Location.X + xAdd >= form.Location.X;
            bool topBound = currentForm.Location.Y + yAdd <= formBottom && currentForm.Location.Y + yAdd >= form.Location.Y;
            bool bottomBound = currentFormBottom + yAdd >= form.Location.Y && currentFormBottom + yAdd <= formBottom;

            bool right = rightBound && (topBound || bottomBound) && !leftBound, left = leftBound && (topBound || bottomBound) && !rightBound, top = topBound && (rightBound || leftBound) && !bottomBound, bottom = bottomBound && (rightBound || leftBound) && !topBound;

            return (right, left, top, bottom, right || left || top || bottom);
        }
    }
}
