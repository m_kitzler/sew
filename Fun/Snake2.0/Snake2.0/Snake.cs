﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    class Snake
    {
        int fieldX, fieldY;
        List<Item> body = new List<Item>();
        Direction lastDir = Direction.Right;
        public Snake(int fieldWidth, int fieldHeight)
        {
            fieldX = fieldWidth; fieldY = fieldHeight;
            body.Add(new Item ((int)Math.Floor((double)(fieldX / 2)), (int)Math.Floor((double)(fieldY / 2))));
            Grow(2);
        }
        public void Move(Direction facing = Direction.None)
        {
            if (facing == Direction.None || Convert.ToInt32(facing) == Convert.ToInt32(lastDir) + ((Convert.ToInt32(lastDir) > 2) ? -2 : 2))
            {
                facing = lastDir;   //Zuweisung wenn keine Input und verhinderung von Rückwertsbew.
            }
            else
            {
                lastDir = facing;   //Merken der zuletzt geg. Richtung
            }
            //for (int i = body.Count - 1; i >= 1; i--)   //Bewegen aller Items außer Ersten (Von hinten nach vorn)
            //{
            //    body[i].X = body[i - 1].X;
            //    body[i].Y = body[i - 1].Y;
            //}
            //switch (facing)
            //{
            //    case Direction.Down:
            //        body[0].Y = (body[0].Y >= fieldY - 1) ? 0 : body[0].Y + 1;
            //        break;
            //    case Direction.Left:
            //        body[0].X = (body[0].X <= 0) ? fieldX - 1 : body[0].X - 1;
            //        break;
            //    case Direction.Right:
            //        body[0].X = (body[0].X >= fieldX - 1) ? 0 : body[0].X + 1;
            //        break;
            //    case Direction.Up:
            //        body[0].Y = (body[0].Y <= 0) ? fieldY - 1 : body[0].Y - 1;
            //        break;
            //}
            switch (facing) //Bewegen des letzten Items auf erste Stelle
            {
                case Direction.Down:
                    body.Insert(0, new Item(body[0].X, (body[0].Y >= fieldY - 1) ? 0 : body[0].Y + 1));
                    break;
                case Direction.Left:
                    body.Insert(0, new Item((body[0].X <= 0) ? fieldX - 1 : body[0].X - 1, body[0].Y));
                    break;
                case Direction.Right:
                    body.Insert(0, new Item((body[0].X >= fieldX - 1) ? 0 : body[0].X + 1, body[0].Y));
                    break;
                case Direction.Up:
                    body.Insert(0, new Item(body[0].X, (body[0].Y <= 0) ? fieldY - 1 : body[0].Y - 1));
                    break;
            }
            body.RemoveAt(body.Count - 1);
        }
        public bool Check()
        {
            for (int i = 3; i < body.Count - 1; i++)
            {
                if (body[0].X == body[i].X && body[0].Y == body[i].Y)
                    return true;
            }
            return false;
        }
        public void Grow(int Amount = 1)
        {
            for (int i = 0; i < Amount; i++)
            {
                body.Add(new Item (body[body.Count - 1].X, body[body.Count - 1].Y ));
            }
        }
        public List<Item> Body
        {
            get { return body; }
        }
        public Item First
        {
            get { return new Item (body[0].X, body[0].Y); }
        }
    }
}
