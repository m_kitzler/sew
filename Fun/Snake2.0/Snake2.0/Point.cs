﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    class Point
    {
        int fieldX, fieldY;
        Item Pos = new Item(0,0);
        Random r = new Random();
        public Point(int fieldWidth, int fieldHeight, List<Item> snkBody)
        {
            fieldX = fieldWidth; fieldY = fieldHeight;
            NewPosition(snkBody);
        }
        public Item Position
        {
            get { return Pos; }
        }
        public Item NewPosition(List<Item> snekBody)
        {
            bool plshelp = true;
            while (plshelp)
            {
                plshelp = false;
                Pos.X = r.Next(0, fieldX);
                Pos.Y = r.Next(0, fieldY);
                for (int i = 0; i < snekBody.Count - 1; i++)
                {
                    if (Pos.X == snekBody[i].X && Pos.Y == snekBody[i].Y)
                        plshelp = true;
                }
            }
            return Pos;
        }
    }
}
