﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

enum Direction
{
    None, Up, Right, Down, Left
}
namespace Snake2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            int width = 20, height = 20, speed = 5;
            Console.WindowHeight = (height > 63) ? 63 : height;
            Console.WindowWidth = (width * 2 > 240) ? 240 : width * 2;
            Console.BufferHeight = (height > 63) ? 63 : height;
            Console.BufferWidth = (width * 2 > 240) ? 240 : (width * 2 < 16) ? 16 : width * 2;
            Console.CursorVisible = false;
            int Choice = 1;
            string scoresPath = @"scores.bin";
            Highscores hs = new Highscores(10);
            if (File.Exists(scoresPath))
            {
                hs.LoadFromFile(scoresPath);
            }
            string name = "Player";
            bool exit = false, input = true;
            while (!exit)
            {
                #region Eingabe
                while (input)
                {
                    switch (Display.mainMenu(Choice, name, speed, hs))
                    {
                        case ConsoleKey.W:
                        case ConsoleKey.UpArrow:
                            if (Choice == 1)
                                Choice = 4;
                            else
                                Choice--;
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.DownArrow:
                            if (Choice == 4)
                                Choice = 1;
                            else
                                Choice++;
                            break;
                        case ConsoleKey.Enter:
                            input = false;
                            break;
                        case ConsoleKey.A:
                        case ConsoleKey.LeftArrow:
                            if (Choice == 2)
                                speed = 5;
                            break;
                        case ConsoleKey.D:
                        case ConsoleKey.RightArrow:
                            if (Choice == 2)
                                speed = 10;
                            break;
                    }
                    Console.Clear();
                }
                input = true;
                #endregion
                switch (Choice)
                {
                    case 1:
                        int score = Game.Start(width, height, speed);
                        hs.Add(name, score.ToString());
                        hs.SaveToFile(scoresPath);
                        break;
                    case 3:
                        Console.WriteLine("Gib deinen Namen ein:");
                        Console.CursorVisible = true;
                        name = Console.ReadLine();
                        Console.CursorVisible = false;
                        break;
                    case 4:
                        exit = true;
                        break;
                }
                Choice = 1;
                Console.Clear();
            }
        }
    }
}
