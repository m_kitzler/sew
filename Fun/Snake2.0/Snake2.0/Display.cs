﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    class Display
    {
        static public ConsoleKey mainMenu(int choice, string name, int speed, Highscores hs)
        {
            Console.WriteLine("Snake\t\tName: {0}\n", name);
            if (choice == 1)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Spiel starten");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            if (choice == 2)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Geschwindigkeit: {0}", (speed == 5) ? "Schnell" : "Langsam");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            if (choice == 3)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Name ändern");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            if (choice == 4)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Beenden\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            for (int i = 0; i < hs.Length(); i++)
            {
                Console.WriteLine((i + 1) + ":\t" + ((hs[i, 0] == null || hs[i, 0] == "") ? "" : hs[i, 0] + " mit " + hs[i, 1]));
            }
            return Console.ReadKey(true).Key;
        }
        static public void Snake(Snake Snek)
        {
            Console.SetCursorPosition(Snek.Body[Snek.Body.Count - 1].X * 2, Snek.Body[Snek.Body.Count - 1].Y);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("██");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = Snek.Body.Count - 2; i >= 0; i--)
            {
                Console.SetCursorPosition(Snek.Body[i].X * 2, Snek.Body[i].Y);
                Console.Write("██");
            }
        }
        static public void Point(Item Pos)
        {
            Console.SetCursorPosition(Pos.X * 2, Pos.Y);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("██");
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
