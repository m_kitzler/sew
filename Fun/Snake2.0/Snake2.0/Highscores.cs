﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    public static class Extensions
    {
        public static string ToHex(this string s, char seperator)
        {
            if (s == null || s.Length <= 0)
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append(Convert.ToInt32(s[0]).ToString("X"));
            for (int i = 1; i < s.Length; i++)
            {
                sb.Append(seperator);
                sb.Append(Convert.ToInt32(s[i]).ToString("X"));
            }
            return sb.ToString();
        }
        public static string FromHexToString(this string s, char seperator)
        {
            if (s == null || s.Length <= 0)
                return null;
            StringBuilder sb = new StringBuilder();
            string[] temp = s.Split(seperator);
            foreach (string S in temp)
            {
                if (S.Length <= 0)
                    continue;
                sb.Append(Convert.ToChar(int.Parse(S, System.Globalization.NumberStyles.HexNumber)));
            }
            return sb.ToString();
        }
    }
    class Highscores
    {
        private string[,] scores;
        public Highscores(int size)
        {
            scores = new string[size, 2];
        }
        public string this[int i, int j]
        {
            get
            {
                return scores[i, j];
            }
            set
            {
                scores[i, j] = value;
            }
        }
        /// <summary>
        /// Gibt die größe des Arrays zurück
        /// </summary>
        /// <returns></returns>
        public int Length()
        {
            return scores.GetLength(0);
        }
        /// <summary>
        /// Vergleicht den mitgegebenen Score und fügt ihn gegebenenfalls hinzu.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="score"></param>
        public void Add(string name, string score)
        {
            for (int i = 0; i < scores.GetLength(0); i++)
            {
                if (Convert.ToInt32(score) > Convert.ToInt32(scores[i, 1]))
                {
                    for (int j = scores.GetLength(0) - 1; j > i; j--)
                    {
                        scores[j, 0] = scores[j - 1, 0];
                        scores[j, 1] = scores[j - 1, 1];
                    }
                    scores[i, 0] = name;
                    scores[i, 1] = score;
                    break;
                }
            }
        }
        public void SaveToFile(string path)
        {
            using (StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.Create)))
            {
                for (int i = 0; i < scores.GetLength(0); i++)
                {
                    if (scores[i, 0] == null || scores[i, 0] == "")
                        continue;
                    sw.WriteLine(scores[i, 0].ToHex(' ') + ";" + scores[i, 1].ToHex(' '));
                }
            }
        }
        public void LoadFromFile(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                for (int i = 0; i < scores.GetLength(0); i++)
                {
                    string Data = sr.ReadLine();
                    if (Data == null || Data == "")
                        continue;
                    string[] data = Data.Split(';');
                    scores[i, 0] = data[0].FromHexToString(' ');
                    scores[i, 1] = data[1].FromHexToString(' ');
                }
            }
        }
    }
}
