﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    class Item
    {
        int PosX, PosY;
        public Item(int X, int Y)
        {
            PosX = X; PosY = Y;
        }
        public int X
        {
            get { return PosX; }
            set { PosX = value; }
        }
        public int Y
        {
            get { return PosY; }
            set { PosY = value; }
        }
    }
}
