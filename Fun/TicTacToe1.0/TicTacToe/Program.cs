﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;                  //zu erinnerung: spielfeld[x = Zeile,y = Spalte]
            Console.ForegroundColor = ConsoleColor.Green;
            #region Variablen und so
            bool nochmal = true;
            bool exit = true;
            bool spiel = true;
            bool einzelspieler = false;
            bool Cheat = false;
            Random r = new Random();
            ConsoleKeyInfo Eingabe;
            string eingabe = "";
            string sp1 = "Spieler 1";
            string sp2name = "Spieler 2";
            string sp2 = sp2name;
            string aktuellerspieler;
            string cheat;
            int[,] spielfeld = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
            int[] zws = { 0, 0 };
            int aktuellerzug;
            int forschleife;
            #endregion
            while (!exit)
            {
                nochmal = true;
                #region Spielmoduseingabe
                for (int x = 1; x != 0; x--)
                {
                    Console.Write("Tic Tac Toe\n1) Einzelspieler\n2) Mehrspieler\n3) Namen ändern\n4) Beenden");
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.D1:
                            einzelspieler = true;
                            break;
                        case ConsoleKey.D2:
                            einzelspieler = false;
                            break;
                        case ConsoleKey.D3:
                            x++;
                            Console.Clear();
                            Namen(ref sp1, ref sp2name);
                            break;
                        case ConsoleKey.D4:
                            exit = true;
                            nochmal = false;
                            break;
                        default:
                            x++;
                            Console.Clear();
                            Console.Write("Tic Tac Toe\n1) Einzelspieler\n2) Mehrspieler\n3) Namen ändern\n4) Beenden\nKeine gültige Eingabe");
                            Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                    Console.Clear();
                }
                #endregion
                #region Spiel
                while (nochmal)
                {
                    aktuellerzug = r.Next(1, 3);
                    if (einzelspieler == true)
                        sp2 = "Computer";
                    else
                        sp2 = sp2name;
                    while (spiel)
                    {
                        #region Eingabe Spieler
                        if (aktuellerzug == 1 || (aktuellerzug == 2 && einzelspieler == false))
                        {
                            for (forschleife = 1; forschleife != 0; forschleife--)
                            {
                                Console.Clear();
                                anzeige(spielfeld, sp1, sp2);
                                if (aktuellerzug == 1)
                                {
                                    aktuellerspieler = sp1;
                                }
                                else
                                {
                                    aktuellerspieler = sp2;
                                }
                                Console.WriteLine("\n{0} ist dran", aktuellerspieler);;
                                switch (Console.ReadKey(true).Key)
                                {
                                    case ConsoleKey.D7:
                                    case ConsoleKey.NumPad7:
                                        zws[0] = 0; zws[1] = 0;
                                        break;
                                    case ConsoleKey.D8:
                                    case ConsoleKey.NumPad8:
                                        zws[0] = 0; zws[1] = 1;
                                        break;
                                    case ConsoleKey.D9:
                                    case ConsoleKey.NumPad9:
                                        zws[0] = 0; zws[1] = 2;
                                        break;
                                    case ConsoleKey.D4:
                                    case ConsoleKey.NumPad4:
                                        zws[0] = 1; zws[1] = 0;
                                        break;
                                    case ConsoleKey.D5:
                                    case ConsoleKey.NumPad5:
                                        zws[0] = 1; zws[1] = 1;
                                        break;
                                    case ConsoleKey.D6:
                                    case ConsoleKey.NumPad6:
                                        zws[0] = 1; zws[1] = 2;
                                        break;
                                    case ConsoleKey.D1:
                                    case ConsoleKey.NumPad1:
                                        zws[0] = 2; zws[1] = 0;
                                        break;
                                    case ConsoleKey.D2:
                                    case ConsoleKey.NumPad2:
                                        zws[0] = 2; zws[1] = 1;
                                        break;
                                    case ConsoleKey.D3:
                                    case ConsoleKey.NumPad3:
                                        zws[0] = 2; zws[1] = 2;
                                        break;
                                    #region cheats
                                    case ConsoleKey.Enter:
                                        Console.CursorVisible = true;
                                        cheat = Console.ReadLine();
                                        if (cheat == "/win")
                                        {
                                            for (int x = 0; x < 2; x++)
                                            {
                                                for (int y = 0; y < 3; y++)
                                                {
                                                    spielfeld[x, y] = aktuellerzug;
                                                }
                                            }
                                            Cheat = true;
                                        }
                                        else
                                        if (cheat == "/lose")
                                        {
                                            for (int x = 0; x < 2; x++)
                                            {
                                                for (int y = 0; y < 3; y++)
                                                {
                                                    if (aktuellerzug == 1)
                                                    {
                                                        spielfeld[x, y] = 2;
                                                    }
                                                    else if (aktuellerzug == 2)
                                                    {
                                                        spielfeld[x, y] = 1;
                                                    }
                                                }
                                            }
                                            Cheat = true;
                                        }
                                        else
                                            forschleife++;
                                        Console.CursorVisible = false;
                                        break;
                                        #endregion
                                }
                                if (spielfeld[zws[0], zws[1]] == 0 && Cheat == false)
                                    spielfeld[zws[0], zws[1]] = aktuellerzug;
                                else
                                if (Cheat == false)
                                {
                                    forschleife++;
                                    Console.Clear();
                                    anzeige(spielfeld, sp1, sp2);
                                    Console.WriteLine("\nEingabe ungültig!\nDrücken sie eine Taste zum fortfahren.");
                                    Console.ReadKey();
                                    Console.Clear();
                                }
                            }
                            aktuellerzug++;
                            if (aktuellerzug == 3)
                                aktuellerzug = 1;
                        }
                        #endregion
                        #region Eingabe Computer
                        else
                        {
                            Console.Clear();
                            anzeige(spielfeld, sp1, sp2);
                            Console.WriteLine("\n{0} ist dran", sp2);
                            Thread.Sleep(1000);
                            for (forschleife = 1; forschleife != 0; forschleife--)
                            {
                                #region Gewinne
                                //erste Zeile
                                if (spielfeld[0, 0] == 1 && spielfeld[0, 1] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[0, 2] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[0, 2] == 1 && spielfeld[0, 1] == 0)
                                    spielfeld[0, 1] = 2;
                                //zweite Zeile
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[1, 2] == 0)
                                    spielfeld[1, 2] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[1, 2] == 1 && spielfeld[1, 0] == 0)
                                    spielfeld[1, 0] = 2;
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[1, 2] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                //dritte Zeile
                                else
                                if (spielfeld[2, 0] == 1 && spielfeld[2, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[2, 1] == 1 && spielfeld[2, 2] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[2, 0] == 1 && spielfeld[2, 2] == 1 && spielfeld[2, 1] == 0)
                                    spielfeld[2, 1] = 2;
                                //erste Spalte
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 0] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[2, 0] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[2, 0] == 1 && spielfeld[1, 0] == 0)
                                    spielfeld[1, 0] = 2;
                                //zweite Spalte
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 1] == 0)
                                    spielfeld[2, 1] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 1] == 1 && spielfeld[0, 1] == 0)
                                    spielfeld[0, 1] = 2;
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[2, 1] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                //dritte Spalte
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[1, 2] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[1, 2] == 1 && spielfeld[2, 2] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[2, 2] == 1 && spielfeld[1, 2] == 0)
                                    spielfeld[1, 2] = 2;
                                //diagonal oben links unten rechts
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 2] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                //diagonal oben rechts unten links
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 0] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[2, 0] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                else
                                #endregion
                                #region Niederlage verhindern
                                //erste Zeile
                                if (spielfeld[0, 0] == 1 && spielfeld[0, 1] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[0, 2] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[0, 2] == 1 && spielfeld[0, 1] == 0)
                                    spielfeld[0, 1] = 2;
                                //zweite Zeile
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[1, 2] == 0)
                                    spielfeld[1, 2] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[1, 2] == 1 && spielfeld[1, 0] == 0)
                                    spielfeld[1, 0] = 2;
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[1, 2] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                //dritte Zeile
                                else
                                if (spielfeld[2, 0] == 1 && spielfeld[2, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[2, 1] == 1 && spielfeld[2, 2] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[2, 0] == 1 && spielfeld[2, 2] == 1 && spielfeld[2, 1] == 0)
                                    spielfeld[2, 1] = 2;
                                //erste Spalte
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 0] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[1, 0] == 1 && spielfeld[2, 0] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[2, 0] == 1 && spielfeld[1, 0] == 0)
                                    spielfeld[1, 0] = 2;
                                //zweite Spalte
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 1] == 0)
                                    spielfeld[2, 1] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 1] == 1 && spielfeld[0, 1] == 0)
                                    spielfeld[0, 1] = 2;
                                else
                                if (spielfeld[0, 1] == 1 && spielfeld[2, 1] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                //dritte Spalte
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[1, 2] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[1, 2] == 1 && spielfeld[2, 2] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[2, 2] == 1 && spielfeld[1, 2] == 0)
                                    spielfeld[1, 2] = 2;
                                //diagonal oben links unten rechts
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 2] == 1 && spielfeld[0, 0] == 0)
                                    spielfeld[0, 0] = 2;
                                else
                                if (spielfeld[0, 0] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 2] == 0)
                                    spielfeld[2, 2] = 2;
                                //diagonal oben rechts unten links
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[1, 1] == 1 && spielfeld[2, 0] == 0)
                                    spielfeld[2, 0] = 2;
                                else
                                if (spielfeld[1, 1] == 1 && spielfeld[2, 0] == 1 && spielfeld[0, 2] == 0)
                                    spielfeld[0, 2] = 2;
                                else
                                if (spielfeld[0, 2] == 1 && spielfeld[2, 0] == 1 && spielfeld[1, 1] == 0)
                                    spielfeld[1, 1] = 2;
                                #endregion
                                #region Random
                                else
                                {
                                    zws[0] = r.Next(0, 3);
                                    zws[1] = r.Next(0, 3);
                                    if (spielfeld[zws[0], zws[1]] == 0)
                                        spielfeld[zws[0], zws[1]] = 2;
                                    else
                                        forschleife++;
                                }
                                #endregion
                            }
                            aktuellerzug = 1;
                        }
                        #endregion
                        auswertung(spielfeld, sp1, sp2, ref spiel);
                    }
                    #endregion
                    #region Nach Spiel
                    for (int x = 1; x != 0; x--)
                    {
                        Console.WriteLine("\n1)Nochmal\n2)Hauptmenü\n3)Beenden");
                        Cheat = false;
                        switch (Console.ReadKey(true).Key)
                        {
                            case ConsoleKey.D1:
                                nochmal = true;
                                spiel = true;
                                for (int i = 0; i < 3; i++)
                                    for (int j = 0; j < 3; j++)
                                        spielfeld[i, j] = 0;
                                break;
                            case ConsoleKey.D2:
                                Console.Clear();
                                nochmal = false;
                                spiel = true;
                                for (int i = 0; i < 3; i++)
                                    for (int j = 0; j < 3; j++)
                                        spielfeld[i, j] = 0;
                                break;
                            case ConsoleKey.D3:
                                nochmal = false;
                                exit = true;
                                break;
                            default:
                                Console.Clear();
                                anzeige(spielfeld, sp1, sp2);
                                Console.WriteLine("\nKeine gültige Eingabe");
                                Thread.Sleep(1000);
                                Console.Clear();
                                anzeige(spielfeld, sp1, sp2);
                                x++;
                                break;
                        }
                    }
                }
                #endregion
            }
        }
        /// <summary>
        /// Zeigt das Spielfeld an.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        static void anzeige(int[,] a, string b, string c)
        {
            Console.WriteLine("|{0}| X - O |{1}|\n", b, c);
            #region erste Zeile
            //erste Ziele der ersten Zeile
            if (a[0, 0] == 1)
                Console.Write("\\_/");
            else
            if (a[0, 0] == 2)
                Console.Write("|¯|");
            else
                Console.Write("7  ");
            Console.Write(" | ");
            if (a[0, 1] == 1)
                Console.Write("\\_/");
            else
            if (a[0, 1] == 2)
                Console.Write("|¯|");
            else
                Console.Write("8  ");
            Console.Write(" | ");
            if (a[0, 2] == 1)
                Console.Write("\\_/");
            else
            if (a[0, 2] == 2)
                Console.Write("|¯|");
            else
                Console.Write("9  ");
            Console.WriteLine("");
            //zweite Zeile der ersten Zeile
            if (a[0, 0] == 1)
                Console.Write("/ \\");
            else
            if (a[0, 0] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[0, 1] == 1)
                Console.Write("/ \\");
            else
            if (a[0, 1] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[0, 2] == 1)
                Console.Write("/ \\");
            else
            if (a[0, 2] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            //dritte Zeile der ersten Zeile
            Console.Write("____|_____|____\n");
            #endregion
            #region zweite Zeile
            //erste Ziele der zweiten Zeile
            if (a[1, 0] == 1)
                Console.Write("\\_/");
            else
            if (a[1, 0] == 2)
                Console.Write("|¯|");
            else
                Console.Write("4  ");
            Console.Write(" | ");
            if (a[1, 1] == 1)
                Console.Write("\\_/");
            else
            if (a[1, 1] == 2)
                Console.Write("|¯|");
            else
                Console.Write("5  ");
            Console.Write(" | ");
            if (a[1, 2] == 1)
                Console.Write("\\_/");
            else
            if (a[1, 2] == 2)
                Console.Write("|¯|");
            else
                Console.Write("6  ");
            Console.WriteLine("");
            //zweite Zeile der zweiten Zeile
            if (a[1, 0] == 1)
                Console.Write("/ \\");
            else
            if (a[1, 0] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[1, 1] == 1)
                Console.Write("/ \\");
            else
            if (a[1, 1] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[1, 2] == 1)
                Console.Write("/ \\");
            else
            if (a[1, 2] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            //dritte Zeile der zweiten Zeile
            Console.Write("____|_____|____\n");
            #endregion
            #region dritte Zeile
            //erste Ziele der dritten Zeile
            if (a[2, 0] == 1)
                Console.Write("\\_/");
            else
            if (a[2, 0] == 2)
                Console.Write("|¯|");
            else
                Console.Write("1  ");
            Console.Write(" | ");
            if (a[2, 1] == 1)
                Console.Write("\\_/");
            else
            if (a[2, 1] == 2)
                Console.Write("|¯|");
            else
                Console.Write("2  ");
            Console.Write(" | ");
            if (a[2, 2] == 1)
                Console.Write("\\_/");
            else
            if (a[2, 2] == 2)
                Console.Write("|¯|");
            else
                Console.Write("3  ");
            Console.WriteLine("");
            //zweite Zeile der dritten Zeile
            if (a[2, 0] == 1)
                Console.Write("/ \\");
            else
            if (a[2, 0] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[2, 1] == 1)
                Console.Write("/ \\");
            else
            if (a[2, 1] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.Write(" | ");
            if (a[2, 2] == 1)
                Console.Write("/ \\");
            else
            if (a[2, 2] == 2)
                Console.Write("|_|");
            else
                Console.Write("   ");
            Console.WriteLine("");
            #endregion
        }
        /// <summary>
        /// Überprüft, ob ein Spieler gewonnen hat oder ob es Unentschieden steht.
        /// </summary>
        /// <param name="ttt"></param>
        /// <param name="sp1"></param>
        /// <param name="sp2"></param>
        /// <param name="spiel"></param>
        static void auswertung(int[,] ttt, string sp1, string sp2, ref bool spiel)
        {
            int a = 0;
            #region Auswertung waagrecht
            for (int x = 0; x < 3; x++)
            {
                if (ttt[x, 0] != 0 && ttt[x, 0] == ttt[x, 1] && ttt[x, 0] == ttt[x, 2])
                {
                    a = ttt[x, 0];
                }
            }
            #endregion
            #region Auswertung senkrecht
            if (a == 0)
                for (int x = 0; x < 3; x++)
                {
                    if (ttt[0, x] != 0 && ttt[0, x] == ttt[1, x] && ttt[0, x] == ttt[2, x])
                    {
                        a = ttt[0, x];
                    }
                }
            #endregion
            #region Auswertung diagonal
            if (ttt[0, 0] != 0 && ttt[0, 0] == ttt[1, 1] && ttt[0, 0] == ttt[2, 2])
            {
                a = ttt[0, 0];
            }
            else
            if (ttt[0, 2] != 0 && ttt[0, 2] == ttt[1, 1] && ttt[0, 2] == ttt[2, 0])
            {
                a = ttt[0, 2];
            }
            #endregion
            #region Unentschieden
            if (ttt[0, 0] != 0 && ttt[0, 1] != 0 && ttt[0, 2] != 0 && ttt[1, 0] != 0 && ttt[1, 1] != 0 && ttt[1, 2] != 0 && ttt[2, 0] != 0 && ttt[2, 1] != 0 && ttt[2, 2] != 0)
                a = 3;
            #endregion
            if (a == 1)
            {
                Console.Clear();
                anzeige(ttt, sp1, sp2);
                Console.WriteLine("\n{0} hat Gewonnen!", sp1);
                spiel = false;
            }
            else
            if (a == 2)
            {
                Console.Clear();
                anzeige(ttt, sp1, sp2);
                Console.WriteLine("\n{0} hat Gewonnen!", sp2);
                spiel = false;
            }
            if (a == 3)
            {
                Console.Clear();
                anzeige(ttt, sp1, sp2);
                Console.WriteLine("\nUnentschieden!");
                spiel = false;
            }
        }
        /// <summary>
        /// Legt die Spilernamen fest.
        /// </summary>
        /// <param name="sp1"></param>
        /// <param name="sp2name"></param>
        static void Namen(ref string sp1, ref string sp2name)
        {
            Console.CursorVisible = true;
            Console.Write("Tic Tac Toe\nName von Spieler 1:");
            sp1 = Console.ReadLine();
            Console.Write("Name von Spieler 2:");
            sp2name = Console.ReadLine();
            Console.CursorVisible = false;
        }
    }
}