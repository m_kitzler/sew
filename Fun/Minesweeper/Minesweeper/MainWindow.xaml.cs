﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Minesweeper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random r;
        Dictionary<(int X, int Y), Grid> fields = new Dictionary<(int X, int Y), Grid>();   //Grid pro Feld, damit ohne viel Aufwand mehrere Elemente eingefügt werden können
        int bombs = 0;
        int flags = 0;
        int successfulFlag = 0;
        List<Polygon> overlayPolys = new List<Polygon>();

        public enum State
        {
            Hidden,
            Uncovered,
            Flagged
        }

        public MainWindow()
        {
            InitializeComponent();

            r = new Random();
            Reset();

            //GridTest.Children.Add(GetIcon(IsRightPoly(GridTest.Children.OfType<Polygon>().First()), -1));
        }

        /// <summary>
        /// Generiert nach rechts oder links zeigende Polygone
        /// </summary>
        /// <param name="facingRight"></param>
        /// <returns></returns>
        private Polygon PolyGen(bool facingRight)
        {
            bool bomb = r.Next(101) <= 10;
            string p1 = facingRight ? "0" : "30", p2 = !facingRight ? "0" : "30", p3 = facingRight ? "0" : "30";                            //Polygonmaße
            Polygon ret = new Polygon
            {
                Points = PointCollection.Parse($"{p1},0 {p2},20 {p3},40"),                                                                  //Polygonmaße
                Fill = Brushes.White,
                VerticalAlignment = VerticalAlignment.Center
            };
            ret.Resources.Add("state", State.Hidden);
            ret.Resources.Add("isBomb", bomb);
            if (bomb)
                bombs++;
            ret.MouseLeftButtonDown += Polygon_Uncover;
            ret.MouseRightButtonDown += Polygon_Flag;
            ret.MouseEnter += Polygon_Highlight;
            return ret;
        }

        /// <summary>
        /// Prüft, ob Polygon nach rechts zeigt.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private bool IsRightPoly(Polygon p)
        {
            return p.Points.Where((Point p) => p.X == 0).Count() == 2;
        }

        /// <summary>
        /// Platziert Grid-Elemente mit Polygonen in das angegebene <paramref name="field"/>.
        /// Ignoriert dabei die erste und letzte Row/Column.
        /// </summary>
        /// <param name="field"></param>
        /// <returns>Ein Dictionary mit den Koordinaten und dem jeweiligen Grid-Element.</returns>
        private Dictionary<(int X, int Y), Grid> GenField(Grid field)
        {
            Dictionary<(int X, int Y), Grid> fieldsDict = new Dictionary<(int X, int Y), Grid>();
            for (int x = 1; x < field.ColumnDefinitions.Count - 1; x++)
            {
                bool faceRight = x % 2 == 0;
                for (int y = 1; y < field.RowDefinitions.Count - 2; y++)    // -2, da ein Feld eine RowSpan von 2 hat
                {
                    Grid g = new Grid();
                    Grid.SetRowSpan(g, 2);
                    Grid.SetColumn(g, x);
                    Grid.SetRow(g, y);
                    g.Children.Add(PolyGen(faceRight));
                    faceRight = !faceRight;
                    field.Children.Add(g);
                    fieldsDict[(X: x, Y: y)] = g;
                }
            }
            return fieldsDict;
        }

        /// <summary>
        /// Gibt ein Grid-Element mit dem Symbol oder der Zahl für ein Polygon zurück.
        /// </summary>
        /// <param name="facingRight">Fügt korrekten Margin ein, je nachdem Polygon nach Rechts zeigt oder nicht.</param>
        /// <param name="type">Art des Symbols. -1 für Bombe, 0 für Flagge, andere für Zahl</param>
        /// <returns></returns>
        private Grid GetIcon(bool facingRight, int type = 0)
        {
            Grid ret = new Grid();
            if (!facingRight)
                ret.Margin = new Thickness(8, 0, 0, 0); //Rechts, Links Margin
            if (type < 0)
            {
                ret.Children.Add(new Ellipse
                {
                    Width = 10,
                    Height = 10,
                    Margin = new Thickness(6, 0, 0, 0),
                    Fill = Brushes.Black,
                    HorizontalAlignment = HorizontalAlignment.Left
                });
                ret.Children.Add(new Rectangle
                {
                    Width = 1,
                    Height = 14,
                    Margin = new Thickness(10.5, 0, 0, 0),
                    Fill = Brushes.Black,
                    HorizontalAlignment = HorizontalAlignment.Left
                });
                ret.Children.Add(new Rectangle
                {
                    Width = 14,
                    Height = 1,
                    Margin = new Thickness(4, 0, 0, 0),
                    Fill = Brushes.Black,
                    HorizontalAlignment = HorizontalAlignment.Left
                });
                //<Ellipse Width="10" Height="10" Margin="6 0 0 0" Fill="Black" HorizontalAlignment="Left"/>
                //<Rectangle Width="1" Height="14" Margin="10.5 0 0 0" Fill="Black" HorizontalAlignment="Left"/>
                //<Rectangle Width="14" Height="1" Margin="4 0 0 0" Fill="Black" HorizontalAlignment="Left"/>
            }
            else if (type == 0)
            {
                ret.Children.Add(new Rectangle {
                    Width = 1,
                    Height = 15,
                    Margin = new Thickness(8, 0, 0, 0),
                    Fill = Brushes.Black,
                    HorizontalAlignment = HorizontalAlignment.Left
                });
                ret.Children.Add(new Polygon {
                    Points = PointCollection.Parse("9,14 18,18 9,22"),
                    Fill = Brushes.Red
                });
                //<Rectangle Width="1" Height="15" Margin="8 0 0 0" Fill="Black" HorizontalAlignment="Left"/>
                //<Polygon Points="18 18, 9 14, 9 22" Fill="Red"/>
            }
            else
            {
                ret.Children.Add(new TextBlock
                {
                    Margin = new Thickness(8, 0, 0, 0),
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Text = type.ToString()
                });
                //<TextBlock VerticalAlignment="Center" Margin="8 0 0 0" Text="Zahl"/>
            }
            return ret;
        }

#region Events
        private void Polygon_Uncover(object sender, MouseButtonEventArgs e)
        {
            Polygon_Uncover(sender as Polygon);
        }
        private void Polygon_Uncover(Polygon p)
        {
            if ((State)p.Resources["state"] == State.Hidden)
            {
                p.Fill = Brushes.Gray;
                if ((bool)p.Resources["isBomb"])
                {
                    (p.Parent as Grid).Children.Add(GetIcon(IsRightPoly(p), -1));
                    GameOver();
                }
                else
                {
                    p.Resources["state"] = State.Uncovered;
                    List<Polygon> nearbyPolys = NearbyPolys(p);

                    //Bomben zählen
                    int count = PolyBombCount(nearbyPolys);
                    /*int count = 0;
                    foreach (Polygon p in nearbyPolys)
                    {
                        if ((bool)p.Resources["isBomb"])
                        {
                            count++;
                        }
                    }*/

                    if (count == 0) //umliegende Aufdecken
                        nearbyPolys.ForEach(x => Polygon_Uncover(x, e));
                    else   //Bobenanzahl einfügen
                        (p.Parent as Grid).Children.Add(GetIcon(IsRightPoly(p), count));

                    p.MouseLeftButtonDown -= Polygon_Uncover;
                    p.MouseRightButtonDown -= Polygon_Flag;
                }
            }
        }

        private void Polygon_Flag(object sender, MouseButtonEventArgs e)
        {
            Polygon p = sender as Polygon;
            switch ((State)p.Resources["state"])
            {
                case State.Hidden:
                    if (flags < bombs)
                    {
                        p.Resources["state"] = State.Flagged;
                        p.Fill = Brushes.Gray;
                        (p.Parent as Grid).Children.Add(GetIcon(IsRightPoly(p), 0));
                        flags++;
                        if ((bool)p.Resources["isBomb"])
                        {
                            successfulFlag++;
                        }
                        if (successfulFlag == bombs)
                        {
                            GameWon();
                        }
                    }
                    break;
                case State.Flagged:
                    p.Resources["state"] = State.Hidden;
                    p.Fill = Brushes.White;
                    (p.Parent as Grid).Children.Remove((p.Parent as Grid).Children.OfType<Grid>().First());
                    flags--;
                    if ((bool)p.Resources["isBomb"])
                    {
                        successfulFlag--;
                    }
                    break;
            }
            labelBombs.Content = bombs - flags;
        }

        private void Polygon_Highlight(object sender, MouseEventArgs e)
        {
            overlayPolys.ForEach(p =>
            {
                (p.Parent as Grid).Children.Remove(p);
            });
            overlayPolys = NearbyPolys(sender as Polygon).Select(p => {
                Polygon ret = new Polygon()
                {
                    Points = p.Points,
                    Fill = new SolidColorBrush(Color.FromArgb(50, 180, 180, 255)),
                    VerticalAlignment = VerticalAlignment.Center,
                    IsHitTestVisible = false
                };
                (p.Parent as Grid).Children.Add(ret);
                return ret;
            }).ToList();
        }
#endregion

        /// <summary>
        /// Gibt eine Liste mit anliegenden Polygonen zurück
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private List<Polygon> NearbyPolys(Polygon p)
        {
            int senderX = Grid.GetColumn((Grid)p.Parent), senderY = Grid.GetRow((Grid)p.Parent);
            (int Left, int Right, int TopCone) border = IsRightPoly(p) ? (Left: 3, Right: 1, TopCone: 2) : (Left: 1, Right: 3, TopCone: -2);

            List<Polygon> ret = new List<Polygon>();
            for (int y = -border.Left; y <= border.Left; y++)
            {
                if (fields.ContainsKey((X: senderX - 1, Y: senderY + y)))
                    ret.Add(fields[(X: senderX - 1, Y: senderY + y)].Children.OfType<Polygon>().First());
            }
            for (int y = -2; y <= 2; y++)
            {
                if (y == 0)
                    continue;
                if (fields.ContainsKey((X: senderX, Y: senderY + y)))
                    ret.Add(fields[(X: senderX, Y: senderY + y)].Children.OfType<Polygon>().First());
            }
            for (int y = -border.Right; y <= border.Right; y++)
            {
                if (fields.ContainsKey((X: senderX + 1, Y: senderY + y)))
                    ret.Add(fields[(X: senderX + 1, Y: senderY + y)].Children.OfType<Polygon>().First());
            }
            if (fields.ContainsKey((X: senderX + border.TopCone, Y: senderY)))
                ret.Add(fields[(X: senderX + border.TopCone, Y: senderY)].Children.OfType<Polygon>().First());

            // DEBUG
            //ret.ForEach(p => p.Fill = Brushes.Red);

            return ret;

            // Pre Dictionary
            /*return Playfield.Children.Cast<Grid>().Where((i) =>
                (i.Children.OfType<Polygon>().First().Resources["isBomb"] != null) &&                                                           //Prüfen, ob gültiges Feld
                (Grid.GetColumn(i) != senderX && Grid.GetRow(i) != senderY) &&                                                                  //Exclude Sender
                ((Grid.GetColumn(i) == senderX - 1) && (Grid.GetRow(i) >= senderY - border.Left && Grid.GetRow(i) <= senderY + border.Left) ||  //Left Column
                (Grid.GetColumn(i) == senderX) && (Grid.GetRow(i) >= senderY - 2 && Grid.GetRow(i) <= senderY + 2) ||                           //Center Column
                (Grid.GetColumn(i) == senderX + 1) && (Grid.GetRow(i) >= senderY - border.Right && Grid.GetRow(i) <= senderY + border.Right))   //Right Column
                ).SelectMany(x => x.Children.OfType<Polygon>()).ToList();*/
        }
        
        /// <summary>
        /// Gibt die Anzahl von Bomben in einer Poly-List zurück.
        /// </summary>
        /// <param name="polyList"></param>
        /// <returns></returns>
        private int PolyBombCount(List<Polygon> polyList) => polyList.Sum(p => (bool)p.Resources["isBomb"] ? 1 : 0);

        private void HeadStart()
        {
            //TODO
        }

        private void Reset()
        {
            bombs = 0;
            flags = 0;
            successfulFlag = 0;
            Playfield.Children.Clear();
            fields = GenField(Playfield);

            labelBombs.Content = bombs - flags;
        }

        private void GameOver()
        {
            MessageBox.Show("Game Over");
            Reset();
        }

        private void GameWon()
        {
            MessageBox.Show("Bomben entschärft!");
            Reset();
        }
    }
}
