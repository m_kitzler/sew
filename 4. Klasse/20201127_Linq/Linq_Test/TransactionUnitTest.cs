using NUnit.Framework;
using sew_linq.data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace sew_linq.unittest
{
    public class TransactionUnitTest
    {
        [SetUp]
        public void Setup()
        {
        }

        /*
         * 2.1) Beispiel: Geben Sie fuer jeden Trader den Wert der Summe
         *      seiner Transaktionen an. Geben Sie den Namen des Tader
         *      und die Summe aus. Ordnen Sie das Ergebnis nach dem
         *      Namen des Traders.
         * 
         */
        [Test]
        public void GroupByTrader()
        {
            List<Transaction> transactions = DataRepository.CreateTransactions();
            List<Trader> traders = DataRepository.CreateTrader();

            var query = from td in traders
                        join tn in (from tn1 in transactions group tn1 by tn1.Trader into traderGroup select new { Trader = traderGroup.Key, TrSum = traderGroup.Sum(t => t.Value) })
                        on td equals tn.Trader
                        orderby td.Name
                        select new { td.Name, Value = tn.TrSum };

            foreach (var trader in query)
            {
                Console.WriteLine($"{trader.Name} {trader.Value}");
            }

            Assert.AreEqual(6, query.ToList().Count);
        }

        /*
         * 2.2) Beispiel: Geben Sie fuer jeden Trader den Wert der Summe
         *      seiner Transaktionen an. Geben Sie den Namen des Tader
         *      und die Summe aus. Ordnen Sie das Ergebnis nach dem
         *      Namen des Traders.
         * 
         */
        [Test]
        public void GroupTransaction()
        {
            List<Transaction> transactions = DataRepository.CreateTransactions();
            List<Trader> traders = DataRepository.CreateTrader();

            var query = from tn1 in transactions
                        group tn1 by tn1.Trader into traderGroup
                        select new { Name = traderGroup.Key.Name, Value = traderGroup.Sum(t => t.Value) };

            foreach (var trader in query)
            {
                Console.WriteLine($"{trader.Name} {trader.Value}");
            }

            Assert.AreEqual(5, query.ToList().Count);
            //???
        }

        /*
         * 2.3) Beispiel: Geben Sie fuer jeden Trader den Wert der Summe
         *      seiner Transaktionen an. Geben Sie den Namen des Tader
         *      und die Summe aus. Berücksichtigen Sie nur Trader die in
         *      Summe mehr als 10000 veranlagt haben. Ordnen Sie das Ergebnis
         *      nach dem Namen des Traders.
         * 
         */
        [Test]
        public void GroupTransactionsHaving()
        {
            List<Transaction> transactions = DataRepository.CreateTransactions();
            List<Trader> traders = DataRepository.CreateTrader();

            var query = from td in traders
                        join tn in (from tn1 in transactions group tn1 by tn1.Trader into trGroup select new { Trader = trGroup.Key, TrSum = trGroup.Sum(t => t.Value) })
                        on td equals tn.Trader
                        where tn.TrSum > 10000
                        orderby td.Name
                        select new { td.Name, Value = tn.TrSum };

            foreach (var trader in query)
            {
                Console.WriteLine($"{trader.Name} {trader.Value}");
            }

            Assert.AreEqual(5, query.ToList().Count);
        }

        /*
         * 2.4) Beispiel: Wieviele Trader gibt es deren Name mit einem A bzw. P
         *      beginnnen. Geben Sie jeweils die Anzahl der Trader aus einen
         *      entsprechenden Namensanfangsbuchstaben haben.
         */
        [Test]
        public void GroupTraderByName()
        {
            List<Trader> traders = DataRepository.CreateTrader();

            var query = from t in traders
                        where t.Name.StartsWith('A') || t.Name.StartsWith('P')
                        group t by t.Name[0] into tGroup
                        select new { NameCat = tGroup.Key, TraderAmount = tGroup.Count() };

            foreach (var tradeGroup in query)
            {
                Console.WriteLine($"{tradeGroup.NameCat} {tradeGroup.TraderAmount}");
            }

            Assert.AreEqual(2, query.ToList().Count);
        }

    }
}
