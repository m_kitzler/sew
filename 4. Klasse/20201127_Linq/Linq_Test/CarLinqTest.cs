﻿using NUnit.Framework;
using sew_linq.data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq_Test
{
    class CarLinqTest
    {
        [Test]
        public void FilterCarByBrand()
        {
            List<Car> cars = DataRepository.CreateCars();
            var query = from c in cars
                        where c.Brand.Equals(ECarBrand.VW)
                        select c.Model;
            foreach (var car in query)
            {
                Console.WriteLine(car);
            }

            Assert.AreEqual(query.Count(), 2);

            var query1 = from c in cars
                         where c.Ps < 100
                         orderby c.Model
                         select c;
        }
    }
}
