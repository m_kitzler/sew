using System;

namespace sew_linq.data
{
    public class Transaction
    {

        private readonly Trader _trader;

        private readonly int _year;

        private readonly int _value;

        public Transaction(Trader trader, int year, int value)
        {
            _trader = trader;
            _year = year;
            _value = value;
        }

        public Trader Trader => _trader;

        public int Year => _year;

        public int Value => _value;

        protected bool Equals(Transaction other)
        {
            return Equals(_trader, other._trader) && _year == other._year && _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Transaction)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_trader, _year, _value);
        }
    }
}