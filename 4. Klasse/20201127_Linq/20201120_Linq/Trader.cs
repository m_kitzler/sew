using System;

namespace sew_linq.data
{
    public class Trader
    {

        private readonly string _name;

        private readonly ECities _city;

        public Trader(string name, ECities city)
        {
            _name = name;
            _city = city;
        }

        public string Name => _name;

        public ECities City => _city;

        protected bool Equals(Trader other)
        {
            return _name == other._name && _city == other._city;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Trader)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_name, (int)_city);
        }
    }
}