﻿using System.Collections.Generic;

namespace sew_linq.data
{
    public enum ECarBrand
    {
        Mercedes, VW, BMW, Kia, Tesla, Toyota
    }

    public enum EEngineType
    {
        Electric, Diesel, Gasoline
    }

    public class Car
    {
        private string _model;
        private ECarBrand _brand;
        private EEngineType _engine;
        private int _ps;
        private string _color;
        private List<string> _accessories = new List<string>();

        public Car(string model, ECarBrand brand, EEngineType engine, int ps, string color, List<string> accessories)
        {
            _model = model;
            _brand = brand;
            _engine = engine;
            _ps = ps;
            _color = color;
            _accessories = accessories;
        }

        public string Model { get => _model; set => _model = value; }
        public ECarBrand Brand { get => _brand; set => _brand = value; }
        public EEngineType Engine { get => _engine; set => _engine = value; }
        public int Ps { get => _ps; set => _ps = value; }
        public string Color { get => _color; set => _color = value; }
        public List<string> Accessories { get => _accessories; set => _accessories = value; }
    }
}
