﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace P20210211_XMLFileIndexer
{
    class Program
    {
        static XDocument doc;
        static void Main(string[] args)
        {
            string root = @"C:\Users\Miau\Git\!SEW\4. Klasse";
            doc = new XDocument(DirToXE(root));
            Console.WriteLine(doc);
            doc.Save(@"C:\Users\Miau\Desktop\XML.xml");
        }

        static XElement DirToXE(string path)
        {
            #region OldOld
            //XElement ret = new XElement(GetValidString("_" + Path.GetFileName(path)));

            //string[] dirs = Directory.GetDirectories(path);
            //foreach (string d in dirs)
            //{
            //    ret.Add(DirToXE(d));
            //}

            //string[] files = Directory.GetFiles(path);
            //foreach (string f in files)
            //{
            //    ret.Add(new XElement(GetValidString("_" + Path.GetFileName(f))));
            //}
            #endregion
            #region Old
            //return new XElement(GetValidString("_" + Path.GetFileName(path)), (from d in Directory.GetDirectories(path) select DirToXE(d)).Union(from f in Directory.GetFiles(path) select new XElement(GetValidString("_" + Path.GetFileName(f)))));
            #endregion

            XElement ret = new XElement("folder");
            ret.SetAttributeValue("name", Path.GetFileName(path));

            string[] dirs = Directory.GetDirectories(path);
            foreach (string d in dirs)
            {
                ret.Add(DirToXE(d));
            }

            string[] files = Directory.GetFiles(path);
            foreach (string f in files)
            {
                XElement file = new XElement("file");
                file.SetAttributeValue("name", Path.GetFileName(f));
                ret.Add(file);
            }

            return ret;
        }

        static string GetValidString(string illegal)
        {
            illegal = illegal.Replace(' ', '_');
            return Regex.Replace(illegal, @"[!,=+\-()]*", "");
        }
    }
}
