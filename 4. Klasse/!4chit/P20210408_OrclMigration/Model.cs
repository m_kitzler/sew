﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace P20210408_OrclMigration
{
    public partial class SchoolContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                //optionsBuilder.UseOracle("User Id=scott;Password=oracle;Data Source=192.168.127.128:40001/orcl;");
                options.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\Miau\Documents\Schule.mdf"";Integrated Security=True;Connect Timeout=30");
                options.LogTo((text) => Console.WriteLine($"\n---------DB-Log---------\n{text}\n------------------------\n"), LogLevel.Information);
            }
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<SchoolClass> Classes { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public class Student
        {
            /*public Student()
            {
                Subjects = new HashSet<Subject>();
            }*/

            public int StudentId { get; set; }
            public int CatalogNumber { get; set; }
            public SchoolClass SchoolClass { get; set; }

            public List<Subject> Subjects { get; set; }
        }
        public class SchoolClass
        {
            /*public SchoolClass()
            {
                Teachers = new HashSet<Teacher>();
            }*/

            [Key]
            public int ClassId { get; set; }
            public string Label { get; set; }

            public Teacher ClassBoard { get; set; }

            public List<Teacher> Teachers { get; set; }
        }
        public class Teacher
        {
            /*public Teacher()
            {
                Subjects = new HashSet<Subject>();
                Classes = new HashSet<SchoolClass>();
            }*/

            public int TeacherId { get; set; }
            public string Abbreviation { get; set; }

            public List<SchoolClass> ClassBoards { get; set; }

            public List<Subject> Subjects { get; set; }
            public List<SchoolClass> Classes { get; set; }
        }
        public class Subject
        {
            /*public Subject()
            {
                Students = new HashSet<Student>();
                Teachers = new HashSet<Teacher>();
            }*/

            public int SubjectId { get; set; }

            public List<Student> Students { get; set; }
            public List<Teacher> Teachers { get; set; }
        }
        public class Department
        {
            public List<SchoolClass> Classes { get; set; }
            public Teacher Board { get; set; }
        }
    }
}
