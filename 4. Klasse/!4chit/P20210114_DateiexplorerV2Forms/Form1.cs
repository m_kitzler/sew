﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;

namespace P20210114_DateiexplorerV2Forms
{
    public partial class Form1 : Form
    {
        private enum Direction
        {
            Last,
            Next,
            New
        }
        private List<FileFolderInfo> lastDirs;
        private List<FileFolderInfo> nextDirs;
        private Thread listViewHelper;
        private Thread detailsBoxHelper;

        public Form1()
        {
            InitializeComponent();

            lastDirs = new List<FileFolderInfo>();
            nextDirs = new List<FileFolderInfo>();

            icons.Images.Add(new Icon(@"..\..\icons\shell32_4.ico"));
            listViewFilesNFolders.SmallImageList = icons;
            listViewFilesNFolders.LargeImageList = icons;

            InitBaseDirs();
        }

        private void InitBaseDirs()
        {
            TreeNode thispc = new TreeNode("Dieser PC")
            {
                Name = "NodeThisPC",
                Tag = new FileFolderInfo("thisPC", true)
                {
                    AlternativeName = "Dieser PC"
                }
            };
            treeViewDirs.Nodes.Add(thispc);
            foreach (DriveInfo d in DriveInfo.GetDrives())
            {
                if (d.IsReady)
                {
                    TreeNode drive = new TreeNode(d.VolumeLabel + " (" + d.Name + ")");
                    drive.Name = "Node" + d.Name;
                    drive.Tag = new FileFolderInfo(d.Name, true);
                    drive.Nodes.Add("placeholder");
                    treeViewDirs.Nodes.Find("NodeThisPC", false)[0].Nodes.Add(drive);
                }
            }

            // Temp
            TreeNode ex = new TreeNode("Beispiele")
            {
                Name = "NodeExamples",
                Tag = new FileFolderInfo(Environment.ExpandEnvironmentVariables(@"%userprofile%\Desktop\_Git\!SEW\4. Klasse\!4chit\P20210114_DateiexplorerV2Forms\Beispiele"), true)
            };
            treeViewDirs.Nodes.Add(ex);

            TreeNode re = new TreeNode("remote")
            {
                Name = "NodeRemote",
                Tag = new FileFolderInfo(".", true)
                {
                    IsRemote = true
                }
            };
            re.Nodes.Add("placeholder");
            treeViewDirs.Nodes.Add(re);

            ListViewChangeDir(Direction.New, new FileFolderInfo("thisPC", true) {
                AlternativeName = "Dieser PC"
            });
        }

        /// <summary>
        /// Prüft, ob für Control c ein Invoke notwendig ist und führt Action a aus.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="a"></param>
        private void SaveExec(Control c, Action a)
        {
            if (c.InvokeRequired)
                Invoke(a);
            else
                a.Invoke();
        }

        /// <summary>
        /// Erweitert ein TreeNode um seine Unterordner.
        /// </summary>
        /// <param name="source"></param>
        private void TreeNodeExpand(TreeNode source)
        {
            Task.Run(() => {
                FileFolderInfo info = (FileFolderInfo)source.Tag;

                Invoke(new Action(() => source.Nodes.Clear()));
                if (info.IsRemote)
                {
                    using (TcpClient server = new TcpClient())
                    {
                        try
                        {
                            server.Connect("localhost", 9999);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                        if (server.Connected)
                        {
                            using (StreamWriter sw = new StreamWriter(server.GetStream()))
                            {
                                sw.AutoFlush = true;
                                sw.WriteLine("f:" + info.Path);
                                using (StreamReader sr = new StreamReader(server.GetStream()))
                                {
                                    string line = sr.ReadLine();
                                    while (line != "end"/*sr.Peek() != -1*/)
                                    {
                                        FileFolderInfo data = JsonSerializer.Deserialize<FileFolderInfo>(line);
                                        TreeNode directory = new TreeNode(data.Name);
                                        //directory.Name = "Node" + d.FullName;
                                        directory.Tag = data;
                                        if (data.IsExpandable)
                                        {
                                            directory.Nodes.Add("placeholder");
                                        }
                                        Invoke(new Action(() => source.Nodes.Add(directory)));
                                        line = sr.ReadLine();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (string dirPath in Directory.GetDirectories(info.Path))
                    {
                        DirectoryInfo d = new DirectoryInfo(dirPath);
                        TreeNode directory = new TreeNode(d.Name);
                        //directory.Name = "Node" + d.FullName;
                        directory.Tag = new FileFolderInfo(d.FullName, true);
                        try
                        {
                            if (d.GetDirectories().Length > 0)  //Peek, ob Node expandable sein soll
                            {
                                directory.Nodes.Add("placeholder");
                            }
                        }
                        catch (UnauthorizedAccessException)
                        {
                            directory.Text += " (kein Zugriff)";
                        }
                        Invoke(new Action(() => source.Nodes.Add(directory)));
                    }
                }
            });
        }

        /// <summary>
        /// Ändert den angezeigten Pfad in der ListView.
        /// Falls der Pfad auf eine Datei zeigt, wird diese Ausgewählt.
        /// </summary>
        /// <param name="direction">Gibt an, ob der vorherige, nächste oder eine neuer Pfad gewählt werden soll.</param>
        /// <param name="path">Der neue Pfad, falls "direction" auf "New" ist.</param>
        private void ListViewChangeDir(Direction direction, FileFolderInfo info = null)
        {
            if (listViewHelper != null && listViewHelper.IsAlive)
                listViewHelper.Abort();
            listViewHelper = new Thread(() =>
            {
                //Ungültigen Aufruf ignorieren
                if (direction == Direction.New && info == null)
                    return;

                #region Verlauf setzen
                switch (direction)
                {
                    case Direction.New:
                        lastDirs.Add(info);
                        nextDirs.Clear();
                        break;
                    case Direction.Last:
                        if (lastDirs.Count > 0)
                        {
                            nextDirs.Add((FileFolderInfo)listViewFilesNFolders.Tag);
                            info = lastDirs[lastDirs.Count - 1];
                            lastDirs.RemoveAt(lastDirs.Count - 1);
                        }
                        else
                            return;
                        break;
                    case Direction.Next:
                        if (nextDirs.Count > 0)
                        {
                            lastDirs.Add((FileFolderInfo)listViewFilesNFolders.Tag);
                            info = nextDirs[nextDirs.Count - 1];
                            nextDirs.RemoveAt(nextDirs.Count - 1);
                        }
                        else
                            return;
                        break;
                }
                #endregion

                SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Items.Clear())); //Clear
                SaveExec(richTextBoxDetails, new Action(() => DetailsChangeFile(null)));    //Clear Detailbox

                SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Tag = info));    //aktiven Ordner setzen
                SaveExec(textBoxPath, new Action(() => textBoxPath.Text = info.AlternativeName ?? info.Path));    //Pfad in TextBox aktualisieren


                //Pfad anpassen, wenn er auf Datei zeigt
                string path = info.Path, filePath = null;
                if (!info.IsFolder)
                {
                    filePath = path;
                    path = Path.GetDirectoryName(path);
                }

                //Ausnahmefall bei "Dieser PC" Seite
                if (path == "thisPC")   //TODO: Festplatten anzeigen
                    return;

                #region Ordnerinhalt anzeigen
                if (info.IsRemote)  //Todo
                {
                    using (TcpClient server = new TcpClient())
                    {
                        try
                        {
                            server.Connect("localhost", 9999);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                        if (server.Connected)
                        {
                            using (StreamWriter sw = new StreamWriter(server.GetStream()))
                            {
                                sw.AutoFlush = true;
                                sw.WriteLine("ff:" + info.Path);
                                using (StreamReader sr = new StreamReader(server.GetStream()))
                                {
                                    string line = sr.ReadLine();
                                    while (line != "end"/*sr.Peek() != -1*/)
                                    {
                                        FileFolderInfo data = JsonSerializer.Deserialize<FileFolderInfo>(line);

                                        ListViewItem f = new ListViewItem(new string[]{
                                            data.Name,
                                            data.LastWrite.ToString("g"),
                                            data.IsFolder ? "Dateiordner" : Path.GetExtension(data.Path),
                                            data?.FileSize ?? ""
                                        });
                                        f.Tag = data;
                                        if (data.IsFolder)
                                            f.ImageIndex = 0;

                                        SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Items.Add(f)));

                                        line = sr.ReadLine();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        List<string> folders = new List<string>(Directory.GetDirectories(path));
                        if (folders.Count > 0)
                        {
                            foreach (string folder in folders)
                            {
                                // 0: Name, 1: Änderungsdatum, 2: Typ, 3: Größe
                                ListViewItem f = new ListViewItem(new string[]{
                                    Path.GetFileName(folder),
                                    new DirectoryInfo(folder).LastWriteTime.ToString("g"),
                                    "Dateiordner",
                                    ""
                                });
                                f.Tag = new FileFolderInfo(folder, true);

                                f.ImageIndex = 0;
                                SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Items.Add(f)));
                            }
                        }
                        List<string> files = new List<string>(Directory.GetFiles(path));
                        if (files.Count > 0)
                        {
                            for (int i = 0; i < files.Count; i++)
                            {
                                ListViewItem f = new ListViewItem(new string[]{
                                    Path.GetFileName(files[i]),
                                    new FileInfo(files[i]).LastWriteTime.ToString("g"),
                                    Path.GetExtension(files[i]),
                                    GetFileSize(files[i])
                                });
                                f.Tag = new FileFolderInfo(files[i], false);

                                //f.Name = "Item" + files[i];

                                SaveExec(listViewFilesNFolders, new Action(() => icons.Images.Add(Icon.ExtractAssociatedIcon(files[i]))));
                                f.ImageIndex = i + 1;

                                SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Items.Add(f)));
                            }
                        }
                    }
                    catch (UnauthorizedAccessException)
                    {
                        ListViewItem noAccess = new ListViewItem(new string[] {
                        "Kein Zugriff",
                        "",
                        "",
                        "",
                    });
                        noAccess.Tag = new FileFolderInfo(null, false);
                        SaveExec(listViewFilesNFolders, new Action(() => listViewFilesNFolders.Items.Add(noAccess)));
                    }
                }
                #endregion

                //wenn auf Datei gezeigt wird, diese markieren
                if (filePath != null)
                {   //Bsp: C:\Intel\Logs\IntelICCS.log
                    SaveExec(listViewFilesNFolders, new Action(() =>
                    {
                        listViewFilesNFolders.Focus();
                        listViewFilesNFolders.Items.Find("Item" + filePath, false)[0].Selected = true;
                    }));
                }
            });
            listViewHelper.Start();
        }

        private void ListViewChangeDir(Direction direction, string path)
        {
            ListViewChangeDir(direction, new FileFolderInfo(path, !File.Exists(path)));
        }

        /// <summary>
        /// Gibt die größe einer Datei als string mit B, KB, MB, oder GB zurück.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GetFileSize(string path)
        {
            FileInfo file = new FileInfo(path);
            string hlp = "B";
            double hlp1 = file.Length;
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "KB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "MB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "GB";
            }
            return Math.Round(hlp1, 2).ToString() + " " + hlp;
        }

        /// <summary>
        /// Ändert das Details-Fenster
        /// </summary>
        /// <param name="file"></param>
        private void DetailsChangeFile(FileFolderInfo file)
        {
            if (detailsBoxHelper != null && detailsBoxHelper.IsAlive)
                detailsBoxHelper.Abort();
            detailsBoxHelper = new Thread(() =>
            {
                SaveExec(richTextBoxDetails, () => richTextBoxDetails.Hide());
                SaveExec(pictureBoxDetails, () => pictureBoxDetails.Hide());
                if (file != null && !file.IsFolder)
                {
                    if (file.IsRemote)
                    {
                        using (TcpClient server = new TcpClient())
                        {
                            try
                            {
                                server.Connect("localhost", 9999);
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                            }
                            if (server.Connected)
                            {
                                using (StreamWriter sw = new StreamWriter(server.GetStream()))
                                {
                                    sw.AutoFlush = true;
                                    sw.WriteLine("fi:" + file.Path);
                                    SaveExec(richTextBoxDetails, () => richTextBoxDetails.LoadFile(server.GetStream(), RichTextBoxStreamType.PlainText));
                                    SaveExec(richTextBoxDetails, () => richTextBoxDetails.Show());
                                }
                            }
                        }
                    }
                    else
                    {
                        if (File.Exists(file.Path))
                        {
                            string[] picExt = { ".jpg", ".png" };
                            if (picExt.Contains(Path.GetExtension(file.Path)))
                            {
                                SaveExec(pictureBoxDetails, () => pictureBoxDetails.Image = Image.FromFile(file.Path));
                                SaveExec(pictureBoxDetails, () => pictureBoxDetails.Show());
                            }
                            else if (Path.GetExtension(file.Path) == ".rtf")
                            {
                                SaveExec(richTextBoxDetails, () => richTextBoxDetails.LoadFile(file.Path));
                                SaveExec(richTextBoxDetails, () => richTextBoxDetails.Show());
                            }
                            else
                            {
                                SaveExec(richTextBoxDetails, () => richTextBoxDetails.Text = File.ReadAllText(file.Path));
                                SaveExec(richTextBoxDetails, () => richTextBoxDetails.Show());
                            }
                        }
                        else
                        {
                            throw new FileNotFoundException("Diese Datei existiert nicht.", file.Path);
                        }
                    }
                }
            });
            detailsBoxHelper.Start();
        }

        // -------------------- Events --------------------

        #region MenuStrip
        private void LadenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogBrowseFiles.ShowDialog() == DialogResult.OK)
            {
                ListViewChangeDir(Direction.New, folderBrowserDialogBrowseFiles.SelectedPath);
            }
        }

        private void DetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem[] viewMenuItems = new ToolStripMenuItem[] { detailToolStripMenuItem, largeIconToolStripMenuItem };
            if (!detailToolStripMenuItem.Checked)
            {
                foreach (ToolStripMenuItem t in viewMenuItems)
                {
                    t.Checked = false;
                }
                detailToolStripMenuItem.Checked = true;
            }
            listViewFilesNFolders.View = View.Details;
        }

        private void LargeIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem[] viewMenuItems = new ToolStripMenuItem[] { detailToolStripMenuItem, largeIconToolStripMenuItem };
            if (!largeIconToolStripMenuItem.Checked)
            {
                foreach (ToolStripMenuItem t in viewMenuItems)
                {
                    t.Checked = false;
                }
                largeIconToolStripMenuItem.Checked = true;
            }
            listViewFilesNFolders.View = View.LargeIcon;
        }
        #endregion

        #region TreeView
        private void treeViewDirs_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FileFolderInfo info = (FileFolderInfo)e.Node.Tag;
            if (info.Path != "thisPC")
                ListViewChangeDir(Direction.New, info);
        }

        private void treeViewDirs_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if ((e.Node.Tag as FileFolderInfo).Path != "thisPC")
            {
                treeViewDirs.BeginUpdate();
                TreeNodeExpand(e.Node);
                treeViewDirs.EndUpdate();
            }
        }
        #endregion

        #region ListView
        private void ListViewFilesNFolders_DoubleClick(object sender, EventArgs e)
        {
            // 0: Name, 1: Änderungsdatum, 2: Typ, 3: Größe
            if (listViewFilesNFolders.SelectedItems.Count > 0)
            {
                ListViewItem selectedItem = listViewFilesNFolders.SelectedItems[0];
                FileFolderInfo info = (FileFolderInfo)selectedItem.Tag;
                if (info.IsFolder)
                {
                    ListViewChangeDir(Direction.New, info);
                }
                else
                {
                    Process.Start(info.Path);
                }
            }
        }

        private void buttonChangeDir_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(textBoxPath.Text))
            {
                ListViewChangeDir(Direction.New, textBoxPath.Text);
            }
            else if (File.Exists(textBoxPath.Text))
            {
                ListViewChangeDir(Direction.New, textBoxPath.Text);
            }
            else
            {
                MessageBox.Show("Dieser Ordner existiert nicht!");
            }
        }

        private void listViewFilesNFolders_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.XButton1:
                    ListViewChangeDir(Direction.Last);
                    break;
                case MouseButtons.XButton2:
                    ListViewChangeDir(Direction.Next);
                    break;
            }
        }

        private void listViewFilesNFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewFilesNFolders.SelectedItems.Count > 0)
            {
                ListViewItem selectedItem = listViewFilesNFolders.SelectedItems[0];
                DetailsChangeFile((FileFolderInfo)selectedItem.Tag);
            }
        }
        #endregion
    }
}
