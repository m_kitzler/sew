﻿using System;

namespace P20210114_DateiexplorerV2Forms
{
    public class FileFolderInfo
    {
        public string Path { get; }
        public bool IsFolder { get; }
        public string Name { get; set; }
        public string AlternativeName { get; set; }
        public bool IsRemote { get; set; }
        public bool IsExpandable { get; set; }
        public DateTime LastWrite { get; set; }
        public string FileSize { get; set; }

        public FileFolderInfo(string path, bool isFolder)
        {
            Path = path;
            IsFolder = isFolder;
        }
    }
}
