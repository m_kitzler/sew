﻿using System;
using System.Collections.Generic;

namespace Fabrikmethode
{
    #region Mahlzeiten
    class Mahlzeit { };

    class PizzaQuadro : Mahlzeit
    {
        public static string name = "Pizza Quadro";
        public PizzaQuadro()
        {
            Console.WriteLine(name + " gebacken.");
        }
    }

    class PizzaFungi : Mahlzeit
    {
        public static string name = "Pizza Fungi";
        public PizzaFungi()
        {
            Console.WriteLine(name + " gebacken.");
        }
    }

    class PizzaKriegsverbrechen : Mahlzeit
    {
        public static string name = "Pizza Hawaii";
        public PizzaKriegsverbrechen()
        {
            Console.WriteLine(name + " gebacken.");
        }
    }

    class Brotwiaschtl : Mahlzeit
    {
        public Brotwiaschtl(string Beilage = "")
        {
            Console.WriteLine("Brotwiaschtl fiade.");
            if (!String.IsNullOrEmpty(Beilage))
            {
                Console.WriteLine("Serviert mit {0}", Beilage);
            }
        }
    }

    class Oregano : Mahlzeit
    {
        public static string name = "\"Oregano\"";
        public Oregano()
        {
            Console.WriteLine(name + " ist fertig.");
        }
    }

    class KaffeSchwarz : Mahlzeit
    {
        public static string name = "Kaffe schwarz";
        public KaffeSchwarz()
        {
            Console.WriteLine(name + " ist fertig.");
        }
    }

    class KaffeLatte : Mahlzeit
    {
        public static string name = "Kaffelatte";
        public KaffeLatte()
        {
            Console.WriteLine(name + " ist fertig.");
        }
    }
    #endregion
    #region Restautants
    abstract class Restaurant
    {
        protected Mahlzeit mahlzeit;
        protected List<Type> mahlzeiten;

        protected abstract void MahlzeitZubereiten();
        protected void BestellungAufnehmen()
        {
            Console.WriteLine("Ihre Bestellung bitte!");
        }
        protected void MahlzeitServieren()
        {
            Console.WriteLine("Hier Ihre Mahlzeit. Guten Apetit!\n");
        }

        public void MahlzeitLiefern()
        {
            BestellungAufnehmen();
            MahlzeitZubereiten();
            MahlzeitServieren();
        }
    }

    class Pizzeria : Restaurant
    {
        public Pizzeria()
        {
            mahlzeiten = new List<Type>();
            mahlzeiten.Add(typeof(PizzaQuadro));
            mahlzeiten.Add(typeof(PizzaFungi));
            mahlzeiten.Add(typeof(PizzaKriegsverbrechen));
        }

        protected override void MahlzeitZubereiten()
        {
            bool accept = false;
            int index = 0;
            while (!accept)
            {
                Display.Menu(mahlzeiten, index);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.LeftArrow:
                        if (index == 0)
                            index = mahlzeiten.Count - 1;
                        else
                            index--;
                        break;
                    case ConsoleKey.RightArrow:
                        if (index == mahlzeiten.Count - 1)
                            index = 0;
                        else
                            index++;
                        break;
                    case ConsoleKey.Enter:
                        accept = true;
                        break;
                }
            }
            mahlzeit = (Mahlzeit)Activator.CreateInstance(mahlzeiten[index]);
        }
    }

    class Imbiss : Restaurant
    {
        protected override void MahlzeitZubereiten()
        {
            mahlzeit = new Brotwiaschtl("Pommes und Ketchup");
        }
    }

    class KaffehausAmsterdam : Restaurant
    {
        public KaffehausAmsterdam()
        {
            mahlzeiten = new List<Type>();
            mahlzeiten.Add(typeof(KaffeSchwarz));
            mahlzeiten.Add(typeof(KaffeLatte));
            mahlzeiten.Add(typeof(Oregano));
        }
        protected override void MahlzeitZubereiten()
        {
            bool accept = false;
            int index = 0;
            while (!accept)
            {
                Display.Menu(mahlzeiten, index);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.LeftArrow:
                        if (index == 0)
                            index = mahlzeiten.Count - 1;
                        else
                            index--;
                        break;
                    case ConsoleKey.RightArrow:
                        if (index == mahlzeiten.Count - 1)
                            index = 0;
                        else
                            index++;
                        break;
                    case ConsoleKey.Enter:
                        accept = true;
                        break;
                }
            }
            mahlzeit = (Mahlzeit)Activator.CreateInstance(mahlzeiten[index]);
        }
    }
    #endregion

    static class Display
    {
        public static void Menu(List<Type> menu, int index)
        {
            Console.Write("< " + menu[index].GetField("name").GetValue(null) + " >");
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("----------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorLeft = 0;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            Pizzeria daToni = new Pizzeria();
            daToni.MahlzeitLiefern();

            Imbiss Bruno = new Imbiss();
            Bruno.MahlzeitLiefern();

            KaffehausAmsterdam am = new KaffehausAmsterdam();
            am.MahlzeitLiefern();
        }
    }
}
