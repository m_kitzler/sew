﻿using System;

namespace Strategie
{
    class Sorter
    {
        private Strategy strategy;
        public Strategy strat
        {
            get
            {
                return strategy;
            }
            set
            {
                strategy = value;
            }
        }
        public void Sort()
        {
            strategy?.Algo();
        }
    }

    abstract class Strategy
    {
        public abstract void Algo();
    }

    class StratPogo : Strategy
    {
        public override void Algo()
        {
            Console.WriteLine("Pogosort");
        }
    }

    class StratBubble : Strategy
    {
        public override void Algo()
        {
            Console.WriteLine("Bubblesort");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Sorter s = new Sorter();
            s.Sort();
            s.strat = new StratPogo();
            s.Sort();
            s.strat = new StratBubble();
            s.Sort();
        }
    }
}
