﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P20210304_EF_Oracle
{
    class Program
    {
        // dotnet ef dbcontext scaffold "User Id=scott;Password=oracle;Data Source=192.168.127.128:40001/orcl;" Oracle.EntityFrameworkCore
        static void Main(string[] args)
        {
            using (ModelContext scott = new ModelContext())
            {
                Console.WriteLine("Emps:");
                foreach (var emp in scott.Emps)
                {
                    Console.WriteLine($"{emp.Empno}:{emp.Ename}");
                }

                Console.WriteLine("\nDeps:");
                foreach (var dep in scott.Depts)
                {
                    Console.WriteLine($"{dep.Deptno}:{dep.Dname}\t{dep.Loc}");
                }

                Console.WriteLine("\n");
                DateTime start = DateTime.Now;
                WriteEmps(scott);
                DateTime stop = DateTime.Now;
                Console.WriteLine(stop - start);

                // Mit Liste ist es effizienter (schneller und weniger Anfragen)
                Console.WriteLine();
                DateTime start2 = DateTime.Now;
                WriteEmps2(scott.Emps.ToList());
                DateTime stop2 = DateTime.Now;
                Console.WriteLine(stop2 - start2);

                Console.WriteLine("\n");
                foreach (var dep in scott.Depts)
                {
                    Console.WriteLine($"{dep.Deptno}:{dep.Dname}\t{dep.Loc}");

                    var query = from e in scott.Emps where e.Deptno == dep.Deptno select e;

                    foreach (var e in query)
                    {
                        Console.WriteLine($"\t{e.Empno}:{e.Ename}");
                    }
                }
            }
        }

        static void WriteEmps(ModelContext scott, int? id = null, string prefix = "")
        {
            var queryHasAsMgr = from e in scott.Emps where e.Mgr == id select e;
            foreach (var m in queryHasAsMgr)
            {
                Console.WriteLine(prefix + m.Empno + ":" + m.Ename);
                WriteEmps(scott, m.Empno, prefix + "\t");
            }
        }

        static void WriteEmps2(List<Emp> data, int? id = null, string prefix = "")
        {
            var queryHasAsMgr = from e in data where e.Mgr == id select e;
            foreach (var m in queryHasAsMgr)
            {
                Console.WriteLine(prefix + m.Empno + ":" + m.Ename);
                WriteEmps2(data, m.Empno, prefix + "\t");
            }
        }
    }
}
