﻿#nullable disable

namespace P20210304_EF_Oracle
{
    public partial class Bonu
    {
        public string Ename { get; set; }
        public string Job { get; set; }
        public decimal? Sal { get; set; }
        public decimal? Comm { get; set; }
    }
}
