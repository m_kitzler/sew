﻿#nullable disable

namespace P20210304_EF_Oracle
{
    public partial class Salgrade
    {
        public decimal? Grade { get; set; }
        public decimal? Losal { get; set; }
        public decimal? Hisal { get; set; }
    }
}
