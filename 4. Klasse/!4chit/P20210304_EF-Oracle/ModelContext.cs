﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

#nullable disable

namespace P20210304_EF_Oracle
{
    public partial class ModelContext : DbContext
    {
        public ModelContext()
        {
        }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bonu> Bonus { get; set; }
        public virtual DbSet<Dept> Depts { get; set; }
        public virtual DbSet<Emp> Emps { get; set; }
        public virtual DbSet<Salgrade> Salgrades { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseOracle("User Id=scott;Password=oracle;Data Source=192.168.127.128:40001/orcl;");
                optionsBuilder.LogTo((text) => Console.WriteLine($"\n---------DB-Log---------\n{text}\n------------------------\n"), LogLevel.Information);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("SCOTT");

            modelBuilder.Entity<Bonu>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BONUS");

                entity.Property(e => e.Comm)
                    .HasColumnType("NUMBER")
                    .HasColumnName("COMM");

                entity.Property(e => e.Ename)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ENAME");

                entity.Property(e => e.Job)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("JOB");

                entity.Property(e => e.Sal)
                    .HasColumnType("NUMBER")
                    .HasColumnName("SAL");
            });

            modelBuilder.Entity<Dept>(entity =>
            {
                entity.HasKey(e => e.Deptno);

                entity.ToTable("DEPT");

                entity.Property(e => e.Deptno)
                    .HasPrecision(2)
                    .HasColumnName("DEPTNO");

                entity.Property(e => e.Dname)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .HasColumnName("DNAME");

                entity.Property(e => e.Loc)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasColumnName("LOC");
            });

            modelBuilder.Entity<Emp>(entity =>
            {
                entity.HasKey(e => e.Empno);

                entity.ToTable("EMP");

                entity.Property(e => e.Empno)
                    .HasPrecision(4)
                    .HasColumnName("EMPNO");

                entity.Property(e => e.Comm)
                    .HasColumnType("NUMBER(7,2)")
                    .HasColumnName("COMM");

                entity.Property(e => e.Deptno)
                    .HasPrecision(2)
                    .HasColumnName("DEPTNO");

                entity.Property(e => e.Ename)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ENAME");

                entity.Property(e => e.Hiredate)
                    .HasColumnType("DATE")
                    .HasColumnName("HIREDATE");

                entity.Property(e => e.Job)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("JOB");

                entity.Property(e => e.Mgr)
                    .HasPrecision(4)
                    .HasColumnName("MGR");

                entity.Property(e => e.Sal)
                    .HasColumnType("NUMBER(7,2)")
                    .HasColumnName("SAL");

                entity.HasOne(d => d.DeptnoNavigation)
                    .WithMany(p => p.Emps)
                    .HasForeignKey(d => d.Deptno)
                    .HasConstraintName("FK_DEPTNO");
            });

            modelBuilder.Entity<Salgrade>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SALGRADE");

                entity.Property(e => e.Grade)
                    .HasColumnType("NUMBER")
                    .HasColumnName("GRADE");

                entity.Property(e => e.Hisal)
                    .HasColumnType("NUMBER")
                    .HasColumnName("HISAL");

                entity.Property(e => e.Losal)
                    .HasColumnType("NUMBER")
                    .HasColumnName("LOSAL");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
