﻿using System.Drawing;

namespace P20210107_DateiexplorerV2
{
    public static class Icons
    {
        public static Icon GetFolder()
        {
            return new Icon(@".\icons\shell32_4.ico");
        }

        public static Icon GetUSB()
        {
            return new Icon(@".\icons\shell32_27.ico");
        }

        public static Icon GetDrive()
        {
            return new Icon(@".\icons\shell32_166.ico");
        }

        public static Icon GetCD()
        {
            return new Icon(@".\icons\shell32_302.ico");
        }
    }
}
