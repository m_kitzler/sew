﻿using System;
using System.Windows.Forms;

namespace P20201008_DataBinding
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            textBoxW.Text = Width.ToString();
            textBoxH.Text = Height.ToString();
        }

        private void textBoxW_TextChanged(object sender, EventArgs e)
        {
            Width = Convert.ToInt32(textBoxW.Text);
        }

        private void textBoxH_TextChanged(object sender, EventArgs e)
        {
            Height = Convert.ToInt32(textBoxH.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Text = textBoxName.Text;
        }
    }
}
