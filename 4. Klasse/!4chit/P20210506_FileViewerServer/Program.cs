﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using P20210114_DateiexplorerV2Forms;

namespace P20210506_FileViewerServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string root = Environment.ExpandEnvironmentVariables(@"%userprofile%\Desktop\_Git");

            TcpListener server = new TcpListener(IPAddress.Any, 9999);
            server.Start();
            while (true)
            {
                string requested;
                TcpClient client = server.AcceptTcpClient();
                using (StreamReader sr = new StreamReader(client.GetStream(), leaveOpen: true))
                {
                    requested = sr.ReadLine();
                }
                Console.WriteLine("[REQUEST]" + requested);

                string[] data = requested.Split(new char[] { ':' }, 2);
                if (data[1][0] == '.')   //Set Base dir
                    data[1] = root + data[1].Substring(1);

                using (StreamWriter sw = new StreamWriter(client.GetStream(), leaveOpen: true))
                {
                    sw.AutoFlush = true;
                    if (data[0] == "fi" && File.Exists(data[1]))
                    {
                        if (File.Exists(data[1]))
                        {
                            //sw.WriteLine("<From Server>\n");
                            sw.Write(File.ReadAllText(data[1]));
                        }
                    }
                    else if (data[0] == "f" || data[0] == "ff")
                    {
                        foreach (string dirPath in Directory.GetDirectories(data[1]))
                        {
                            DirectoryInfo d = new DirectoryInfo(dirPath);
                            string name = d.Name;
                            bool expandable;
                            try
                            {
                                //Peek, ob Node expandable sein soll
                                expandable = d.GetDirectories().Length > 0;
                            }
                            catch (UnauthorizedAccessException)
                            {
                                expandable = false;
                                name += " (kein Zugriff)";
                            }
                            FileFolderInfo send = new FileFolderInfo(d.FullName.Replace(root, "."), true) {
                                Name = name,
                                IsRemote = true,
                                IsExpandable = expandable,
                                LastWrite = d.LastWriteTime
                            };
                            sw.WriteLine(JsonSerializer.Serialize(send));
                        }
                        if (data[0] == "ff")    //files and fodlers
                        {
                            foreach (string filePath in Directory.GetFiles(data[1]))
                            {
                                FileInfo f = new FileInfo(filePath);
                                FileFolderInfo send = new FileFolderInfo(f.FullName, false) {
                                    Name = f.Name,
                                    IsRemote = true,
                                    LastWrite = f.LastWriteTime,
                                    FileSize = GetFileSize(f.FullName)
                                };
                                sw.WriteLine(JsonSerializer.Serialize(send));
                            }
                        }
                        sw.WriteLine("end");
                    }
                }
                client.Dispose();
            }
        }

        /// <summary>
        /// Gibt die größe einer Datei als string mit B, KB, MB, oder GB zurück.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string GetFileSize(string path)
        {
            FileInfo file = new FileInfo(path);
            string hlp = "B";
            double hlp1 = file.Length;
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "KB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "MB";
            }
            if (hlp1 > 1024)
            {
                hlp1 = hlp1 / 1024.0;
                hlp = "GB";
            }
            return Math.Round(hlp1, 2).ToString() + " " + hlp;
        }
    }
}
