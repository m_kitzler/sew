﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace P20210422_SocketVerbindungen
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKey key = Console.ReadKey(true).Key;
            if (key == ConsoleKey.C)
            {
                Console.WriteLine("Client");
                TcpClient client = new TcpClient();
                client.Connect(IPAddress.Loopback, 666);
                Console.WriteLine($"Connected to: {client.Client.RemoteEndPoint}");
                Task.Run(() => HandleReceive(client));
                using (BinaryWriter writer = new BinaryWriter(client.GetStream()))
                {
                    //writer.AutoFlush = true;
                    bool exit = false;
                    while (!exit)
                    {
                        string read = Console.ReadLine();
                        if (read.ToLower() == "quit")
                            exit = true;
                        else
                        {
                            //writer.WriteLine(read);
                            byte[] temp = Encoding.UTF8.GetBytes(read);
                            byte[] send = new byte[temp.Length + 2];
                            send[0] = (byte)(temp.Length / 256);
                            send[1] = (byte)(temp.Length % 256);
                            temp.CopyTo(send, 2);
                            writer.Write(send);
                            writer.Flush();
                        }
                    }
                }
                client.Dispose();
            }
            else if (key == ConsoleKey.S)
            {
                Console.WriteLine("Server");
                TcpListener server = new TcpListener(IPAddress.Any, 666);
                server.Start();
                while (true)
                {
                    Task.Run(() => HandleClient(server.AcceptTcpClient()));
                }
            }
        }

        static void HandleReceive(TcpClient client)
        {
            using (BinaryReader reader = new BinaryReader(client.GetStream()))
            {
                bool exit = false;
                while (!exit)
                {
                    try
                    {
                        //string rec = reader.ReadLine();
                        int msgLen = reader.ReadByte() * 256;
                        msgLen += reader.ReadByte();
                        byte[] msg = reader.ReadBytes(msgLen);
                        string rec = Encoding.UTF8.GetString(msg);
                        Console.WriteLine($"{rec}");
                    }
                    catch
                    {
                        Console.WriteLine($"Server disconnected");
                        exit = true;
                    }
                }
            }
        }

        static void HandleClient(TcpClient client)
        {
            Console.WriteLine($"New Client: {client.Client.RemoteEndPoint}");
            using (BinaryReader reader = new BinaryReader(client.GetStream()))
            {
                using (BinaryWriter writer = new BinaryWriter(client.GetStream()))
                {
                    //writer.AutoFlush = true;
                    bool exit = false;
                    while (!exit)
                    {
                        try
                        {
                            //string rec = reader.ReadLine();
                            int msgLen = reader.ReadByte() * 256;
                            msgLen += reader.ReadByte();
                            byte[] msg = reader.ReadBytes(msgLen);
                            string rec = Encoding.UTF8.GetString(msg);

                            if (rec.StartsWith("file:"))
                            {
                                string filePath = rec.Split(':', 2)[1];
                                Console.WriteLine($"{client.Client.RemoteEndPoint} requests: {filePath}{(File.Exists(filePath) ? "" : "- Not Found!")}");
                                if (File.Exists(filePath))
                                {
                                    using (StreamReader fr = new StreamReader(File.OpenRead(filePath)))
                                    {
                                        while (fr.Peek() != -1)
                                        {
                                            //writer.WriteLine(fr.ReadLine());
                                            byte[] temp = Encoding.UTF8.GetBytes(fr.ReadLine());
                                            byte[] send = new byte[temp.Length + 2];
                                            send[0] = (byte)(temp.Length / 256);
                                            send[1] = (byte)(temp.Length % 256);
                                            temp.CopyTo(send, 2);
                                            writer.Write(send);
                                            writer.Flush();
                                        }
                                    }
                                }
                                else
                                {
                                    //writer.WriteLine("File does not exist!");
                                    byte[] temp = Encoding.UTF8.GetBytes("File does not exist!");
                                    byte[] send = new byte[temp.Length + 2];
                                    send[0] = (byte)(temp.Length / 256);
                                    send[1] = (byte)(temp.Length % 256);
                                    temp.CopyTo(send, 2);
                                    writer.Write(send);
                                    writer.Flush();
                                }
                            }
                            else
                            {
                                Console.WriteLine($"From {client.Client.RemoteEndPoint}: {rec}");
                            }
                        }
                        catch
                        {
                            Console.WriteLine($"Client {client.Client.RemoteEndPoint} left");
                            exit = true;
                        }
                    }
                }
            }
        }
    }
}
