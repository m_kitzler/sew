﻿using Microsoft.EntityFrameworkCore;
using System;

namespace P20210225_EF_Tierheim
{
    public partial class AnimalContext : DbContext
    {
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Bird> Birds { get; set; }
        public DbSet<Dog> Dogs { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\Miau\Documents\tierheim.mdf"";Integrated Security=True;Connect Timeout=30");
        // Zum Anwenden auf Datenbank:
        // dotnet ef migrations add InitialCreate (nachher anderen Name)
        // dotnet ef database update

        public class Animal
        {
            // Mit id am ende wird automatisch AutoIncrement
            public int AnimalId { get; set; }
            public DateTime DateOfAdmission { get; set; }
            public override string ToString()
            {
                return $"{this.AnimalId} : {this.DateOfAdmission}";
            }
        }

        public class Dog : Animal
        {
            public string HowToBark { get; set; }
            public override string ToString()
            {
                return base.ToString() + $" : {this.HowToBark}";
            }
        }

        public class Bird : Animal
        {
            public int WingSpan { get; set; }
            public override string ToString()
            {
                return base.ToString() + $" : {this.WingSpan}";
            }
        }
    }
}
