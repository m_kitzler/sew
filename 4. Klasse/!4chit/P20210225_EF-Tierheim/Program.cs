﻿using System;
using System.Linq;
using static P20210225_EF_Tierheim.AnimalContext;

namespace P20210225_EF_Tierheim
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new AnimalContext())
            {
                //db.Database.Migrate();

                db.Add(new Animal { DateOfAdmission = DateTime.Now });
                db.Add(new Dog { DateOfAdmission = DateTime.Now, HowToBark = "Wuff" });
                db.Add(new Bird { DateOfAdmission = DateTime.Now, WingSpan = 12 });
                db.SaveChanges();

                foreach (var item in db.Animals)
                {
                    Console.WriteLine(item);
                }

                db.RemoveRange(db.Animals.ToList());
                db.SaveChanges();
            }
        }
    }
}
