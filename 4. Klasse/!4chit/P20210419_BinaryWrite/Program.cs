﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace P20210419_BinaryWrite
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient server = new TcpClient();
            server.Connect("", 8888);
            BinaryWriter bw = new BinaryWriter(server.GetStream());
            byte[] send = Encoding.UTF8.GetBytes("Hello World!");
            bw.Write(send);
            bw.Dispose();
            Console.WriteLine("Sent message");
            Console.ReadKey(true);
        }
    }
}
