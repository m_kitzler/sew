﻿using System;
using System.Threading;

namespace _20201210_Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            //Parameterlos
            if (!true)
            {
                ThreadStart del = new ThreadStart(Test);
                Thread thread = new Thread(del);
                thread.Start();
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine("Primär");
                }
            }

            //Mit Parameter
            if (!true)
            {
                //Für mehrere Parameter Klasse mitgeben
                ParameterizedThreadStart del1 = new ParameterizedThreadStart(Tuwas);
                Thread t1 = new Thread(del1);
                t1.Start(42);
                Console.WriteLine("Hello World!");
            }

            //Auf Thread warten
            if (true)
            {
                ParameterizedThreadStart del2 = new ParameterizedThreadStart(Tuwas);
                Thread t2 = new Thread(del2);
                t2.Start(42);
                t2.Join();
                Console.WriteLine("Hello World!");
            }

            Console.ReadKey();
        }

        public static void Test()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Sekundär");
            }
        }

        public static void Tuwas(object o)
        {
            Thread.Sleep(1000);
            Console.WriteLine(o);
        }
    }
}
