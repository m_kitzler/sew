﻿using System;
using System.Xml.Linq;

namespace P20210208_XDocument
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            XElement sumsi = new XElement("Sumsi");
            XElement sparefroh = new XElement("Sparefroh");

            XElement c = new XElement("C");
            XElement b = new XElement("B");
            b.Add(sumsi);
            b.Add(sparefroh);
            XElement a = new XElement("A");
            a.Add(new XAttribute("id", 1));

            XElement Wurzl = new XElement("Wurzl");
            Wurzl.Add(a);
            Wurzl.Add(b);
            Wurzl.Add(c);

            XDocument doc = new XDocument(Wurzl);
            Console.WriteLine(doc);
        }
    }
}