﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace P20210419_BinaryRead
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener server = new TcpListener(IPAddress.Any, 8888);
            BinaryReader br;
            server.Start();
            while (true)
            {
                try
                {
                    TcpClient client = server.AcceptTcpClient();
                    br = new BinaryReader(client.GetStream());
                    Console.WriteLine(Encoding.UTF8.GetString(br.ReadBytes(256)));
                }
                catch
                { }
            }
        }
    }
}
