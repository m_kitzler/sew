﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace P20201214_Eratosthenes
{
    public static class Extensions
    {
        public static string Head(this List<long> li, int count = 10)
        {
            StringBuilder sb = new StringBuilder(li[0].ToString());
            for (int i = 1; i < count && i < li.Count; i++)
            {
                sb.Append(", " + li[i]);
            }

            return sb.ToString();
        }
    }

    class Program
    {
        static List<long>[] _num;

        static void Main(string[] args)
        {
            // Wertebereich, von 0 bis Konstante
            const long calcTo = 100000000;
            // Anzahl der Threads
            const int numThreads = 24;

            // Timer starten
            DateTime start = DateTime.Now;

            List<long> allPrime = new List<long>();   //Liste mit allen Primzahlen
            _num = new List<long>[numThreads];        //Liste für jeden Thread

            int step = (int)Math.Floor((double)(calcTo / numThreads));    //Anzahl der Werte, die ein Thread berechnen muss
            int rest = (int)(calcTo % numThreads);                         //Rest, der bei Division übrig bleibt. Wird dem letzten Thread noch angefügt
            Thread[] threads = new Thread[numThreads];                      //Array mit allen Threads

            // Debug
            Console.WriteLine($"Step: {step}, Rest: {rest}");

            // Starten der Threads
            for (int i = 0; i <= threads.Length - 1; i++)
            {
                _num[i] = new List<long>();
                threads[i] = new Thread(Calc);
                threads[i].Start((step * i, step * (i + 1) + (i == threads.Length - 1 ? rest : 0), /*_num[i]*/ allPrime, false));
            }

            // foreach (Thread t in threads)       //Warten, bis jeder Thread fertig
            // {
            //     t.Join();
            // }
            threads[threads.Length - 1].Join();    //Auf letzten warten

            // Alle listen in eine Cancaten
            // foreach (List<long> li in _num)
            // {
            //     allPrime.AddRange(li);
            // }

            // Timer stoppen
            DateTime stop = DateTime.Now;

            // Ausgabe
            Console.WriteLine($"Size: {allPrime.Count}");
            Console.WriteLine($"Time: {stop - start}");

            // Debug
            // Console.WriteLine(allPrime.Head(230));
        }

        static void CalcOld(object o)
        {         //and Broken
            (int Start, int Stop, List<long> Into, bool Debug) parameters = ((int Start, int Stop, List<long> Into, bool Debug))o;

            DateTime start = DateTime.Now;

            for (int i = parameters.Start; i <= parameters.Stop; i++)
            {
                if (i == 1)    //Ausnahmefall
                    continue;
                if (i == 2 || i == 3 || i == 5 || i == 7)
                {
                    parameters.Into.Add(i);
                }
                else if (i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0)
                {
                    parameters.Into.Add(i);
                }
            }

            if (parameters.Debug)
            {
                DateTime stop = DateTime.Now;
                Console.WriteLine($"Thread succeeded in: {stop - start}");
            }
        }

        static void Calc(object o)
        {
            (int Start, int Stop, List<long> Into, bool Debug) parameters = ((int Start, int Stop, List<long> Into, bool Debug))o;

            DateTime start = DateTime.Now;

            bool prime = true;
            for (int i = parameters.Start; i <= parameters.Stop; i++)
            {
                if (i == 0 || i == 1)    //Ausnahmefälle
                    continue;
                for (int j = 2; j <= Math.Sqrt(i); j++)
                {
                    if (i != j && i % j == 0)
                    {
                        prime = false;
                        break;
                    }
                }
                if (prime)
                    parameters.Into.Add(i);
                prime = true;
            }

            if (parameters.Debug)
            {
                DateTime stop = DateTime.Now;
                Console.WriteLine($"Thread succeeded in: {stop - start}");
            }
        }
    }
}