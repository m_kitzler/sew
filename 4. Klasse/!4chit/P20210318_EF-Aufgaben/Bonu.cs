﻿#nullable disable

namespace P20210318_EF_Aufgaben
{
    public partial class Bonu
    {
        public string Ename { get; set; }
        public string Job { get; set; }
        public decimal? Sal { get; set; }
        public decimal? Comm { get; set; }
    }
}
