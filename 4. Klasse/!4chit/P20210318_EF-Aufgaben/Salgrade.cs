﻿#nullable disable

namespace P20210318_EF_Aufgaben
{
    public partial class Salgrade
    {
        public decimal? Grade { get; set; }
        public decimal? Losal { get; set; }
        public decimal? Hisal { get; set; }
    }
}
