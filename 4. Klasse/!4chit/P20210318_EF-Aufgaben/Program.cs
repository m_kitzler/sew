﻿using System;
using System.Linq;

namespace P20210318_EF_Aufgaben
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ModelContext scott = new ModelContext())
            {
                // 2)
                //(from e in scott.Emps where e.Deptno == 10 select e.Ename + "\t" + e.Job + "\t" + e.Hiredate).ToList().ForEach((s) => Console.WriteLine(s));

                // 12)
                /*
                var emps = scott.Emps.ToList();
                scott.Depts.ToList().ForEach((d) =>
                {
                    Console.WriteLine($"{d.Deptno} : {d.Dname}");
                    (from e in emps where e.Deptno == d.Deptno orderby e.Hiredate descending select e).ToList().ForEach((e) =>
                        Console.WriteLine($"\t{e.Ename} : {e.Hiredate}")
                    );
                });
                */

                // 23)
                /*
                var deps = (from e in scott.Emps where e.Job != "MANAGER" || e.Job != "PRESIDENT" group e.Job by e.Deptno into d select new { d.Key, jobCount = d.Count() }).ToList();
                foreach (var d in deps)
                { 
                    Console.WriteLine($"{d.Key.Value}: {d.jobCount}");
                }*/

                // 38)
                /*
                var emps = scott.Emps.ToList();
                var query = (from e in emps select new { e.Empno, e.Deptno, e.Comm, e.Sal, Bonus = (e.Comm == 0 && e.Sal == 0 ? 1200 : 0) })
                    .GroupBy(e => e.Deptno, e => e, (key, d) => new { key.Value, Sum = d.Sum(x => x.Sal) + d.Sum(x => x.Comm) });
                foreach (var d in query)
                {
                    Console.WriteLine(d.Value + ": " + d.Sum);
                }*/

                // 53)
                var depNY = (from d in scott.Depts where d.Loc.Equals("NEW YORK") select new { d, EmpCount = d.Emps.Count }).FirstOrDefault();
                Console.WriteLine($"{depNY.d.Dname}, {depNY.d.Loc}: {depNY.EmpCount}");
            }
        }
    }
}
