﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

AppDomain.CurrentDomain.SetData("DataDirectory", @"C:\Users\Miau\Desktop\_Git\!SEW\4. Klasse\!4chit\P20210218_EntitiyFramework");
Console.WriteLine("Hello World!");

public class BloggingContext : DbContext
{
    public DbSet<Blog> Blogs { get; set; }

    public DbSet<Post> Posts { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""|DataDirectory|\MyDB.mdf"";Integrated Security=True;Connect Timeout=30");
    }
}

public class Blog
{
    public int BlogId { get; set; }
    public string Url { get; set; }

    public List<Post> Posts { get; set; }
}

public class Post
{
    public int PostId { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }

    public Blog Blog { get; set; }
}

public class AuditEntry
{
    public int AuditEntryId { get; set; }
    public string Username { get; set; }
    public string Action { get; set; }
}