﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace P20210128_Bierkistenfüller
{
    public partial class Form1 : Form
    {
        private Thread tSupplier;
        private Thread tFiller;
        private Thread tTransporter;

        private Supplier _supplier;
        private Filler _filler;
        private Transporter _transporter;

        public Form1()
        {
            InitializeComponent();

            tSupplier = new Thread(Test);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void Test(object o)
        {

        }
    }

    public abstract class _Worker
    {
        public string Status { get; private set; } = "Waiting";
        public abstract void Start();
    }

    public class Supplier : _Worker
    {
        public override void Start()
        {
            throw new NotImplementedException();
        }
    }

    public class Filler : _Worker
    {
        public override void Start()
        {
            throw new NotImplementedException();
        }
    }

    public class Transporter : _Worker
    {
        public override void Start()
        {
            throw new NotImplementedException();
        }
    }
}
