﻿using System;

namespace P20201001_Counter
{
    class Program
    {
        static bool exit = false;
        static void Main(string[] args)
        {
            Counter c = new Counter();
            c.OnReached += Reached;

            Console.WriteLine("Press a to increase total");
            while (!exit)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.A)
                {
                    Console.WriteLine("added one");
                    c.Increase();
                }
            }

            Console.WriteLine("Drücke Escape zum Beenden");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) { }
        }

        static void Reached(object sender, EventArgs e)
        {
            Console.WriteLine("The threshold of " + (e as ReachedEventArgs).threshold + " was reached at " + (e as ReachedEventArgs).fired);
            exit = true;
        }
    }
}
