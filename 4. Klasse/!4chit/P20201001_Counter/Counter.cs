﻿using System;

namespace P20201001_Counter
{
    class ReachedEventArgs : EventArgs
    {
        public int threshold;
        public DateTime fired;

        public ReachedEventArgs(int threshold, DateTime fired)
        {
            this.threshold = threshold;
            this.fired = fired;
        }
    }

    class Counter
    {
        private int current = 0, threshold;
        public event EventHandler OnReached;

        public Counter()
        {
            threshold = new Random().Next(5, 10);
        }

        public void Increase()
        {
            current++;
            if (current == threshold)
                Reached(new ReachedEventArgs(threshold, DateTime.Now));
        }

        protected void Reached(EventArgs e) //Single-Responsibility Prinzip
        {
            OnReached?.Invoke(this, e);
        }
    }
}
