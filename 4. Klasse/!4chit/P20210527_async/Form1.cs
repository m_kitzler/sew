﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P20210527_async
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Tuwas()
        {
            Thread.Sleep(5000);
            MessageBox.Show("Tuwas fertig!");
        }

        private int Machwas()
        {
            Thread.Sleep(5000);
            return new Random().Next(1, 46);
        }

        private async Task<int> MachwasAsync()
        {
            return await Task.Run(Machwas);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tuwas();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Task.Run(Tuwas);
        }


        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Machwas().ToString());
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            int val = await MachwasAsync();
            MessageBox.Show(val.ToString());
        }
    }
}
