﻿namespace P20210510_ToDo
{
    public class TodoItem
    {
        public string Title { get; set; }
        public bool IsDone { get; set; }

        public TodoItem()
        {

        }

        public TodoItem(string s)
        {
            if (s[0] == 't')
            {
                IsDone = true;
            }
            else if (s[0] == 'f')
            {
                IsDone = false;
            }
            Title = s.Substring(1);
        }

        public override string ToString()
        {
            return $"{(IsDone ? "t" : "f")}{Title}";
        }
    }
}
