﻿using System.Windows;

namespace WpfDatenbindung_01
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ABC MyABC { get; set; }
        public Person karli { get; set; }

        public MainWindow()
        {

            InitializeComponent();
            DataContext = this;
            MyABC = new ABC();
            MyABC.Name = "Guglhupf";

            karli = new Person();
            karli.PersonName = "Koarl";

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            this.Resources["Current"] = karli;
            MessageBox.Show(karli.PersonName);
        }
    }
}
