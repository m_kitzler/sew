﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20211118_LinkedListYield
{
    public class MyLinkedList<T> : IEnumerable where T : IComparable
    {
        public Element<T>? root { get; private set; }

        public MyLinkedList() { }

        public MyLinkedList(T root)
        {
            this.root = new Element<T>(root);
        }

        public T this[int i]
        {
            get
            {
                Element<T>? hlp = root;
                while (i >= 0)
                {
                    if (hlp == null)
                        throw new IndexOutOfRangeException();
                    if (i == 0)
                        return hlp.Value;
                    hlp = hlp.NextElement;
                    i--;
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                Element<T>? hlp = root;
                while (i >= 0)
                {
                    if (hlp == null)
                        throw new IndexOutOfRangeException();
                    if (i == 0)
                        hlp.Value = value;
                    else
                    {
                        hlp = hlp.NextElement;
                        i--;
                    }
                }
                if (i < 0)
                    throw new IndexOutOfRangeException();
            }
        }

        public void AddAtEnd(T toAdd)
        {
            if (root == null)
                root = new Element<T>(toAdd);
            else
            {
                Element<T> hlp = root;
                while (hlp.NextElement != null)
                {
                    hlp = hlp.NextElement;
                }
                hlp.NextElement = new Element<T>(toAdd);
            }
        }

        public void AddAtBegin(T toAdd)
        {
            if (root == null)
                root = new Element<T>(toAdd);
            else
            {
                Element<T> hlp = root;
                root = new Element<T>(toAdd);
                root.NextElement = hlp;
            }
        }

        public int Length
        {
            get
            {
                int i = 0;
                Element<T>? hlp = root;
                while (hlp != null)
                {
                    i++;
                    hlp = hlp.NextElement;
                }
                return i;
            }
        }

        public int Count
        {
            get
            {
                return _Count(root);
            }
        }

        private int _Count(Element<T>? current) => current == null ? 0 : _Count(current.NextElement) + 1;

        #region ???
        public string? RevPrint() => RevPrint(root);

        private string? RevPrint(Element<T> current) => (current.NextElement == null) ? current.Value.ToString() : RevPrint(current.NextElement) + ", " + current.Value;
        #endregion

        public override string ToString()
        {
            StringBuilder retVal = new StringBuilder();
            Element<T>? hlp = root;
            while (hlp != null)
            {
                retVal.Append(hlp.Value);
                retVal.Append(" > ");
                hlp = hlp.NextElement;
            }
            retVal.Append("null");
            return retVal.ToString();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            Element<T>? curr = root;
            while (curr != null)
            {
                yield return curr.Value;
                curr = curr.NextElement;
            }
        }
    }
}
