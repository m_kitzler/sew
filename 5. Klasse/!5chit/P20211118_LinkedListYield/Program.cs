﻿// See https://aka.ms/new-console-template for more information

using P20211118_LinkedListYield;

MyLinkedList<int> ll = new();
ll.AddAtEnd(1);
ll.AddAtEnd(2);
ll.AddAtEnd(3);
ll.AddAtEnd(4);
ll.AddAtEnd(5);
Console.WriteLine(ll);

foreach (int i in ll)
{
    Console.WriteLine(i);
}