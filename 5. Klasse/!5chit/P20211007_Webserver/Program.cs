﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace P20211007_Webserver
{
    class Program
    {
        static X509Certificate serverCertificate = new(@"C:\Users\Miau\Desktop\mylocalsite.pfx");
        static void Main(string[] args)
        {
            IPAddress listenOn = IPAddress.Loopback;
            int listenOnPort = 1000;
            TcpListener l = new(listenOn, listenOnPort);
            l.Start();
            Console.WriteLine($"Listening on {listenOn}:{listenOnPort}\n");
            while (true)
            {
                TcpClient c = l.AcceptTcpClient();
                Task.Run(() => HandleClient(c));
            }
        }

        static void HandleClient(TcpClient c)
        {
            Console.WriteLine($"Request from {c.Client.RemoteEndPoint}");
            var stream = c.GetStream();//new SslStream(c.GetStream(), false);
            //stream.AuthenticateAsServer(serverCertificate, false, true);
            //Console.WriteLine(stream.IsEncrypted);

            Dictionary<string, string> request = new();
            using (StreamReader sr = new(stream, leaveOpen: true))
            {
                while (sr.Peek() != -1)
                {
                    string data = sr.ReadLine();
                    Match m = Regex.Match(data, @"(.+?):? (.+)");
                    request.Add(m.Groups[1].Value, m.Groups[2].Value);
                }
            }
            if (request.Count == 0)
                return;
#if DEBUG
            foreach (var k in request)
            {
                Console.WriteLine($"\t{k.Key}: {k.Value}");
            }
#endif

            if (request.ContainsKey("GET"))
            {
                string rootPath = @"..\..\..";
                string path = rootPath + request["GET"].Split(' ')[0];
                if (Path.GetExtension(path) == "")  //add index file for folders
                {
                    path = path + (path[path.Length - 1] == '/' ? "index.html" : "/index.html");
                }
                path = path.Replace('/', '\\'); //to windows path

                using (StreamWriter sw = new(stream))
                {
                    sw.AutoFlush = true;
                    if (File.Exists(path))
                    {
                        Console.WriteLine($"200 OK\nReturning {path}\n");
                        sw.WriteLine("HTTPS/2 200 OK");
                        switch (Path.GetExtension(path))
                        {
                            case ".swf":
                                sw.WriteLine("Content-Type: application/x-shockwave-flash");
                                break;
                            case ".wasm":
                                sw.WriteLine("Content-Type: application/wasm");
                                break;
                            case ".html":
                                sw.WriteLine("Content-Type: text/html");
                                break;
                            case ".js":
                                sw.WriteLine("Content-Type: text/javascript");
                                break;
                            case ".jpg":
                                sw.WriteLine("Content-Type: image/jpeg");
                                break;
                            default:
                                sw.WriteLine("Content-Type: text/plain");
                                break;
                        }
                        sw.WriteLine();
                        FileStream f = new FileStream(path, FileMode.Open);
                        f.CopyTo(stream);
                        f.Close();
                    }
                    else
                    {
                        Console.WriteLine("HTTP/2 404 NOT FOUND\n");
                        sw.WriteLine("HTTP/2 404 NOT FOUND");
                        sw.WriteLine("Content-Type: text/html");
                        sw.WriteLine();
                        FileStream f = new FileStream(rootPath + @"\404.html", FileMode.Open);
                        f.CopyTo(stream);
                        f.Close();
                    }
                }
            }

            c.Close();
        }
    }
}