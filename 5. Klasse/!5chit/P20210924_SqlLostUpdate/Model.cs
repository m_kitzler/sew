﻿using Microsoft.EntityFrameworkCore;

namespace P20210924_SqlLostUpdate
{
    public class CityContext : DbContext
    {
        const string conn = "server=localhost;database=insy;user=root;password=6zhb7ujn;treattinyasboolean=true";

        public DbSet<City> Cities { get; set; }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseMySql(conn, ServerVersion.AutoDetect(conn));
    }

    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EmployeeCount { get; set; }
    }
}
