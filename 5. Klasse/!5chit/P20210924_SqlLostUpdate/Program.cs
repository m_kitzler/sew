﻿using System;
using System.Linq;

namespace P20210924_SqlLostUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            City[] cities = new City[] { new City() { Name = "New York", EmployeeCount = 200 }, new City() { Name = "Vienna", EmployeeCount = 300 }, new City() { Name = "Sydney", EmployeeCount = 100 } };

            CityContext db = new CityContext();
            CityContext db2 = new CityContext();

            db.RemoveRange(db.Cities);
            db.AddRange(cities);
            db.SaveChanges();
            Console.WriteLine("Data Added");

            //db
            cities[0].EmployeeCount -= 8;

            //db2
            City ny = db2.Cities.First();
            ny.EmployeeCount += 2;
            db2.SaveChanges();

            //db
            if (/*!db.ChangeTracker.HasChanges()*/db.Entry(cities[0]).State == Microsoft.EntityFrameworkCore.EntityState.Unchanged)
                db.SaveChanges();
            else
            {
                Console.WriteLine("Entity changed, retrying");
                db.Entry(cities[0]).Reload();
                cities[0].EmployeeCount -= 8;
                db.SaveChanges();
            }
        }
    }
}
