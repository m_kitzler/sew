﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace P20210916_ClientServerMusic
{
    public abstract class MusicBase
    {
        protected DateTime lastElapse = DateTime.Now;
        protected System.Timers.Timer syncTimer = new(100) { AutoReset = true };
        protected ushort tick = 0;

        protected MusicBase()
        {
            syncTimer.Elapsed += TimerElapsed;
        }

        protected virtual void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            tick++;
            lastElapse = DateTime.Now;
        }
    }

    public class MusicClient : MusicBase
    {
        private TcpClient client = new();

        private CancellationTokenSource cts = new();
        private Task packetReceiver;

        private Dictionary<int, Packet> Packets = new();
        private List<TimeSpan> delays = new();

        /// <summary>
        /// Gibt die durchschnittliche Verzögerung des Client-Timer und Server-Timers zurück.
        /// </summary>
        public int AveragePing
        {
            get
            {
                return (delays.Count > 0) ? Math.Abs(Convert.ToInt32(delays.Average((d) => d.TotalMilliseconds))) : 9999;
            }
        }

        /// <summary>
        /// Zeigt, ob der Timer mit dem Server synchronisiert wurde.
        /// </summary>
        /// <remarks>
        /// Durch setzen auf false kann eine neusynchronisation erzwungen werden.
        /// </remarks>
        public bool Synchronised { get; set; }

        public MusicClient() : base()
        {
            packetReceiver = new Task(() => Receiver(cts.Token), cts.Token);
        }

        /// <summary>
        /// Verbindet sich mit dem angegebenen Server und spielt dessen Musik ab.
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public void Start(IPAddress ip, int port)
        {
            client.Connect(ip, port);
            packetReceiver.Start();
            syncTimer.Start();
        }

        /// <summary>
        /// Trennt die Verbindung vom Server.
        /// </summary>
        public void Stop()
        {
            syncTimer.Stop();
            cts.Cancel();
            client.Close();

            delays.Clear();
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            base.TimerElapsed(sender, e);

            if (Packets.ContainsKey(tick))
            {
                if (Packets[tick].PacketType == PacketType.Tone)
                    Console.Beep(Packets[tick].Frequency, Packets[tick].Duration);
                else if (Packets[tick].PacketType == PacketType.Delay)
                    Task.Delay(Packets[tick].Duration).Wait();
                Packets.Remove(tick);
            }
        }

        private void Receiver(CancellationToken token)
        {
            StreamReader sr = new StreamReader(client.GetStream(), leaveOpen: true);
            try
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    if (client.Available > 0)
                    {
                        DateTime recTime = DateTime.Now;
                        string data = sr.ReadLine();
                        Packet p = JsonSerializer.Deserialize<Packet>(data);
                        if (p.PacketType == PacketType.Tone)
                        {
                            Packets.Add(p.Tick, p);
                        }
                        else if (p.PacketType == PacketType.Sync)
                        {
                            if (tick != p.Tick)
                                tick = p.Tick;
                            delays.Add(recTime - lastElapse);
                            if (!Synchronised && delays.Count >= 3 && AveragePing > 10)
                            {
                                syncTimer.Stop();
                                syncTimer.Start();
                                delays.Clear();
                            }
                            if (delays.Count > 10)
                            {
                                delays.RemoveAt(0);
                                if (AveragePing > 50)
                                    Synchronised = false;
                                else
                                    Synchronised = true;
                            }
                        }
                    }
                    Task.Delay(1).Wait();
                }
            }
            catch (OperationCanceledException)
            {
                sr.Close();
            }
        }
    }

    public class MusicServer : MusicBase
    {
        private TcpListener server;
        /// <summary>
        /// Die mit dem Server verbundenen CLients.
        /// </summary>
        public List<TcpClient> Clients { get; private set; } = new();

        private CancellationTokenSource cts = new();
        private Task packetSender;

        /// <summary>
        /// Die zum Senden ausstehenden Pakete.
        /// </summary>
        public Queue<Packet> Packets { get; private set; } = new();

        public MusicServer(int port) : base()
        {
            server = new TcpListener(IPAddress.Any, port);
            packetSender = new Task(() => Sender(cts.Token), cts.Token);
        }

        /// <summary>
        /// Startet den Server.
        /// </summary>
        public void Start()
        {
            server.Start();
            server.BeginAcceptTcpClient(NewClient, server);
            packetSender.Start();
            syncTimer.Start();
        }

        /// <summary>
        /// Stoppt den Server.
        /// </summary>
        public void Stop()
        {
            syncTimer.Stop();
            cts.Cancel();
            server.Stop();
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            base.TimerElapsed(sender, e);

            TcpClient[] tmpClients = new TcpClient[Clients.Count];
            Clients.CopyTo(tmpClients);
            foreach (TcpClient c in tmpClients)
            {
                if (!c.SendKeepalive())
                    Clients.Remove(c);
                else
                {
                    c.SendPacket(new Packet(tick));
                }
            }
        }

        private void Sender(CancellationToken token)
        {
            try
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    if (Packets.Count > 0)
                    {
                        Packet p = Packets.Dequeue();
                        Clients.ForEach((c) => {
                            if (c.SendKeepalive())
                            {
                                c.SendPacket(p);
                            }
                        });
                    }
                    Task.Delay(1).Wait();
                }
            }
            catch (OperationCanceledException)
            {
                Clients.ForEach((c) => c.Close());
            }
        }

        private void NewClient(IAsyncResult ar)
        {
            TcpListener server = ar.AsyncState as TcpListener;
            if (!cts.IsCancellationRequested)
            {
                Clients.Add(server.EndAcceptTcpClient(ar));
                server.BeginAcceptTcpClient(NewClient, server);
            }
        }

        /// <summary>
        /// Sendet einen Ton mit der angegebenen Frequenz und Länge.
        /// </summary>
        /// <param name="frequency"></param>
        /// <param name="duration"></param>
        public void PlayTone(int frequency, int duration)
        {
            Packets.Enqueue(new(tick, frequency, duration));
        }

        /// <summary>
        /// Ließt ein Lied aus einer Datei.
        /// </summary>
        /// <remarks>
        /// Pro Zeile darf nur ein Ton/eine Verzögerung sein. Zwei Nummern in einer Zeile werden als Ton, eine als Verzögerung erkannt.
        /// </remarks>
        /// <param name="path">Pfad der zu lesenden Datei</param>
        public void FromFile(string path)
        {
            if (File.Exists(path))
            {
                Regex r = new Regex("[0-9]+");
                using (StreamReader sr = new StreamReader(new FileStream(path, FileMode.Open)))
                {
                    int lastDelay = 0;
                    while (!sr.EndOfStream)
                    {
                        int[] vals = r.Matches(sr.ReadLine()).Select<Match, int>((m) => int.Parse(m.Value)).ToArray();
                        if (vals.Length == 2)
                        {
                            Packets.Enqueue(new((ushort)(tick + lastDelay / 100), vals[0], vals[1]));
                            lastDelay += vals[1];
                        }
                        else if (vals.Length == 1)
                        {
                            Packets.Enqueue(new((ushort)(tick + lastDelay / 100), vals[0]));
                            lastDelay += vals[0];
                        }
                    }
                }
            }
        }
    }
}
