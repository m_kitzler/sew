﻿namespace P20210916_ClientServerMusic
{
    public class Packet
    {
        public Packet() { } //for json serializer
        public Packet(ushort tick)
        {
            PacketType = PacketType.Sync;
            Tick = tick;
        }
        public Packet(ushort tick, int frequency, int duration)
        {
            PacketType = PacketType.Tone;
            Tick = tick;
            Frequency = frequency;
            Duration = duration;
        }
        public Packet(ushort tick, int duration)
        {
            PacketType = PacketType.Delay;
            Tick = tick;
            Duration = duration;
        }

        public PacketType PacketType { get; init; }
        public ushort Tick { get; init; }
        public int Frequency { get; init; }
        public int Duration { get; init; }
    }

    public enum PacketType
    {
        Sync,
        Tone,
        Delay
    }
}
