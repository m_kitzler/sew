﻿using System.IO;
using System.Net.Sockets;
using System.Text.Json;

namespace P20210916_ClientServerMusic
{
    public static class Extensions
    {
        public static bool SendKeepaliveOld(this TcpClient tcp)
        {
            try
            {
                tcp.Client.Send(new byte[0], 0, SocketFlags.None);
            }
            catch (SocketException) {}
            return tcp.Connected;
        }

        public static bool SendKeepalive(this TcpClient tcp)
        {
            bool blockingState = tcp.Client.Blocking;
            try
            {
                tcp.Client.Blocking = false;
                tcp.Client.Send(new byte[0], 0, 0);
                return true;
            }
            catch (SocketException e)
            {
                // 10035 == WSAEWOULDBLOCK
                if (e.NativeErrorCode.Equals(10035))
                    return true;
                else
                {
                    return false;
                }
            }
            finally
            {
                tcp.Client.Blocking = blockingState;
            }
        }

        public static bool SendSyncOld(this TcpClient tcp, ushort tick)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(tcp.GetStream(), leaveOpen: true))
                {
                    sw.WriteLine(JsonSerializer.Serialize(new Packet(tick)));
                }
            }
            catch (SocketException) {}
            return tcp.Connected;
        }

        public static void SendPacket(this TcpClient tcp, Packet p)
        {
            using (StreamWriter sw = new StreamWriter(tcp.GetStream(), leaveOpen: true))
            {
                sw.WriteLine(JsonSerializer.Serialize(p));
            }
        }
    }
}
