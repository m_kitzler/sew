﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace P20210916_ClientServerMusic
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            const string fileLoc = @"..\..\.";
#else
            const string fileLoc = "";
#endif
            Console.CursorVisible = false;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("c für Client, s für Server");
                ConsoleKey input = Console.ReadKey(true).Key;
                if (input == ConsoleKey.C)
                {
                    Console.Clear();

                    Console.Write("Gib die IP-Adresse ein: ");
                    Console.CursorVisible = true;
                    string[] tempInput = Console.ReadLine().Split(':');
                    Console.CursorVisible = false;
                    MusicClient client = new MusicClient();
                    client.Start((tempInput[0] == "") ? IPAddress.Loopback : IPAddress.Parse(tempInput[0]), (tempInput.Length > 1) ? Convert.ToInt32(tempInput[1]) : 8888);

                    bool exit = false;
                    while (!exit)
                    {
                        if (Console.KeyAvailable)
                            if (Console.ReadKey(true).Key == ConsoleKey.Q)
                                exit = true;
                        if (client.Synchronised)
                        {
                            Console.SetCursorPosition(0, 1);
                            Console.Write($"Ping: {client.AveragePing}   ");
                        }
                        Console.Title = client.Synchronised ? "Verbunden" : "Verbindung wird hergestellt...";
                        Task.Delay(50).Wait();
                    }

                    client.Stop();
                }
                else if (input == ConsoleKey.S)
                {
                    Console.Clear();
                    MusicServer server = new MusicServer(8888);
                    server.Start();

                    int lastClientCount = 0;      //Drawing of connected clients
                    bool exit = false;
                    while (!exit)
                    {
                        if (Console.KeyAvailable)
                        {
                            input = Console.ReadKey(true).Key;
                            if (input == ConsoleKey.D1)
                                server.FromFile(fileLoc + @".\Tetris.txt");
                            if (input == ConsoleKey.D2)
                                server.FromFile(fileLoc + @".\HappyBirthday.txt");
                            if (input == ConsoleKey.Q)
                                exit = true;
                        }

                        Console.Title = $"Connected Clients: {server.Clients.Count}, Packets in queue: {server.Packets.Count}";
                        Console.SetCursorPosition(0, 0);
                        Console.WriteLine("1 für Tetris, 2 für Happy Birthday, q um zu Beenden\n");
                        Console.WriteLine("Verbundene Clients:");
                        server.Clients.ForEach((c) => Console.WriteLine(c.Client.RemoteEndPoint));
                        for (int i = 0; i < lastClientCount - server.Clients.Count; i++)
                            Console.WriteLine("                    ");
                        lastClientCount = server.Clients.Count;

                        Task.Delay(50).Wait();
                    }
                    server.Stop();
                }
            }
        }
    }
}
