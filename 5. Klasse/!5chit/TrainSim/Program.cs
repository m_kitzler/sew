﻿using System;
using System.Threading;

namespace TrainSim
{
    static class Sems {
        public static Semaphore screen = new Semaphore(1, 1);
        //public static SemaphoreSlim sc = new SemaphoreSlim(1);
        public static Semaphore[] indermittn = new Semaphore[8];
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WindowWidth = 85;
            Console.CursorVisible = false;
            // print rail
            for (int i = 0; i < 80; i++)
            {
                Console.Write("=");
            }
            for (int i = 0; i < Sems.indermittn.Length; i++)
            {
                Sems.indermittn[i] = new Semaphore(1, 1);
                if (i < Sems.indermittn.Length - 1)
                {
                    Console.SetCursorPosition(10 + 10 * (i % 10), 1);
                    Console.Write("|");
                    Console.SetCursorPosition(10 + 10 * (i % 10), 2);
                    Console.Write("/");
                }
            }


            while (true)
            {
                var rk = Console.ReadKey();
                if (rk.KeyChar == 'r')
                    ;//indermittn[0].Release();
                else
                {
                    new Thread(StartTrain).Start();
                    Console.Write('\b');
                }
                    
            }
        }

        static int cnt=0;
        private static void StartTrain()  // create train with random size and increasing Number
        {
            Train current = new Train(1+new Random().Next(2, 5), ++cnt);
            // print train
            while (current.CurrPos < 79)
            {

                current.DisplayOnRail();
                current.Move();
                Thread.Sleep(200);
            }
            
        }
    }
    class Train
    {
        public readonly int Lenght;   //Number of Wagons
        public readonly int Nr;     //Serial Nr of reation
        public int currSector { get; private set; } // Where I am
        public int CurrPos { get; private set; }    // Where I am exactly
        public Train(int size, int nr)
        {
            Lenght = size;
            CurrPos = Lenght;
            Nr = nr;
        }
        public void Move() {          
            {
                //DisplayOnRail(); // zeichnet Zug auf Schienen

                if (CurrPos % 10 == 0)//alle 10 kommt ein Semaphor
                {
                    currSector++;
                    Sems.indermittn[currSector].WaitOne();
                    RefreshGui(false);
                }              

                if ((CurrPos-Lenght+1)%10==0)//Ausfahrt
                {
                    if (currSector > 1)
                    {
                        Sems.indermittn[currSector - 1].Release(); //gibt vorigen Sektor wieder frei
                        RefreshGui(true);
                    }
                }
            }
            CurrPos++;
            if (CurrPos == 79)//Verschwinde, Zug!
            {
                Sems.indermittn[currSector].Release(); //gibt letzen Sektor wieder frei
                RefreshGui(false);
            }
        }
        private void RefreshGui(bool release) {

            Sems.screen.WaitOne();

            // aus??
            if (CurrPos == 79)
            {
                Console.SetCursorPosition(CurrPos - 7, 0);
                Console.WriteLine("=======");//Verschwinde, Zug! nun definitiv :)

                Console.SetCursorPosition(0, 3 + Nr);
                Console.Write($"Train nr {Nr}({Thread.CurrentThread.GetHashCode()}) entered section {currSector}, released {currSector }");

                Console.SetCursorPosition((10 * (currSector)), 2); //letzen Sem neu zeichne
                Console.Write("/");
                Sems.screen.Release();
                return;
            }

            Console.SetCursorPosition(0, 3 + Nr);

            string status = release == true ? "releasing" : "entering ";
            Console.Write($"Train nr {Nr}({Thread.CurrentThread.GetHashCode()}) entered section {currSector}, released {currSector-1}");

            if (!release)
            {
                Console.SetCursorPosition(10 * (currSector % 10), 2);
                Console.Write("-");  //Semaphor als gesperrt zeichnen
            }
            else
            {
                Console.SetCursorPosition((10 * (currSector-1 )), 2);
                Console.Write("/"); // Semaphor als frei zeichnen
            }
            Sems.screen.Release();
           
        }
        public void DisplayOnRail() {
            Sems.screen.WaitOne();

            Console.SetCursorPosition(CurrPos-Lenght, 0);
            Console.Write("=");
            Console.SetCursorPosition(CurrPos, 0);
            Console.Write("|");

            Sems.screen.Release();
        }
    }
}
