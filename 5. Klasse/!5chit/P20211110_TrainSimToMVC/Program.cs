﻿using P20211110_TrainSimToMVC;

for (int i = 0; i < Sems.segment.Length; i++)
{
	Sems.segment[i] = new(1, 1);
}

Console.Write("==========");
for (int i = 0; i < 10; i++)
{
    Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop + 1);
    Console.Write("|");
    Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop + 1);
    Console.Write("/");
    Console.SetCursorPosition(Console.CursorLeft - 1, 0);
    Console.Write("==========");
}

List<Train> trains = new List<Train>();
int trainNum = 0;
DateTime lastTrain = DateTime.Now;
while (true)
{
    if (DateTime.Now - lastTrain > new TimeSpan(0, 0, 0, 0, 2000) || Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.A)
    {
        lastTrain = DateTime.Now;
        Train t = new(new Random().Next(4,6), trainNum++);
        t.TrainReachedEnd += RemoveTrain;
        Task.Run(t.MoveToEnd);
        trains.Add(t);
    }
}

void RemoveTrain(object? o, EventArgs e)
{
    trains.Remove(o as Train);
}

public static class Sems
{
	public static Semaphore screen = new(1, 1);
    public static int currentLogPos = 4;
	public static Semaphore[] segment = new Semaphore[10];
}