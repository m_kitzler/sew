﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20211110_TrainSimToMVC
{
	public class TrainView
	{
		private TrainModel model;

		public TrainView(TrainModel model)
		{
			Console.CursorVisible = false;
			this.model = model;
			model.ValuesChanged += Redraw;
		}

		private void Redraw(object? sender, EventArgs e)
		{
			Sems.screen.WaitOne();
			if (model.TailPos + 1 > 0)
			{
				Console.SetCursorPosition(model.TailPos - 1, 0);
				Console.Write("=");
			}
			Console.SetCursorPosition(model.TailPos, 0);
			for (int i = 0; i < model.Lenght && i + model.TailPos < 10 * 11; i++)
            {
				if (model.TailPos + i > 0)
					Console.Write("|");
            }
			if (model.CurrPos % 10 == 0 && model.CurrPos > 0 && model.CurrPos < 10 * 11)
			{
				Console.SetCursorPosition(model.CurrPos, 2);
				Console.Write("-");
			}
			if (model.TailPos % 10 == 0 && model.TailPos > 10)
			{
				Console.SetCursorPosition(model.TailPos - 10, 2);
				Console.Write("/");
			}
			Sems.screen.Release();
		}
	}
}
