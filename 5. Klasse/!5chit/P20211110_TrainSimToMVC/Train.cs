﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20211110_TrainSimToMVC
{
	public class Train
	{
		private TrainModel model;
		private TrainView view;

		public event EventHandler? TrainReachedEnd;

		public Train(int size, int nr)
		{
			model = new TrainModel(size, nr, new Random().Next(170, 230));
			view = new TrainView(model);
		}

		public void MoveToEnd()
		{
			while(!model.Move()) { }
			TrainReachedEnd?.Invoke(this, new());
		}
	}
}
