﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20211110_TrainSimToMVC
{
	public class TrainModel
	{
		public readonly int Lenght;
		public readonly int Nr;
		public readonly int Speed;
		public int TailSector
		{
			get
			{
				return (TailPos / 10) - 1;  // -1, da erster Sektor keine Ampel
			}
		}
		public int CurrSector
		{
			get
			{
				return (CurrPos / 10) - 1;
			}
		}
		private int currPos;
		public int CurrPos {
			get
			{
				return currPos;
			}
			set
			{
				currPos = value;
				ValuesChanged?.Invoke(this, new());
			}
		}
		public int TailPos
		{
			get
			{
				return CurrPos - Lenght + 1;
			}
		}
		public bool ReachedEnd { get; private set; } = false;
		public event EventHandler? ValuesChanged;

		public TrainModel(int size, int nr, int speed)
		{
			Lenght = size;
			currPos = Lenght;
			Nr = nr;
			Speed = speed;
		}

		public bool Move()
		{
			if (CurrPos % 10 == 9)
				LockSector(CurrSector + 1);   // Lock next
			CurrPos++;
			if (TailPos % 10 == 0)
				ReleaseSector(TailSector - 1);    // Release prev
			Thread.Sleep(Speed);
			bool ReachedEnd = TailPos > 10 * 11 - 1;
			if (ReachedEnd)
				ReleaseSector(TailSector);
			return ReachedEnd;
		}

		private void LockSector(int s)
		{
			if (s >= 0 && s < 10)
			{
				Sems.segment[s].WaitOne();
				//Sems.screen.WaitOne();
				//Console.SetCursorPosition(0, Sems.currentLogPos++);
				//Console.Write($"Train {Nr} locked Sector {s}");
				//Sems.screen.Release();
			}
		}

		private void ReleaseSector(int s)
		{
			if (s >= 0 && s < 10)
			{
				Sems.segment[s].Release();
				//Sems.screen.WaitOne();
				//Console.SetCursorPosition(0, Sems.currentLogPos++);
				//Console.Write($"Train {Nr} released Sector {s}");
				//Sems.screen.Release();
			}
		}
	}
}
